﻿using System;

namespace RadiationTreatmentPlanner.Utils.ROIType
{
    /// <summary>
    /// Radiation Therapy Region of Interest Interpreted Code:
    /// https://dicom.innolitics.com/ciods/rt-structure-set/rt-roi-observations/30060080/300600a4
    /// </summary>
    public class RtRoiInterpretedType
    {
        public string Code { get; }
        public string Description { get; }

        /// <summary>
        /// Radiation Therapy Region of Interest Interpreted Code.
        /// </summary>
        /// <param name="code">Unique code of the RT ROI Interpreted Type. It is used to
        /// clearly distinguish between regions of interest.</param>
        /// <param name="description">Description of the RT ROI Interpreted Type.</param>
        public RtRoiInterpretedType(string code, string description)
        {
            Code = code ?? throw new ArgumentNullException(nameof(code));
            Description = description ?? throw new ArgumentNullException(nameof(description));
        }

        protected bool Equals(RtRoiInterpretedType other)
        {
            return Code == other.Code && Description == other.Description;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((RtRoiInterpretedType)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Code, Description);
        }
    }
}
