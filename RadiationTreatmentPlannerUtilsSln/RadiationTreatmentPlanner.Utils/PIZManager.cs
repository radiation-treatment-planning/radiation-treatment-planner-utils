﻿using System.Collections.Generic;

namespace RadiationTreatmentPlanner.Utils
{
    public class PIZManager
    {
        /*
         * Manages PIZ. In this class a list of PIZ numbers is stored.
         * The list can be created via constructor and then alterd
         * later.
         *
         */

        private List<string> _pizs = new List<string>();

        public PIZManager(params string[] pizList)
        {
            _pizs.AddRange(pizList);
        }

        public IEnumerable<string> GetAllPIZs() => _pizs;


        // Adds PIZ to the Queue.
        public void AddPIZ(string piz)
        {
            _pizs.Add(piz);
        }

        // Returns number of PIZ contained in the Queue.
        public int Size()
        {
            return _pizs.Count;
        }
    }
}
