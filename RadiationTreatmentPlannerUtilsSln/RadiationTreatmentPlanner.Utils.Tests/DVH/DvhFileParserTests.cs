﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.Tests.DVH
{
    [TestFixture]
    public class DvhFileParserTests
    {
        [SetUp]
        public void SetUp()
        {
            _dvhFileParser = DVHFileParser.CreateFromFile(_fileName);
        }

        private readonly string _fileName = "FilesForTesting/CumulativeDvhExampleFile.txt";
        private DVHFileParser _dvhFileParser;

        [Test]
        public void ThrowFileNotFoundExceptionIfFileNotFound_Test()
        {
            Assert.Throws<FileNotFoundException>(() => DVHFileParser.CreateFromFile("wrong_file_path.txt"));
        }

        [Test]
        public void GetVersion_Test()
        {
            Assert.AreEqual("1.0.1", _dvhFileParser.GetVersion());
        }

        [Test]
        public void GetPiz_Test()
        {
            Assert.AreEqual("12345678", _dvhFileParser.GetPiz());
        }

        [Test]
        public void GetPlan_Test()
        {
            Assert.AreEqual("AutoPlan", _dvhFileParser.GetPlan());
        }

        [Test]
        public void GetStructure_Test()
        {
            Assert.AreEqual("GTV", _dvhFileParser.GetStructure());
        }

        [Test]
        public void GetTotalVolume_Test()
        {
            var expectedVolumeValue = new UVolume(61.8671054324199, UVolume.VolumeUnit.ccm);
            var actualVolume = _dvhFileParser.GetTotalVolume();

            Assert.AreEqual(expectedVolumeValue.Value, actualVolume.Value);
            Assert.AreEqual(expectedVolumeValue.Unit, actualVolume.Unit);
        }

        [Test]
        public void GetCoverage_Test()
        {
            Assert.AreEqual(1, _dvhFileParser.GetCoverage());
        }

        [Test]
        public void GetSamplingCoverage_Test()
        {
            Assert.AreEqual(1.00021608280217, _dvhFileParser.GetSamplingCoverage());
        }

        [Test]
        public void GetNumberOfFractions_Test()
        {
            Assert.AreEqual(12, _dvhFileParser.GetNumberOfFractions());
        }

        [Test]
        public void GetDoseVolumePairsList_CorrectNumberOfElements_Test()
        {
            var doseValuePairsEnumerable = _dvhFileParser.GetDVHCurve();
            Assert.AreEqual(8512, _dvhFileParser.GetDVHCurve().Count());
        }

        [Test]
        public void GetDoseVolumePairsList_Test()
        {
            var doseValuePairsEnumerable = _dvhFileParser.GetDVHCurve();
            var expectedResults = new List<Tuple<UDose, UVolume>>();
            expectedResults.Add(Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy),
                new UVolume(61.8671054324197, UVolume.VolumeUnit.ccm)));
            expectedResults.Add(Tuple.Create(new UDose(0.01, UDose.UDoseUnit.Gy),
                new UVolume(61.8671054324197, UVolume.VolumeUnit.ccm)));
            expectedResults.Add(Tuple.Create(new UDose(0.02, UDose.UDoseUnit.Gy),
                new UVolume(61.8671054324197, UVolume.VolumeUnit.ccm)));

            var idx = 0;
            foreach (var tuple in doseValuePairsEnumerable.GetPoints())
            {
                Assert.AreEqual(expectedResults.ElementAt(idx).Item1.Value, tuple.Item1.Value);
                Assert.AreEqual(expectedResults.ElementAt(idx).Item1.Unit, tuple.Item1.Unit);
                Assert.AreEqual(expectedResults.ElementAt(idx).Item2.Value, tuple.Item2.Value);
                Assert.AreEqual(expectedResults.ElementAt(idx).Item2.Unit, tuple.Item2.Unit);
                idx++;
                if (idx > 2)
                    break;
            }

            Assert.AreEqual(DVHCurve.Type.CUMULATIVE, doseValuePairsEnumerable.DVHType);
            Assert.AreEqual(DVHCurve.DoseType.PHYSICAL, doseValuePairsEnumerable.TypeOfDose);
        }

        [Test]
        public void CreateFromFile_cGy_AsDoseUnit_And_Percent_AsVolumeUnit_Test()
        {
            var dvhFileParser = DVHFileParser.CreateFromFile("FilesForTesting/CumulativeDvhExampleFile4.txt");
            var totalVolume = dvhFileParser.GetTotalVolume();
            Assert.AreEqual(new UVolume(61.8671054324199, UVolume.VolumeUnit.ccm), totalVolume);

            var dvhCurve = dvhFileParser.GetDVHCurve();
            Assert.AreEqual(new UDose(1000, UDose.UDoseUnit.cGy), dvhFileParser.GetMaxDose());
            Assert.AreEqual(new UDose(0, UDose.UDoseUnit.cGy), dvhFileParser.GetMinDose());
            Assert.AreEqual(new UDose(500, UDose.UDoseUnit.cGy), dvhFileParser.GetMedianDose());
            Assert.AreEqual(new UDose(500, UDose.UDoseUnit.cGy), dvhFileParser.GetStandardDeviationDose());
            Assert.AreEqual(new UDose(800, UDose.UDoseUnit.cGy), dvhFileParser.GetPrescriptionDose());

            foreach (var point in dvhCurve.GetPoints())
            {
                Assert.AreEqual(UDose.UDoseUnit.cGy, point.Item1.Unit);
                Assert.AreEqual(UVolume.VolumeUnit.percent, point.Item2.Unit);
            }
        }
    }
}
