﻿using System;
using System.Collections.Generic;
using System.Linq;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.Fields;
using RadiationTreatmentPlanner.Utils.OptimizationObjetives;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.Patient
{
    public class UPlanSetup : IUPlanSetup
    {
        public string Id { get; }
        public UPatient Patient { get; }
        public UCourse Course { get; }
        public Option<uint> NumberOfFractions { get; private set; }
        public UDose TotalPrescriptionDose { get; private set; }
        public UStructureSet StructureSet { get; }
        public IEnumerable<UDvhEstimate> DvhEstimates => _dvhEstimates.AsEnumerable();
        private readonly List<UDvhEstimate> _dvhEstimates;

        private readonly List<IObjective> _optimizationObjectives;
        public IEnumerable<IObjective> OptimizationObjectives => _optimizationObjectives;

        private List<VMATArc> _vmatArcs;
        public IEnumerable<VMATArc> VmatArcs => _vmatArcs;


        internal UPlanSetup(string id, UPatient patient, UCourse course, Option<uint> numberOfFractions,
            UDose totalPrescriptionDose, UStructureSet structureSet)
        {
            Id = id;
            Patient = patient;
            Course = course;
            NumberOfFractions = numberOfFractions;
            TotalPrescriptionDose = totalPrescriptionDose;
            StructureSet = structureSet;
            _dvhEstimates = new List<UDvhEstimate>();
            _optimizationObjectives = new List<IObjective>();
            _vmatArcs = new List<VMATArc>();
        }

        public void SetTotalDose(UDose totalPrescriptionDose) => TotalPrescriptionDose = totalPrescriptionDose;
        public void SetNumberOfFractions(Option<uint> numberOfFractions) => NumberOfFractions = numberOfFractions;

        /// <summary>
        /// Add an optimization objective to plan setup.
        /// </summary>
        /// <param name="objective">An optimization objective</param>
        /// <returns></returns>
        public OptimizationObjectiveAddResult AddOptimizationObjective(IObjective objective)
        {
            if (StructureSet.Structures.Any(x => x.Id == objective.StructureId))
            {
                _optimizationObjectives.Add(objective);
                return new OptimizationObjectiveAddResult(true, "Successfully added one optimization objective.");
            }
            return new OptimizationObjectiveAddResult(false,
                $"Objective was not added, because there exists no structure with ID" +
                $" {objective.StructureId} in structure set with ID {StructureSet.Id}.");
        }

        /// <summary>
        /// Add multiple objectives to plan setup.
        /// </summary>
        /// <param name="objectives">Optimization objectives</param>
        /// <returns>OptimizationObjectiveAddResult</returns>
        public OptimizationObjectiveAddResult AddOptimizationObjectiveRange(IEnumerable<IObjective> objectives)
        {
            foreach (var objective in objectives)
            {
                if (StructureSet.Structures.All(x => x.Id != objective.StructureId))
                    return new OptimizationObjectiveAddResult(false,
                        $"No objectives were added, because there exists no structure with ID" +
                        $" {objective.StructureId} in structure set with ID {StructureSet.Id}.");
            }
            _optimizationObjectives.AddRange(objectives);
            return new OptimizationObjectiveAddResult(true,
                $"Successfully added {objectives.Count()} optimization objectives.");
        }

        /// <summary>
        /// Adds a DVH Estimate.
        /// </summary>
        public void AddDvhEstimate(string structureId, double coverage, UDose minDose, UDose meanDose, UDose medianDose, UDose maxDose,
            double samplingCoverage, UDose standardDeviationDose, UVolume volume, DVHCurve dvhCurve)
        {
            var structure = StructureSet.Structures.FirstOrDefault(x => x.Id == structureId);
            if (structure is null)
                throw new ArgumentException(
                    $"Cannot match Dvh Estimate to structure with id {structureId}," +
                    $" because structure does not exist in structure set with id {StructureSet.Id}.");
            _dvhEstimates.Add(new UDvhEstimate(this, structure, coverage, minDose, meanDose, medianDose, maxDose, samplingCoverage, standardDeviationDose, volume, dvhCurve));
        }

        public void AddDvhEstimate(UDvhEstimateDetached dvhEstimate)
        {
            if (dvhEstimate.PlanSetupId != this.Id)
                throw new ArgumentException(
                    $"Cannot add DvhEstimate, because ids of PlanSetup and DvhEstimate's PlanSetup don't match." +
                    $" Received {dvhEstimate.PlanSetupId} and {this.Id}, but they should be equal.");
            var structure = StructureSet.Structures.FirstOrDefault(x => x.Id == dvhEstimate.StructureId);
            if (structure is null)
                throw new ArgumentException(
                    $"Cannot match Dvh Estimate to structure with id {dvhEstimate.StructureId}," +
                    $" because structure does not exist in structure set with id {StructureSet.Id}.");
            _dvhEstimates.Add(new UDvhEstimate(this, structure, dvhEstimate.Coverage, dvhEstimate.MinDose,
                dvhEstimate.MeanDose, dvhEstimate.MedianDose, dvhEstimate.MaxDose, dvhEstimate.SamplingCoverage,
                dvhEstimate.StandardDeviationDose, dvhEstimate.Volume, dvhEstimate.DvhCurve));
        }

        /// <summary>
        /// Creates a detached version of the plan setup.
        /// </summary>
        /// <returns>Detached copy of plan setup.</returns>
        public UPlanSetupDetached Detach()
        {
            var planSetupDetached =
                new UPlanSetupDetached(Id, NumberOfFractions, TotalPrescriptionDose, StructureSet.Detach());
            planSetupDetached.AddOptimizationObjectiveRange(_optimizationObjectives);
            planSetupDetached.AddVMATArcRange(_vmatArcs);
            foreach (var dvhEstimate in _dvhEstimates) planSetupDetached.AddDvhEstimate(dvhEstimate.Detach());
            return planSetupDetached;
        }

        /// <summary>
        /// Add VMAT arc.
        /// </summary>
        /// <param name="arc">VMAT arc.</param>
        public void AddVMATArc(VMATArc arc)
        {
            if (arc == null) throw new ArgumentNullException(nameof(arc));
            if (_vmatArcs.Any(x => x.Id == arc.Id))
                throw new ArgumentException($"There already exists an arc with ID {arc.Id}. Arc IDs must be unique.");
            _vmatArcs.Add(arc);
        }

        /// <summary>
        /// Add range of VMAT arcs.
        /// </summary>
        /// <param name="arcs">VMAT arcs.</param>
        public void AddVMATArcRange(IEnumerable<VMATArc> arcs)
        {
            if (arcs == null) throw new ArgumentNullException(nameof(arcs));
            foreach (var arc in arcs) AddVMATArc(arc);
        }
    }
}
