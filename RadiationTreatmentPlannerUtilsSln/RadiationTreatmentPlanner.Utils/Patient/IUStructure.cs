﻿using System.Windows.Media.Media3D;
using Optional;

namespace RadiationTreatmentPlanner.Utils.Patient
{
    public interface IUStructure
    {
        string Id { get; }
        Option<(double, double, double)> CenterPoint { get; }
        Option<string> DicomType { get; }
        Option<double> Volume { get; }
        Option<bool> HasSegment { get; }
        Option<MeshGeometry3D> MeshGeometry3D { get; }
    }
}
