﻿using System;
using System.ComponentModel;

namespace RadiationTreatmentPlanner.Utils.Fields
{
    public class GantryRotation
    {
        public AngleInDegree StartAngle { get; }
        public AngleInDegree FinishAngle { get; }
        public RotationDirection RotationDirection { get; }

        /// <summary>
        /// Gantry rotation with start angle, finish angle and rotation direction.
        /// </summary>
        /// <param name="startAngle">Start angle of the gantry.</param>
        /// <param name="finishAngle">Finish angle of the gantry.</param>
        /// <param name="rotationDirection">Rotation direction of the gantry.</param>
        public GantryRotation(AngleInDegree startAngle, AngleInDegree finishAngle, RotationDirection rotationDirection)
        {
            if (!Enum.IsDefined(typeof(RotationDirection), rotationDirection))
                throw new InvalidEnumArgumentException(nameof(rotationDirection), (int)rotationDirection,
                    typeof(RotationDirection));
            StartAngle = startAngle;
            FinishAngle = finishAngle;
            RotationDirection = rotationDirection;
        }

        public override string ToString()
        {
            return $"Start: {StartAngle}, Finish: {FinishAngle}, Direction: {RotationDirection}";
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as GantryRotation);
        }

        public bool Equals(GantryRotation other)
        {
            return other != null
                   && StartAngle.Equals(other.StartAngle)
                   && FinishAngle.Equals(other.FinishAngle)
                   && RotationDirection.Equals(other.RotationDirection);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(StartAngle, FinishAngle, RotationDirection);
        }
    }
}