﻿using System;

namespace RadiationTreatmentPlanner.Utils
{
    public class EmptyUnitException : Exception
    {
        public EmptyUnitException()
        {
        }

        public EmptyUnitException(string unit) : base(
            $"Empty Unit not allowed. Received: {unit}")
        {
        }
    }
}
