﻿using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Patient;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.OptimizationObjetives
{
    public static class ObjectivesFactory
    {
        public static PointObjective BuildPointObjective(string structureId,
            ObjectiveOperator objectiveOperator,
            Priority priority, UDose dose, UVolume volumeInPercent)
        {
            return new PointObjective(structureId, objectiveOperator, priority, dose, volumeInPercent);
        }

        public static EUDObjective BuildEudObjective(string structureId,
            ObjectiveOperator objectiveOperator,
            Priority priority, UDose dose, double parameterA)
        {
            return new EUDObjective(structureId, objectiveOperator, priority, dose, parameterA);
        }

        public static MeanDoseObjective BuildMeanDoseObjective(string structureId, UDose dose,
            Priority priority)
        {
            return new MeanDoseObjective(structureId, dose, priority);
        }
    }
}
