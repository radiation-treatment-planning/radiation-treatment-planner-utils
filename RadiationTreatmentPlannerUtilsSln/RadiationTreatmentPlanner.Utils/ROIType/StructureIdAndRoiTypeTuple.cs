﻿using System;

namespace RadiationTreatmentPlanner.Utils.ROIType
{
    public class StructureIdAndRoiTypeTuple
    {
        public string StructureId { get; }
        public RtRoiInterpretedType RtRoiInterpretedType { get; }

        public StructureIdAndRoiTypeTuple(string structureId, RtRoiInterpretedType rtRoiInterpretedType)
        {
            StructureId = structureId ?? throw new ArgumentNullException(nameof(structureId));
            RtRoiInterpretedType =
                rtRoiInterpretedType ?? throw new ArgumentNullException(nameof(rtRoiInterpretedType));
        }

        protected bool Equals(StructureIdAndRoiTypeTuple other)
        {
            return StructureId == other.StructureId && Equals(RtRoiInterpretedType, other.RtRoiInterpretedType);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((StructureIdAndRoiTypeTuple)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(StructureId, RtRoiInterpretedType);
        }
    }
}