﻿using System.Collections.Generic;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Point;

namespace RadiationTreatmentPlanner.Utils.Tests.Point
{
    [TestFixture]
    public class UPointsArraySorterTests
    {
        [Test]
        public void Sort_ArrayAsInput_Test()
        {
            var arrayToSort = new UPoint[]
            {
                new UPoint(2, 3),
                new UPoint(1, 2),
                new UPoint(3, 1),
                new UPoint(4, 3),
                new UPoint(2, 2)
            };

            var expectedResult = new UPoint[]
            {
                new UPoint(1, 2),
                new UPoint(2, 2),
                new UPoint(2, 3),
                new UPoint(3, 1),
                new UPoint(4, 3)
            };

            var sorter = new UPointArraySorter(new UPointComparer());
            var result = sorter.Sort(arrayToSort);

            for (var i = 0; i < result.Length; i++)
            {
                var resultPoint = result[i];
                var expectedPoint = expectedResult[i];

                Assert.AreEqual(expectedPoint, resultPoint);
            }
        }

        [Test]
        public void Sort_ListAsInput_Test()
        {
            var arrayToSort = new List<UPoint>
            {
                new UPoint(2, 3),
                new UPoint(1, 2),
                new UPoint(3, 1),
                new UPoint(4, 3),
                new UPoint(2, 2)
            };

            var expectedResult = new List<UPoint>
            {
                new UPoint(1, 2),
                new UPoint(2, 2),
                new UPoint(2, 3),
                new UPoint(3, 1),
                new UPoint(4, 3)
            };

            var sorter = new UPointArraySorter(new UPointComparer());
            var result = sorter.Sort(arrayToSort);

            for (var i = 0; i < result.Count; i++)
            {
                var resultPoint = result[i];
                var expectedPoint = expectedResult[i];

                Assert.AreEqual(expectedPoint, resultPoint);
            }
        }
    }
}
