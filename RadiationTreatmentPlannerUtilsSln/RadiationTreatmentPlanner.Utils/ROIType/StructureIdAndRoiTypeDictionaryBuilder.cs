﻿using System;
using System.Collections.Generic;

namespace RadiationTreatmentPlanner.Utils.ROIType
{
    public class StructureIdAndRoiTypeDictionaryBuilder
    {
        private readonly List<StructureIdAndRoiTypeTuple> _structureIdAndRoiTypeTuples;

        internal StructureIdAndRoiTypeDictionaryBuilder()
        {
            _structureIdAndRoiTypeTuples = new List<StructureIdAndRoiTypeTuple>();
        }

        public static StructureIdAndRoiTypeDictionaryBuilder Initialize() =>
            new StructureIdAndRoiTypeDictionaryBuilder();

        public StructureIdAndRoiTypeDictionaryBuilder WithStructureIdAndRoiType(string structureId,
            RtRoiInterpretedType rtRoiInterpretedType)
        {
            if (structureId == null) throw new ArgumentNullException(nameof(structureId));
            if (rtRoiInterpretedType == null) throw new ArgumentNullException(nameof(rtRoiInterpretedType));
            _structureIdAndRoiTypeTuples.Add(new StructureIdAndRoiTypeTuple(structureId, rtRoiInterpretedType));
            return this;
        }

        public StructureIdAndRoiTypeDictionary Build() =>
            new StructureIdAndRoiTypeDictionary(_structureIdAndRoiTypeTuples);
    }
}
