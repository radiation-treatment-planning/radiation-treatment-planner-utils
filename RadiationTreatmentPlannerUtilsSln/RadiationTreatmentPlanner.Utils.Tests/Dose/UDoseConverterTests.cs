﻿using System;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;

namespace RadiationTreatmentPlanner.Utils.Tests.Dose
{
    [TestFixture]
    public class UDoseConverterTests
    {
        [Test]
        public void ToUDose_Gy_AsDoseUnit_Test()
        {
            var doseAsString = "20.13Gy";
            Assert.AreEqual(new UDose(20.13, UDose.UDoseUnit.Gy), doseAsString.ToUDose());
        }

        [Test]
        public void ToUDose_cGy_AsDoseUnit_Test()
        {
            var doseAsString = "20.13cGy";
            Assert.AreEqual(new UDose(20.13, UDose.UDoseUnit.cGy), doseAsString.ToUDose());
        }

        [Test]
        public void ToUDose_Percent_AsDoseUnit_Test()
        {
            var doseAsString = "20.13%";
            Assert.AreEqual(new UDose(20.13, UDose.UDoseUnit.Percent), doseAsString.ToUDose());
        }

        [Test]
        public void ToUDose_Unknown_DoseUnit_Test()
        {
            var doseAsString = "20.13Unknown";
            Assert.AreEqual(new UDose(20.13, UDose.UDoseUnit.Unknown), doseAsString.ToUDose());
        }

        [Test]
        public void ToUDose_Unknown_DoseValue_Test()
        {
            var doseAsString = "a12bcGy";

            var dose = doseAsString.ToUDose();
            Assert.AreEqual(UDose.UDoseUnit.cGy, dose.Unit);
            Assert.IsNaN(dose.Value);
        }

        [Test]
        public void ToUDose_Unknown_DoseValue2_Test()
        {
            var doseAsString = "Gyabc";
            var dose = doseAsString.ToUDose();
            Assert.AreEqual(UDose.UDoseUnit.Gy, dose.Unit);
            Assert.IsNaN(dose.Value);
        }

        [Test]
        public void UDose_ImplicitOperator_Test()
        {
            var doseAsString = "20.13Gy";
            UDose dose = doseAsString;
            Assert.AreEqual(new UDose(20.13, UDose.UDoseUnit.Gy), dose);
        }

        [Test]
        public void UDose_ConvertUDoseToStringAndBack_Test()
        {
            var dose = new UDose(20.345, UDose.UDoseUnit.Gy);
            var doseAsString = dose.ToString();

            Assert.AreEqual("20.345Gy", doseAsString);
            Assert.AreEqual(dose, doseAsString.ToUDose());
        }

        [TestCase(2.0, 1.4, 12u, 0.921569)]
        [TestCase(20.0, 1.4, 12u, 18.0392)]
        [TestCase(60.0, 1.6, 12u, 110.0)]
        [TestCase(60.0, 1.6, 10u, 126.667)]
        public void UDose_PhysicalToEqd2_Test(double doseValue, double alphaOverBeta, uint numberOfFractions,
            double expectedResultValue)
        {
            var dose = new UDose(doseValue, UDose.UDoseUnit.Gy);
            var expectedResult = new UDose(expectedResultValue, UDose.UDoseUnit.Gy);
            var result = dose.PhysicalToEqd2(alphaOverBeta, numberOfFractions);
            Assert.AreEqual(expectedResult.Value, result.Value, 1e-3);
        }

        [Test]
        public void UDose_PhysicalToEqd2__ThrowArgumentOutOfRangeException_For_AlphaOverBeta_LowerEqualZeroTest()
        {
            var dose = new UDose(10, UDose.UDoseUnit.Gy);
            Assert.Throws<ArgumentOutOfRangeException>(() => dose.PhysicalToEqd2(-1, 10));
            Assert.Throws<ArgumentOutOfRangeException>(() => dose.PhysicalToEqd2(0, 10));
        }

        [Test]
        public void UDose_PhysicalToEqd2__ThrowArgumentOutOfRangeException_For_NumberOfFractions_EqualZeroTest()
        {
            var dose = new UDose(10, UDose.UDoseUnit.Gy);
            Assert.Throws<ArgumentOutOfRangeException>(() => dose.PhysicalToEqd2(1.6, 0));
        }

        [TestCase(2.0, 1.4, 12u, 2.2381)]
        [TestCase(20.0, 1.4, 12u, 43.8095)]
        [TestCase(60.0, 1.6, 12u, 247.5)]
        [TestCase(60.0, 1.6, 10u, 285.0)]
        public void UDose_PhysicalToEqd0_Test(double doseValue, double alphaOverBeta, uint numberOfFractions,
            double expectedResultValue)
        {
            var dose = new UDose(doseValue, UDose.UDoseUnit.Gy);
            var expectedResult = new UDose(expectedResultValue, UDose.UDoseUnit.Gy);
            var result = dose.PhysicalToEqd0(alphaOverBeta, numberOfFractions);
            Assert.AreEqual(expectedResult.Value, result.Value, 1e-3);
        }

        [Test]
        public void UDose_PhysicalToEqd0__ThrowArgumentOutOfRangeException_For_AlphaOverBeta_LowerEqualZero_Test()
        {
            var dose = new UDose(10, UDose.UDoseUnit.Gy);
            Assert.Throws<ArgumentOutOfRangeException>(() => dose.PhysicalToEqd0(-1, 10));
            Assert.Throws<ArgumentOutOfRangeException>(() => dose.PhysicalToEqd0(0, 10));
        }

        [Test]
        public void UDose_PhysicalToEqd0__ThrowArgumentOutOfRangeException_For_NumberOfFractions_EqualZero_Test()
        {
            var dose = new UDose(10, UDose.UDoseUnit.Gy);
            Assert.Throws<ArgumentOutOfRangeException>(() => dose.PhysicalToEqd0(1.6, 0));
        }

        [TestCase(30.0, 1.4, 72.8571)]
        [TestCase(30.0, 1.6, 67.5)]
        [TestCase(60.0, 1.6, 135)]
        public void UDose_Eqd2ToEqd0_Test(double doseValue, double alphaOverBeta, double expectedResultValue)
        {
            var dose = new UDose(doseValue, UDose.UDoseUnit.Gy);
            var expectedResult = new UDose(expectedResultValue, UDose.UDoseUnit.Gy);
            var result = dose.Eqd2ToEqd0(alphaOverBeta);
            Assert.AreEqual(expectedResult.Value, result.Value, 1e-3);
        }

        [Test]
        public void UDose_Eqd2ToEqd0__ThrowArgumentOutOfRangeException_For_AlphaOverBeta_LowerEqualZero_Test()
        {
            var dose = new UDose(10, UDose.UDoseUnit.Gy);
            Assert.Throws<ArgumentOutOfRangeException>(() => dose.Eqd2ToEqd0(0));
            Assert.Throws<ArgumentOutOfRangeException>(() => dose.Eqd2ToEqd0(-1));
        }

        [TestCase(30.0, 1.4, 12.3529)]
        [TestCase(30.0, 1.6, 13.333)]
        [TestCase(60.0, 1.6, 26.666)]
        public void UDose_Eqd0ToEqd2_Test(double doseValue, double alphaOverBeta, double expectedResultValue)
        {
            var dose = new UDose(doseValue, UDose.UDoseUnit.Gy);
            var expectedResult = new UDose(expectedResultValue, UDose.UDoseUnit.Gy);
            var result = dose.Eqd0ToEqd2(alphaOverBeta);
            Assert.AreEqual(expectedResult.Value, result.Value, 1e-3);
        }

        [Test]
        public void UDose_Eqd0ToEqd2__ThrowArgumentOutOfRangeException_For_AlphaOverBeta_LowerEqualZero_Test()
        {
            var dose = new UDose(10, UDose.UDoseUnit.Gy);
            Assert.Throws<ArgumentOutOfRangeException>(() => dose.Eqd0ToEqd2(0));
            Assert.Throws<ArgumentOutOfRangeException>(() => dose.Eqd0ToEqd2(-1));
        }
    }
}
