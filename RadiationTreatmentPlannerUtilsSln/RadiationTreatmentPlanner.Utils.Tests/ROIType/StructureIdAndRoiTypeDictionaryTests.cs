﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.ROIType;

namespace RadiationTreatmentPlanner.Utils.Tests.ROIType

{
    [TestFixture]
    public class StructureIdAndRoiTypeDictionaryTests
    {
        [Test]
        public void ContainsStructureId_ReturnTrueIfTrue_Test()
        {
            var dict = StructureIdAndRoiTypeDictionaryBuilder.Initialize()
                .WithStructureIdAndRoiType("Bowel Small", RtRoiInterpretedTypePresets.SMALL_BOWEL())
                .Build();

            Assert.IsTrue(dict.ContainsStructureId("Bowel Small"));
        }

        [Test]
        public void ContainsStructureId_ReturnFalseIfFalse_Test()
        {
            var dict = StructureIdAndRoiTypeDictionaryBuilder.Initialize()
                .WithStructureIdAndRoiType("Bowel Small", RtRoiInterpretedTypePresets.SMALL_BOWEL())
                .Build();

            Assert.IsFalse(dict.ContainsStructureId("PTV"));
        }

        [Test]
        public void ContainsStructureId_ThrowArgumentNullException_ForNullArgument_Test()
        {
            var dict = StructureIdAndRoiTypeDictionaryBuilder.Initialize()
                .WithStructureIdAndRoiType("Bowel Small", RtRoiInterpretedTypePresets.SMALL_BOWEL())
                .Build();

            Assert.Throws<ArgumentNullException>(() => dict.ContainsStructureId(null));
        }

        [Test]
        public void TryGetROITypeOfStructureWithId_ThrowArgumentNullExceptionForNullArguments_Test()
        {
            var dict = StructureIdAndRoiTypeDictionaryBuilder.Initialize()
                .WithStructureIdAndRoiType("Bowel Small", RtRoiInterpretedTypePresets.SMALL_BOWEL())
                .Build();

            RtRoiInterpretedType resultType;
            Assert.Throws<ArgumentNullException>(() =>
                dict.TryGetROITypeOfStructureWithId(null, out resultType));
        }

        [Test]
        public void TryGetROITypeOfStructureWithId_ReturnTrueIfDictContainsStructure_Test()
        {
            var dict = StructureIdAndRoiTypeDictionaryBuilder.Initialize()
                .WithStructureIdAndRoiType("Bowel Small", RtRoiInterpretedTypePresets.SMALL_BOWEL())
                .Build();

            RtRoiInterpretedType resultType;
            var result = dict.TryGetROITypeOfStructureWithId("Bowel Small", out resultType);

            Assert.AreEqual(RtRoiInterpretedTypePresets.SMALL_BOWEL(), resultType);
            Assert.IsTrue(result);
        }

        [Test]
        public void TryGetROITypeOfStructureWithId_ReturnFalseIfDictDoesNotContainStructure_Test()
        {
            var dict = StructureIdAndRoiTypeDictionaryBuilder.Initialize()
                .WithStructureIdAndRoiType("Bowel Small", RtRoiInterpretedTypePresets.SMALL_BOWEL())
                .Build();

            RtRoiInterpretedType resultType;
            var result = dict.TryGetROITypeOfStructureWithId("PTV", out resultType);

            Assert.IsNull(resultType);
            Assert.IsFalse(result);
        }

        [Test]
        public void ContainsROIType_ReturnTrueIfTrue_Test()
        {
            var dict = StructureIdAndRoiTypeDictionaryBuilder.Initialize()
                .WithStructureIdAndRoiType("Bowel Small", RtRoiInterpretedTypePresets.SMALL_BOWEL())
                .Build();

            Assert.IsTrue(dict.ContainsROIType(RtRoiInterpretedTypePresets.SMALL_BOWEL()));
        }

        [Test]
        public void ContainsROIType_ReturnFalseIfFalse_Test()
        {
            var dict = StructureIdAndRoiTypeDictionaryBuilder.Initialize()
                .WithStructureIdAndRoiType("Bowel Small", RtRoiInterpretedTypePresets.SMALL_BOWEL())
                .Build();

            Assert.IsFalse(dict.ContainsROIType(RtRoiInterpretedTypePresets.PTV_DOM()));
        }

        [Test]
        public void ContainsROIType_ThrowArgumentNullException_ForNullArgument_Test()
        {
            var dict = StructureIdAndRoiTypeDictionaryBuilder.Initialize()
                .WithStructureIdAndRoiType("Bowel Small", RtRoiInterpretedTypePresets.SMALL_BOWEL())
                .Build();

            Assert.Throws<ArgumentNullException>(() => dict.ContainsROIType(null));
        }

        [Test]
        public void TryGetStructureIdWithROIType_ReturnTrueIfDictContainsStructure_Test()
        {
            var dict = StructureIdAndRoiTypeDictionaryBuilder.Initialize()
                .WithStructureIdAndRoiType("Bowel Small", RtRoiInterpretedTypePresets.SMALL_BOWEL())
                .Build();

            string resultType;
            var result = dict.TryGetStructureIdWithROIType(RtRoiInterpretedTypePresets.SMALL_BOWEL(), out resultType);

            Assert.AreEqual("Bowel Small", resultType);
            Assert.IsTrue(result);
        }

        [Test]
        public void TryGetStructureIdWithROIType_ReturnFalseIfDictDoesNotContainStructure_Test()
        {
            var dict = StructureIdAndRoiTypeDictionaryBuilder.Initialize()
                .WithStructureIdAndRoiType("Bowel Small", RtRoiInterpretedTypePresets.SMALL_BOWEL())
                .Build();

            string resultType;
            var result = dict.TryGetStructureIdWithROIType(RtRoiInterpretedTypePresets.PTV(), out resultType);

            Assert.IsNull(resultType);
            Assert.IsFalse(result);
        }

        [Test]
        public void Equals_ReturnTrueIfTrue_Test()
        {
            var dict = StructureIdAndRoiTypeDictionaryBuilder.Initialize()
                .WithStructureIdAndRoiType("Bowel Small", RtRoiInterpretedTypePresets.SMALL_BOWEL())
                .Build();
            var dict2 = StructureIdAndRoiTypeDictionaryBuilder.Initialize()
                .WithStructureIdAndRoiType("Bowel Small", RtRoiInterpretedTypePresets.SMALL_BOWEL())
                .Build();

            Assert.AreEqual(dict, dict2);
        }

        [Test]
        public void Equals_ReturnFalseIfFalse_Test()
        {
            var dict = StructureIdAndRoiTypeDictionaryBuilder.Initialize()
                .WithStructureIdAndRoiType("Small Bowel", RtRoiInterpretedTypePresets.SMALL_BOWEL())
                .Build();
            var dict2 = StructureIdAndRoiTypeDictionaryBuilder.Initialize()
                .WithStructureIdAndRoiType("Bowel Small", RtRoiInterpretedTypePresets.SMALL_BOWEL())
                .Build();

            Assert.AreNotEqual(dict, dict2);
        }

        [Test]
        public void GetAllROITypes_Test()
        {
            var dict = StructureIdAndRoiTypeDictionaryBuilder.Initialize()
                .WithStructureIdAndRoiType("Small Bowel", RtRoiInterpretedTypePresets.SMALL_BOWEL())
                .WithStructureIdAndRoiType("PTV", RtRoiInterpretedTypePresets.PTV())
                .Build();

            var expectedResult = new List<RtRoiInterpretedType>()
                { RtRoiInterpretedTypePresets.SMALL_BOWEL(), RtRoiInterpretedTypePresets.PTV() };
            var result = dict.GetAllROITypes();

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void GetAllStructureIds_Test()
        {
            var dict = StructureIdAndRoiTypeDictionaryBuilder.Initialize()
                .WithStructureIdAndRoiType("Small Bowel", RtRoiInterpretedTypePresets.SMALL_BOWEL())
                .WithStructureIdAndRoiType("PTV", RtRoiInterpretedTypePresets.PTV())
                .Build();

            var expectedResult = new List<string> { "Small Bowel", "PTV" };
            var result = dict.GetAllStructureIds();

            Assert.AreEqual(expectedResult, result);
        }
    }
}
