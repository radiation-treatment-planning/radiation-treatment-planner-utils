﻿using System;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.OptimizationObjetives;

namespace RadiationTreatmentPlanner.Utils.Tests.OptimizationObjectives
{
    [TestFixture]
    public class PriorityTests
    {
        [TestCase(0)]
        [TestCase(10)]
        [TestCase(100)]
        [TestCase(500)]
        [TestCase(1000)]
        public void Constructor_Test(int value)
        {
            var priority = Priority.Factory.BuildPriority(value);
            Assert.AreEqual(value, priority.Value);
        }

        [TestCase(-1)]
        [TestCase(1001)]
        public void Constructor_ThrowArgumentExceptionForInvalidValue_Test(int value)
        {
            Assert.Throws<ArgumentException>(() => Priority.Factory.BuildPriority(value));
        }

        [Test]
        public void EqualsOperator_ReturnTrueIfEqual_Test()
        {
            var value = 100;
            Assert.AreEqual(Priority.Factory.BuildPriority(value), Priority.Factory.BuildPriority(value));
        }

        [Test]
        public void EqualsOperator_ReturnFalseIfNotEqual_Test()
        {
            Assert.AreNotEqual(Priority.Factory.BuildPriority(100), Priority.Factory.BuildPriority(500));
        }

        [Test]
        public void BuildMaximumPriority_Test()
        {
            Assert.AreEqual(Priority.Factory.BuildPriority(1000), Priority.Factory.BuildMaximumPriority());
        }
    }
}
