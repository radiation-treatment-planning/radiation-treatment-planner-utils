﻿Version DVH exporter: 1.0.1
Patient PIZ: 12345678
Plan: AutoPlan
Structure: OAR
TotalVolume: 61.8671054324199 cm³
Coverage: 1
Sampling Coverage: 1.0
Max dose: 1000 cGy
Min dose: 0 cGy
Median dose: 500 cGy
Standard deviation dose: 500 cGy
Prescription dose: 800 cGy
Number of fractions: 12

Dose [cGy]	Volume [%]
0	100
500	30
1000	1