﻿using System;

namespace RadiationTreatmentPlanner.Utils.Dose
{

    [Serializable]
    public class NegativeDoseException : Exception
    {
        public NegativeDoseException()
        {
        }

        public NegativeDoseException(double doseValue) : base(
            $"Negative number not allowed. Received: {doseValue}")
        {
        }
    }
}
