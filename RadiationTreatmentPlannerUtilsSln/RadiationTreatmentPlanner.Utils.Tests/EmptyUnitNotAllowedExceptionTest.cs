﻿using NUnit.Framework;

namespace RadiationTreatmentPlanner.Utils.Tests
{
    [TestFixture]
    public class EmptyUnitNotAllowedExceptionTest
    {
        [Test]
        public void EmptyUnitNotAllowedException_OneInputArgument_Test()
        {
            var unit = "";
            Assert.Throws<EmptyUnitException>(() => ThrowEmptyUnitNotAllowedException(unit));
        }

        private void ThrowEmptyUnitNotAllowedException(string unit)
        {
            if (unit == "")
                throw new EmptyUnitException(unit);
        }
    }
}
