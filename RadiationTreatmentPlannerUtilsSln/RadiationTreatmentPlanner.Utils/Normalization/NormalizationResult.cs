﻿using System;
using System.Collections.Generic;
using System.Linq;
using RadiationTreatmentPlanner.Utils.DVH;

namespace RadiationTreatmentPlanner.Utils.Normalization
{
    public class NormalizationResult
    {
        private readonly UDvhEstimateDetached[] _normalizedDvhEstimates;
        public IEnumerable<UDvhEstimateDetached> NormalizedDvhEstimates => _normalizedDvhEstimates;
        public double NormalizationScore { get; }

        public NormalizationResult(IEnumerable<UDvhEstimateDetached> normalizedDvhEstimates, double normalizationScore)
        {
            if (normalizedDvhEstimates == null) throw new ArgumentNullException(nameof(normalizedDvhEstimates));
            _normalizedDvhEstimates = normalizedDvhEstimates.ToArray();
            NormalizationScore = normalizationScore;
        }
    }
}