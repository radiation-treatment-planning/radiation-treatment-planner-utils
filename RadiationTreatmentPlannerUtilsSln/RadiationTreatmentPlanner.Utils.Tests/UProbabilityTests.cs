﻿using System;
using NUnit.Framework;

namespace RadiationTreatmentPlanner.Utils.Tests
{
    [TestFixture]
    public class UProbabilityTests
    {
        [TestCase(0.0)]
        [TestCase(0.1)]
        [TestCase(0.6)]
        [TestCase(1.0)]
        public void CreateNewUProbability_Test(double value)
        {
            var probability = new UProbability(value);
            Assert.AreEqual(value, probability.Value);
        }

        [TestCase(-1.0)]
        [TestCase(2.0)]
        public void CreateNotAcceptableProbability_Test(double value)
        {
            Assert.Throws<ArgumentException>(() => new UProbability(value));
        }

        [Test]
        public void CompareTwoObjectsWithSameValueReturnsTrue_Test()
        {
            var probability1 = new UProbability(0.5);
            var probability2 = new UProbability(0.5);

            Assert.AreEqual(probability1, probability2);

        }

    }
}
