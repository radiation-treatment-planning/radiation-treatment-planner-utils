﻿using System;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Fields;

namespace RadiationTreatmentPlanner.Utils.Tests.Fields
{
    [TestFixture]
    public class VMATArcTests
    {
        [Test]
        public void Constructor_Test()
        {
            var id = "ARC1";
            var gantryRotation = new GantryRotation(new AngleInDegree(10), new AngleInDegree(170), RotationDirection.Clockwise);
            var collimatorRotation = new AngleInDegree(35);
            var couchRotation = new AngleInDegree(0);
            var result = new VMATArc(id, gantryRotation, collimatorRotation, couchRotation);

            Assert.AreEqual(id, result.Id);
            Assert.AreEqual(gantryRotation, result.GantryRotation);
            Assert.AreEqual(collimatorRotation, result.CollimatorRotation);
            Assert.AreEqual(couchRotation, result.CouchRotation);
        }

        [Test]
        public void Constructor_ThrowArgumentNullExceptions_Test()
        {
            var id = "ARC1";
            var gantryRotation = new GantryRotation(new AngleInDegree(10), new AngleInDegree(170), RotationDirection.Clockwise);
            var collimatorRotation = new AngleInDegree(35);
            var couchRotation = new AngleInDegree(0);

            Assert.Throws<ArgumentNullException>(() =>
                new VMATArc(null, gantryRotation, collimatorRotation, couchRotation));
            Assert.Throws<ArgumentNullException>(() =>
                new VMATArc(id, null, collimatorRotation, couchRotation));
        }

        [Test]
        public void Equals_Test()
        {
            var gantryRotation = new GantryRotation(new AngleInDegree(10), new AngleInDegree(170), RotationDirection.Clockwise);
            var collimatorRotation = new AngleInDegree(35);
            var couchRotation = new AngleInDegree(0);

            var arc1 = new VMATArc("ARC1", gantryRotation, collimatorRotation, couchRotation);
            var arc2 = new VMATArc("ARC1", gantryRotation, collimatorRotation, couchRotation);
            var arc3 = new VMATArc("ARC3", gantryRotation, collimatorRotation, couchRotation);
            var arc4 = new VMATArc("ARC1", gantryRotation, collimatorRotation, new AngleInDegree(10));

            Assert.AreEqual(arc1, arc2);
            Assert.AreNotEqual(arc1, arc3);
            Assert.AreNotEqual(arc1, arc4);
        }
    }
}
