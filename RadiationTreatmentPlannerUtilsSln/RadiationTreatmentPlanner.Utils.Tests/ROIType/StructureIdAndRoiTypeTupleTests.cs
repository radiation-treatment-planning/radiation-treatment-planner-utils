﻿using System;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.ROIType;

namespace RadiationTreatmentPlanner.Utils.Tests.ROIType
{
    [TestFixture]
    public class StructureIdAndRoiTypeTupleTests
    {
        [Test]
        public void Constructor_Test()
        {
            var result = new StructureIdAndRoiTypeTuple("GTV", RtRoiInterpretedTypePresets.GTV());

            Assert.AreEqual("GTV", result.StructureId);
            Assert.AreEqual(RtRoiInterpretedTypePresets.GTV(), result.RtRoiInterpretedType);
        }

        [Test]
        public void Constructor_ThrowArgumentNullExceptionForNullArguments_Test()
        {
            Assert.Throws<ArgumentNullException>(() =>
                new StructureIdAndRoiTypeTuple("GTV", null));
            Assert.Throws<ArgumentNullException>(() =>
                new StructureIdAndRoiTypeTuple(null, RtRoiInterpretedTypePresets.GTV()));
        }

        [Test]
        public void Equals_Test()
        {
            var tuple1 = new StructureIdAndRoiTypeTuple("GTV", RtRoiInterpretedTypePresets.GTV());
            var tuple2 = new StructureIdAndRoiTypeTuple("GTV", RtRoiInterpretedTypePresets.GTV());

            Assert.AreEqual(tuple1, tuple2);
        }
    }
}
