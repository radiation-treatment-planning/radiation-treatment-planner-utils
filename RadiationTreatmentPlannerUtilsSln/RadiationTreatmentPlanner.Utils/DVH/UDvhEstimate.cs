﻿using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Patient;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.DVH
{
    public class UDvhEstimate
    {
        public UPlanSetup PlanSetup { get; }
        public UStructure Structure { get; }
        public double Coverage { get; }
        public UDose MinDose { get; }
        public UDose MeanDose { get; }
        public UDose MedianDose { get; }
        public UDose MaxDose { get; }
        public double SamplingCoverage { get; }
        public UDose StandardDeviationDose { get; }
        public UVolume Volume { get; }
        public DVHCurve DvhCurve { get; }

        public UDvhEstimate(UPlanSetup planSetup, UStructure structure, double coverage, UDose minDose, UDose meanDose, UDose medianDose, UDose maxDose,
            double samplingCoverage, UDose standardDeviationDose, UVolume volume, DVHCurve dvhCurve)
        {
            PlanSetup = planSetup;
            Structure = structure;
            Coverage = coverage;
            MinDose = minDose;
            MeanDose = meanDose;
            MedianDose = medianDose;
            MaxDose = maxDose;
            SamplingCoverage = samplingCoverage;
            StandardDeviationDose = standardDeviationDose;
            Volume = volume;
            DvhCurve = dvhCurve;
        }

        public UDvhEstimateDetached Detach()
        {
            return new UDvhEstimateDetached(PlanSetup.Id, Structure.Id, Coverage, MinDose, MeanDose, MedianDose,
                MaxDose, SamplingCoverage, StandardDeviationDose, Volume, DvhCurve);
        }
    }
}
