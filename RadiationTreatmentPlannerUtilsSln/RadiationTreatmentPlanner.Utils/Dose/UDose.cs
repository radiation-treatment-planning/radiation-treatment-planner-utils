﻿using System;
using System.ComponentModel;

namespace RadiationTreatmentPlanner.Utils.Dose
{
    public class UDose
    {
        public enum UDoseUnit
        {
            [Description("Gy")] Gy = 0,
            [Description("cGy")] cGy = 1,
            [Description("%")] Percent = 2,
            [Description("Unknown")] Unknown = -1
        }

        public UDose(double value, UDoseUnit unit)
        {
            Value = value;
            Unit = unit;

            CheckInputs();
        }

        public double Value { get; }
        public UDoseUnit Unit { get; }

        private void CheckInputs()
        {
            if (Value < 0) throw new NegativeDoseException(Value);
        }

        public static implicit operator UDose(string input)
        {
            return input.ToUDose();
        }

        public override string ToString()
        {
            return $"{Value.ToString(UStringHelper.GetDoubleAndStringFormatProvider())}{Unit.ToDescription()}";
        }

        public string ToString(uint numberOfFloatingPointDigits)
        {
            var doseValue = Math.Round(Value, (int)numberOfFloatingPointDigits);
            return $"{doseValue.ToString(UStringHelper.GetDoubleAndStringFormatProvider())}{Unit.ToDescription()}";
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as UDose);
        }
        public bool Equals(UDose other)
        {
            return other != null &&
                   Math.Abs(Value - other.Value) < 0.0001 &&
                   Unit == other.Unit;
        }
    }
}
