﻿using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Point;

namespace RadiationTreatmentPlanner.Utils.Tests.Point
{
    [TestFixture]
    public class UPointTests
    {
        [Test]
        public void EqualOperator_Test()
        {
            var a = new UPoint(1, 2);
            var b = new UPoint(1, 2);

            Assert.AreEqual(true, a == b);
        }

        [TestCase(1, 0)]
        [TestCase(0, 2)]
        [TestCase(0, 0)]
        public void NotEqualOperator_Test(double x, double y)
        {
            var a = new UPoint(1, 2);
            var b = new UPoint(x, y);

            Assert.AreEqual(true, a != b);
        }

        [TestCase(1, 1, 2)]
        [TestCase(2, 2, 4)]
        [TestCase(0.5, 0.5, 1)]
        [TestCase(-0.5, -0.5, -1)]
        public void ScalarMultiplicationOperator_Test(double scalar, double expectedX, double expectedY)
        {
            var a = new UPoint(1, 2);
            var expectedResult = new UPoint(expectedX, expectedY);

            Assert.AreEqual(true, a * scalar == expectedResult);
        }

        [TestCase(1, 1, -1)]
        [TestCase(1, 2, 0)]
        [TestCase(0.5, 0.5, -0.5)]
        [TestCase(-0.5, -0.5, 0.5)]
        public void UPointMultiplicationOperator_Test(double x, double y, double expectedResult)
        {
            var a = new UPoint(1, 2);
            var b = new UPoint(x, y);

            Assert.AreEqual(expectedResult, a * b);
        }

        [TestCase(0, 1, 1, 3)]
        [TestCase(1, -2, 2, 0)]
        [TestCase(0.5, 0.5, 1.5, 2.5)]
        public void UPointAdditionOperator_Test(double x, double y, double expectedX, double expectedY)
        {
            var a = new UPoint(1, 2);
            var b = new UPoint(x, y);
            var expectedResult = new UPoint(expectedX, expectedY);

            Assert.AreEqual(true, (a + b) == expectedResult);
        }

        [TestCase(0, 1, 1, 1)]
        [TestCase(1, -2, 0, 4)]
        [TestCase(0.5, 0.5, 0.5, 1.5)]
        public void UPointSubtractionOperator_Test(double x, double y, double expectedX, double expectedY)
        {
            var a = new UPoint(1, 2);
            var b = new UPoint(x, y);
            var result = a - b;

            Assert.AreEqual(result.X, expectedX);
            Assert.AreEqual(result.Y, expectedY);
        }

        [TestCase(0.5, 0.5, 1.0, 1.0, true)]
        [TestCase(0.5, 0.5, 0.5, 1.0, true)]
        [TestCase(0.5, 0.5, 1.0, 0.5, true)]
        [TestCase(0.5, 0.5, 0.5, 0.5, false)]
        [TestCase(0.5, 1.0, 0.5, 0.5, false)]
        [TestCase(1.0, 1.0, 0.5, 0.5, false)]

        public void Dominates_Tests(double ax, double ay, double bx, double by, bool expectedResult)
        {
            var a = new UPoint(ax, ay);
            var b = new UPoint(bx, by);

            Assert.AreEqual(expectedResult, UPoint.IsDominating(a, b));

        }
    }
}
