﻿using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;

namespace RadiationTreatmentPlanner.Utils.Tests.Dose
{
    [TestFixture]
    public class UDoseTests
    {
        [Test]
        public void NegativeDose_Test()
        {
            Assert.Throws<NegativeDoseException>(() => new UDose(-1.0, UDose.UDoseUnit.Gy));
        }

        [Test]
        public void CreateUDose_Test()
        {
            var dose = new UDose(1.0, UDose.UDoseUnit.Gy);
            Assert.AreEqual(1.0, dose.Value);
            Assert.AreEqual(UDose.UDoseUnit.Gy, dose.Unit);
        }

        [Test]
        public void AreEqual_Test()
        {
            var dose1 = new UDose(80, UDose.UDoseUnit.Gy);
            var dose2 = new UDose(80, UDose.UDoseUnit.Gy);
            Assert.AreEqual(dose1, dose2);
        }

        [Test]
        public void ToString_Test()
        {
            var dose = new UDose(3.141592, UDose.UDoseUnit.Gy);
            var dose2 = new UDose(3.141592, UDose.UDoseUnit.cGy);
            var dose3 = new UDose(3.141592, UDose.UDoseUnit.Percent);
            var dose4 = new UDose(3.141592, UDose.UDoseUnit.Unknown);

            Assert.AreEqual($"3.141592Gy", dose.ToString());
            Assert.AreEqual($"3.141592cGy", dose2.ToString());
            Assert.AreEqual($"3.141592%", dose3.ToString());
            Assert.AreEqual($"3.141592Unknown", dose4.ToString());
        }

        [TestCase(0u, "3Gy")]
        [TestCase(1u, "3.1Gy")]
        [TestCase(2u, "3.14Gy")]
        [TestCase(3u, "3.142Gy")]
        [TestCase(4u, "3.1416Gy")]
        [TestCase(5u, "3.14159Gy")]
        [TestCase(6u, "3.141592Gy")]
        [TestCase(7u, "3.141592Gy")]
        public void ToString_NumberOfFloatingPointDigits_AsParameter_Test(uint numberOfFloatingPointDigits, string expectedResult)
        {
            var dose = new UDose(3.141592, UDose.UDoseUnit.Gy);

            Assert.AreEqual(expectedResult, dose.ToString(numberOfFloatingPointDigits));
        }
    }
}
