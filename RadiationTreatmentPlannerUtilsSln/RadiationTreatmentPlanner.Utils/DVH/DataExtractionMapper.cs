﻿using System;
using System.Collections.Generic;

namespace RadiationTreatmentPlanner.Utils.DVH
{
    public class DataExtractionMapper
    {
        private readonly List<Tuple<Enum, Func<string, string[], dynamic>>> _mapping
            = new List<Tuple<Enum, Func<string, string[], dynamic>>>();

        public void Map(Enum key, Func<string, string[], dynamic> func)
        {
            _mapping.Add(new Tuple<Enum, Func<string, string[], dynamic>>(key, func));
        }

        public IEnumerable<Tuple<Enum, Func<string, string[], dynamic>>> GetAll()
        {
            return _mapping;
        }
    }
}
