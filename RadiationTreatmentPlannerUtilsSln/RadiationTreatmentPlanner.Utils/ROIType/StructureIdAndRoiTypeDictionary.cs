﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RadiationTreatmentPlanner.Utils.ROIType
{
    public class StructureIdAndRoiTypeDictionary
    {
        private readonly Dictionary<string, RtRoiInterpretedType> _dictWithStructureIdAsKey;
        private readonly Dictionary<RtRoiInterpretedType, string> _dictWithRoiTypeAsKey;

        /// <summary>
        /// A dictionary that maps structure IDs to RT ROI interpreted types.
        /// </summary>
        /// <param name="structureIdAndRoiTypeTuples"></param>
        internal StructureIdAndRoiTypeDictionary(
            IEnumerable<StructureIdAndRoiTypeTuple> structureIdAndRoiTypeTuples)
        {
            if (structureIdAndRoiTypeTuples == null)
                throw new ArgumentNullException(nameof(structureIdAndRoiTypeTuples));
            _dictWithStructureIdAsKey = new Dictionary<string, RtRoiInterpretedType>();
            _dictWithRoiTypeAsKey = new Dictionary<RtRoiInterpretedType, string>();
            PopulateDictionaries(structureIdAndRoiTypeTuples);
        }

        private void PopulateDictionaries(IEnumerable<StructureIdAndRoiTypeTuple> structureIdAndRoiTypeTuples)
        {
            foreach (var tuple in structureIdAndRoiTypeTuples)
            {
                _dictWithStructureIdAsKey.Add(tuple.StructureId, tuple.RtRoiInterpretedType);
                _dictWithRoiTypeAsKey.Add(tuple.RtRoiInterpretedType, tuple.StructureId);
            }
        }

        public bool ContainsStructureId(string structureId)
        {
            if (structureId == null) throw new ArgumentNullException(nameof(structureId));
            return _dictWithStructureIdAsKey.ContainsKey(structureId);
        }

        public bool TryGetROITypeOfStructureWithId(string structureId, out RtRoiInterpretedType rtRoiType)
        {
            if (structureId == null) throw new ArgumentNullException(nameof(structureId));
            return _dictWithStructureIdAsKey.TryGetValue(structureId, out rtRoiType);
        }

        public bool ContainsROIType(RtRoiInterpretedType rtRoiInterpretedType)
        {
            if (rtRoiInterpretedType == null) throw new ArgumentNullException(nameof(rtRoiInterpretedType));
            return _dictWithRoiTypeAsKey.ContainsKey(rtRoiInterpretedType);
        }

        public bool TryGetStructureIdWithROIType(RtRoiInterpretedType rtRoiInterpretedType, out string structureId)
            => _dictWithRoiTypeAsKey.TryGetValue(rtRoiInterpretedType, out structureId);

        public IEnumerable<RtRoiInterpretedType> GetAllROITypes() => _dictWithRoiTypeAsKey.Keys;

        public IEnumerable<string> GetAllStructureIds() => _dictWithStructureIdAsKey.Keys;

        protected bool Equals(StructureIdAndRoiTypeDictionary other)
        {
            return Equals(_dictWithStructureIdAsKey, other._dictWithStructureIdAsKey) &&
                   Equals(_dictWithRoiTypeAsKey, other._dictWithRoiTypeAsKey);
        }

        protected bool Equals(Dictionary<string, RtRoiInterpretedType> left,
            Dictionary<string, RtRoiInterpretedType> right)
        {
            var leftKeys = left.Keys.ToList();
            var rightKeys = right.Keys.ToList();
            if (leftKeys.Count != rightKeys.Count) return false;
            foreach (var key in leftKeys)
                if (!rightKeys.Contains(key))
                    return false;
            foreach (var key in leftKeys)
            {
                RtRoiInterpretedType leftValue;
                RtRoiInterpretedType rightValue;
                left.TryGetValue(key, out leftValue);
                right.TryGetValue(key, out rightValue);
                if (!Equals(leftValue, rightValue)) return false;
            }

            return true;
        }

        protected bool Equals(Dictionary<RtRoiInterpretedType, string> left,
            Dictionary<RtRoiInterpretedType, string> right)
        {
            var leftKeys = left.Keys.ToList();
            var rightKeys = right.Keys.ToList();
            if (leftKeys.Count != rightKeys.Count) return false;
            foreach (var key in leftKeys)
                if (!rightKeys.Contains(key))
                    return false;
            foreach (var key in leftKeys)
            {
                string leftValue;
                string rightValue;
                left.TryGetValue(key, out leftValue);
                right.TryGetValue(key, out rightValue);
                if (!Equals(leftValue, rightValue)) return false;
            }

            return true;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((StructureIdAndRoiTypeDictionary)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_dictWithStructureIdAsKey, _dictWithRoiTypeAsKey);
        }
    }
}
