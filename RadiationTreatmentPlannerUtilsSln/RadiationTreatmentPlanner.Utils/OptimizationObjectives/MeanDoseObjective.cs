﻿using System;
using RadiationTreatmentPlanner.Utils.Dose;

namespace RadiationTreatmentPlanner.Utils.OptimizationObjetives
{
    public class MeanDoseObjective : IObjective
    {
        public string StructureId { get; }
        public UDose Dose { get; }
        public Priority Priority { get; }

        public MeanDoseObjective(string structureId, UDose dose, Priority priority)
        {
            StructureId = structureId ?? throw new ArgumentNullException(nameof(structureId));
            Dose = dose;
            Priority = priority;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as MeanDoseObjective);
        }

        public bool Equals(MeanDoseObjective other)
        {
            return other != null
                   && StructureId == other.StructureId
                   && Priority.Equals(other.Priority)
                   && Dose.Equals(other.Dose);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(StructureId, Dose, Priority);
        }
    }
}
