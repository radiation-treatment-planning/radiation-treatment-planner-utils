﻿using System;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Patient;

namespace RadiationTreatmentPlanner.Utils.OptimizationObjetives
{
    public class EUDObjective : IObjective
    {
        public string StructureId { get; }
        public ObjectiveOperator ObjectiveOperator { get; }
        public Priority Priority { get; }
        public UDose Dose { get; }
        public double ParameterA { get; }
        public EUDObjective(string structureId,
            ObjectiveOperator objectiveOperator,
            Priority priority, UDose dose, double parameterA)
        {
            StructureId = structureId ?? throw new ArgumentNullException(nameof(structureId));
            ObjectiveOperator = objectiveOperator;
            Priority = priority;
            Dose = dose;
            ParameterA = parameterA;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as EUDObjective);
        }

        public bool Equals(EUDObjective other)
        {
            return other != null
                   && StructureId == other.StructureId
                   && ObjectiveOperator == other.ObjectiveOperator
                   && Priority.Equals(other.Priority)
                   && Dose.Equals(other.Dose)
                   && ParameterA.Equals(other.ParameterA);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(StructureId, ObjectiveOperator, Priority, Dose, ParameterA);
        }
    }
}
