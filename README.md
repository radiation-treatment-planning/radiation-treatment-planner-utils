# Radiation Treatment Planner Utils
A solution that contains core utilities for radiation treatment planning. It holds custom structs and classes oriented at [Eclipse Scripting API](https://github.com/VarianAPIs/Varian-Code-Samples/wiki) for creating patient data independently of ESAPI.

## Installation
Install it via [NuGet](https://www.nuget.org/packages/RadiationTreatmentPlanner.Utils) or build it from source.

## Usage
For example how to use see the `RadiationTreatmentPlanner.Utils.Tests` project.

### Patient
In a nutshell the foundational object is the patient object which can be created with

```
var patient = new UPatient("Custom Id", "Max Musterfrau");
```

then there are classes that are strictly attached to the patient and can only be created with a patient object. Detached objects of these classes can be created independently of a patient:

```
var patient = new UPatient("Custom Id", "Max Musterfrau");
var structureSetDetached = new UStructureSetDetached("ss"); // Detached structure set. Exists without connection to patient object.

// Create attached structure set that can only exist with patient object.
var structureSetv1 = patient.AddStructureSet(structureSetDetached); // Possibility 1.
var structureSetv2 = patient.AddStructureSetWithId("ss"); // Possibility 2.
var structureSetDetachedAgain = structureSetv1.Detach(); // Returns UStructureSetDetached.
```

### Dose Volume Histograms
Dose volume histograms are called `DVHCurve`. They consist of a `UDose` and `UVolume` tuples. Dose and volume objects can have different units and both can't have negative values:

```
var dose = new UDose(1.0, UDose.UDoseUnit.Gy); // Gray.
dose = new UDose(1.0, UDose.UDoseUnit.cGy); // Centigray.
dose = new UDose(1.0, UDose.UDoseUnit.Percent);
dose = new UDose(1.0, UDose.UDoseUnit.Unknown)

var volume = new UVolume(1.0, UVolume.VolumeUnit.ccm); // Cubic centimeters.
volume = new UVolume(1.0, UVolume.VolumeUnit.cmm); // Cubic milimeters.
volume = new UVolume(1.0, UVolume.VolumeUnit.percent);
volume = new UVolume(1.0, UVolume.VolumeUnit.Undefined);

var doseVolumeTuples = new List<Tuple<UDose, UVolume>>();
doseVolumeTuples.Add(Tuple.Create(new UDose(0.0, UDose.UDoseUnit.Gy), new UVolume(2.3, UVolume.VolumeUnit.ccm)));
doseVolumeTuples.Add(Tuple.Create(new UDose(1.0, UDose.UDoseUnit.Gy), new UVolume(2.0, UVolume.VolumeUnit.ccm)));
doseVolumeTuples.Add(Tuple.Create(new UDose(2.0, UDose.UDoseUnit.Gy), new UVolume(1.0, UVolume.VolumeUnit.ccm)));
doseVolumeTuples.Add(Tuple.Create(new UDose(3.0, UDose.UDoseUnit.Gy), new UVolume(0.0, UVolume.VolumeUnit.ccm)));
var dvhCurve = new DVHCurve(doseVolumeTuples, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
```