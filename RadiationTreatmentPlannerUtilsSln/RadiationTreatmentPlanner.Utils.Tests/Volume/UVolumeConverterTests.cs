﻿using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.Tests.Volume
{
    [TestFixture]
    public class UVolumeConverterTests
    {
        [Test]
        public void ToUVolume_cmm_AsvolumeUnit_Test()
        {
            var volumeAsString = "20.13mm³";
            Assert.AreEqual(new UVolume(20.13, UVolume.VolumeUnit.cmm), volumeAsString.ToUVolume());
        }

        [Test]
        public void ToUVolume_ccm_AsvolumeUnit_Test()
        {
            var volumeAsString = "20.13cm³";
            Assert.AreEqual(new UVolume(20.13, UVolume.VolumeUnit.ccm), volumeAsString.ToUVolume());
        }

        [Test]
        public void ToUVolume_percent_AsvolumeUnit_Test()
        {
            var volumeAsString = "20.13%";
            Assert.AreEqual(new UVolume(20.13, UVolume.VolumeUnit.percent), volumeAsString.ToUVolume());
        }

        [Test]
        public void ToUVolume_Undefined_volumeUnit_Test()
        {
            var volumeAsString = "20.13Undefined";
            Assert.AreEqual(new UVolume(20.13, UVolume.VolumeUnit.Undefined), volumeAsString.ToUVolume());
        }

        [Test]
        public void ToUVolume_Undefined_volumeValue_Test()
        {
            var volumeAsString = "a12bcm³";

            var volume = volumeAsString.ToUVolume();
            Assert.AreEqual(UVolume.VolumeUnit.ccm, volume.Unit);
            Assert.IsNaN(volume.Value);
        }

        [Test]
        public void ToUVolume_Undefined_volumeValue2_Test()
        {
            var volumeAsString = "mm³abc";
            var volume = volumeAsString.ToUVolume();
            Assert.AreEqual(UVolume.VolumeUnit.cmm, volume.Unit);
            Assert.IsNaN(volume.Value);
        }

        [Test]
        public void UVolume_ImplicitOperator_Test()
        {
            var volumeAsString = "20.13mm³";
            UVolume volume = volumeAsString;
            Assert.AreEqual(new UVolume(20.13, UVolume.VolumeUnit.cmm), volume);
        }

        [Test]
        public void UVolume_ConvertUVolumeToStringAndBack_Test()
        {
            var volume = new UVolume(20.345, UVolume.VolumeUnit.cmm);
            var volumeAsString = volume.ToString();

            Assert.AreEqual("20.345mm³", volumeAsString);
            Assert.AreEqual(volume, volumeAsString.ToUVolume());
        }
    }
}
