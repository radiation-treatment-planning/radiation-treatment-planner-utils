﻿using System;
using System.Linq;
using System.Windows.Media.Media3D;
using NUnit.Framework;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Patient;

namespace RadiationTreatmentPlanner.Utils.Tests.Patient
{
    [TestFixture]
    public class UPatientTests
    {
        
        [Test]
        public void CreatePatient_Test()
        {
            var id = "00000";
            var name = "Max Musterfrau";
            var patient = new UPatient(id, name);
            Assert.AreEqual(id, patient.Id);
            Assert.AreEqual(name, patient.Name);
            Assert.IsEmpty(patient.StructureSets);
            Assert.IsEmpty(patient.Courses);
        }

        [Test]
        public void AddCourse_Test()
        {
            var patient = new UPatient("id", "name");
            patient.AddStructureSetWithId("ss");
            var structureSet = patient.StructureSets.FirstOrDefault()?.Detach();

            var plan1 = new UPlanSetupDetached("plan1", Option.Some<uint>(12), new UDose(60, UDose.UDoseUnit.Gy),
                structureSet);
            var plan2 = new UPlanSetupDetached("plan2", Option.Some<uint>(12), new UDose(60, UDose.UDoseUnit.Gy),
                structureSet);
            var course1 = new UCourseDetached("C1");
            var course2 = new UCourseDetached("C2");

            course1.AddPlanSetup(plan1);
            course1.AddPlanSetup(plan2);
            course2.AddPlanSetup(plan1);

            patient.AddCourse(course1);
            patient.AddCourse(course2);

            Assert.AreEqual(2, patient.Courses.Count());
            Assert.NotNull(patient.Courses.FirstOrDefault(x => x.Id == course1.Id));
            Assert.NotNull(patient.Courses.FirstOrDefault(x => x.Id == course2.Id));
            Assert.AreEqual(2, patient.Courses.FirstOrDefault(x => x.Id == course1.Id)?.PlanSetups.Count());
            Assert.AreEqual(1, patient.Courses.FirstOrDefault(x => x.Id == course2.Id)?.PlanSetups.Count());
            Assert.NotNull(patient.Courses.FirstOrDefault(x => x.Id == course1.Id)?.PlanSetups.FirstOrDefault(x => x.Id == plan1.Id));
            Assert.NotNull(patient.Courses.FirstOrDefault(x => x.Id == course1.Id)?.PlanSetups.FirstOrDefault(x => x.Id == plan2.Id));
            Assert.NotNull(patient.Courses.FirstOrDefault(x => x.Id == course2.Id)?.PlanSetups.FirstOrDefault(x => x.Id == plan1.Id));

        }

        [Test]
        public void AddCourse_ThrowArgumentExceptionIfCourseWasAlreadyAdded_Test()
        {
            var patient = new UPatient("id", "name");
            var course = new UCourseDetached("C1");
            patient.AddCourse(course);
            Assert.Throws<ArgumentException>(() => patient.AddCourse(course));
        }

        [Test]
        public void AddCourseWithId_Test()
        {
            var patient = new UPatient("id", "name");
            patient.AddCourseWithId("C1");
            Assert.AreEqual(1, patient.Courses.Count());
            Assert.NotNull(patient.Courses.FirstOrDefault(x => x.Id == "C1"));
        }

        [Test]
        public void AddCourseWithId_ThrowArgumentExceptionIfCourseWasAlreadyAdded_Test()
        {
            var patient = new UPatient("id", "name");
            var course = patient.AddCourseWithId("C1");
            Assert.Throws<ArgumentException>(() => patient.AddCourseWithId("C1"));
        }

        [Test]
        public void AddStructureSet_Test()
        {
            var patient = new UPatient("id", "name");
            Assert.AreEqual(0, patient.StructureSets.Count());

            var structure1 = new UStructureDetached("structure1", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());

            var structure2 = new UStructureDetached("structure2", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());

            var structureSet1 = new UStructureSetDetached("ss1");
            var structureSet2 = new UStructureSetDetached("ss2");

            structureSet1.AddStructure(structure1);
            structureSet1.AddStructure(structure2);
            structureSet2.AddStructure(structure1);
            patient.AddStructureSet(structureSet1);
            patient.AddStructureSet(structureSet2);

            Assert.AreEqual(2, patient.StructureSets.Count());
            var resultStructureSet1 = patient.StructureSets.FirstOrDefault(x => x.Id == "ss1");
            var resultStructureSet2 = patient.StructureSets.FirstOrDefault(x => x.Id == "ss2");

            Assert.AreEqual(2, resultStructureSet1?.Structures.Count());
            Assert.AreEqual(1, resultStructureSet2?.Structures.Count());

            Assert.NotNull(resultStructureSet1.Structures.FirstOrDefault(x => x.Id == "structure1"));
            Assert.NotNull(resultStructureSet1.Structures.FirstOrDefault(x => x.Id == "structure2"));
            Assert.NotNull(resultStructureSet2.Structures.FirstOrDefault(x => x.Id == "structure1"));
        }

        [Test]
        public void AddStructureSet_ThrowArgumentExceptionIfStructureSetWasAlreadyAdded_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSet = new UStructureSetDetached("ss");
            patient.AddStructureSet(structureSet);
            Assert.Throws<ArgumentException>(() => patient.AddStructureSet(structureSet));
        }

        [Test]
        public void AddStructureSetWithId_Test()
        {
            var patient = new UPatient("id", "name");
            patient.AddStructureSetWithId("ss1");
            patient.AddStructureSetWithId("ss2");

            Assert.AreEqual(2, patient.StructureSets.Count());
            Assert.NotNull(patient.StructureSets.FirstOrDefault(x => x.Id == "ss1"));
            Assert.NotNull(patient.StructureSets.FirstOrDefault(x => x.Id == "ss2"));
        }

        [Test]
        public void AddStructureSetWithId_ThrowArgumentExceptionIfStructureSetWasAlreadyAdded_Test()
        {
            var patient = new UPatient("id", "name");
            patient.AddStructureSetWithId("ss");
            Assert.Throws<ArgumentException>(() => patient.AddStructureSetWithId("ss"));
        }
    }
}

