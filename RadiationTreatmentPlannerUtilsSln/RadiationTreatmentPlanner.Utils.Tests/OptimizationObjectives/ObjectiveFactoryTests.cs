﻿using System;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.OptimizationObjetives;
using RadiationTreatmentPlanner.Utils.Patient;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.Tests.OptimizationObjectives
{
    [TestFixture]
    public class ObjectiveFactoryTests
    {
        [Test]
        public void BuildPointObjectiveAsHardConstraint_Test()
        {
            var objective = ObjectivesFactory.BuildPointObjective("s",
                ObjectiveOperator.LOWER, new Priority(500),
                new UDose(10, UDose.UDoseUnit.Gy), new UVolume(100, UVolume.VolumeUnit.percent));

            Assert.AreEqual("s", objective.StructureId);
            Assert.AreEqual(ObjectiveOperator.LOWER, objective.ObjectiveOperator);
            Assert.AreEqual(new UDose(10, UDose.UDoseUnit.Gy), objective.Dose);
            Assert.AreEqual(new UVolume(100, UVolume.VolumeUnit.percent), objective.VolumeInPercent);
            Assert.AreEqual(new Priority(500), objective.Priority);
        }

        [Test]
        public void BuildPointObjective_Test()
        {
            var objective = ObjectivesFactory.BuildPointObjective("s",
                ObjectiveOperator.LOWER, new Priority(500),
                new UDose(10, UDose.UDoseUnit.Gy), new UVolume(100, UVolume.VolumeUnit.percent));

            Assert.AreEqual("s", objective.StructureId);
            Assert.AreEqual(ObjectiveOperator.LOWER, objective.ObjectiveOperator);
            Assert.AreEqual(new UDose(10, UDose.UDoseUnit.Gy), objective.Dose);
            Assert.AreEqual(new UVolume(100, UVolume.VolumeUnit.percent), objective.VolumeInPercent);
            Assert.AreEqual(new Priority(500), objective.Priority);
        }

        [Test]
        public void BuildEUDObjectiveAsHardConstraint_Test()
        {
            var objective = ObjectivesFactory.BuildEudObjective("s",
                ObjectiveOperator.LOWER, new Priority(500), new UDose(60, UDose.UDoseUnit.Gy),
                -40);


            Assert.AreEqual("s", objective.StructureId);
            Assert.AreEqual(ObjectiveOperator.LOWER, objective.ObjectiveOperator);
            Assert.AreEqual(new UDose(60, UDose.UDoseUnit.Gy), objective.Dose);
            Assert.AreEqual(new Priority(500), objective.Priority);
            Assert.AreEqual(-40, objective.ParameterA);
        }

        [Test]
        public void BuildEUDObjectiveAsSoftConstraint_Test()
        {
            var objective = ObjectivesFactory.BuildEudObjective("s",
                ObjectiveOperator.LOWER, new Priority(500), new UDose(60, UDose.UDoseUnit.Gy),
                -40);


            Assert.AreEqual("s", objective.StructureId);
            Assert.AreEqual(ObjectiveOperator.LOWER, objective.ObjectiveOperator);
            Assert.AreEqual(new UDose(60, UDose.UDoseUnit.Gy), objective.Dose);
            Assert.AreEqual(new Priority(500), objective.Priority);
            Assert.AreEqual(-40, objective.ParameterA);
        }

        [Test]
        public void BuildMeanDoseObjectiveAsHardConstraint_Test()
        {
            var objective =
                ObjectivesFactory.BuildMeanDoseObjective("s", new UDose(50, UDose.UDoseUnit.Gy),
                    new Priority(500));

            Assert.AreEqual("s", objective.StructureId);
            Assert.AreEqual(new UDose(50, UDose.UDoseUnit.Gy), objective.Dose);
            Assert.AreEqual(new Priority(500), objective.Priority);
        }

        [Test]
        public void BuildMeanDoseObjectiveAsSoftConstraint_Test()
        {
            var objective =
                ObjectivesFactory.BuildMeanDoseObjective("s", new UDose(50, UDose.UDoseUnit.Gy),
                    new Priority(500));

            Assert.AreEqual("s", objective.StructureId);
            Assert.AreEqual(new UDose(50, UDose.UDoseUnit.Gy), objective.Dose);
            Assert.AreEqual(new Priority(500), objective.Priority);
        }

        [Test]
        public void BuildPointObjectiveAsHardConstraint_ThrowArgumentNullExceptionIfStructureIdIsNull_Test()
        {
            Assert.Throws<ArgumentNullException>(() => ObjectivesFactory.BuildPointObjective(null,
                ObjectiveOperator.UPPER, new Priority(100), new UDose(60, UDose.UDoseUnit.Gy),
                new UVolume(100, UVolume.VolumeUnit.percent)));
        }

        [Test]
        public void BuildPointObjective_ThrowArgumentNullExceptionIfStructureIdIsNull_Test()
        {
            Assert.Throws<ArgumentNullException>(() => ObjectivesFactory.BuildPointObjective(null,
                ObjectiveOperator.UPPER, new Priority(100), new UDose(60, UDose.UDoseUnit.Gy),
                new UVolume(100, UVolume.VolumeUnit.percent)));
        }

        [Test]
        public void BuildEUDObjectiveAsHardConstraint_ThrowArgumentNullExceptionIfStructureIdIsNull_Test()
        {
            Assert.Throws<ArgumentNullException>(() => ObjectivesFactory.BuildEudObjective(null,
                ObjectiveOperator.LOWER, new Priority(500), new UDose(60, UDose.UDoseUnit.Gy),
                -40));
        }

        [Test]
        public void BuildEUDObjectiveAsSoftConstraint_ThrowArgumentNullExceptionIfStructureIdIsNull_Test()
        {
            Assert.Throws<ArgumentNullException>(() => ObjectivesFactory.BuildEudObjective(null,
                ObjectiveOperator.LOWER, new Priority(500), new UDose(60, UDose.UDoseUnit.Gy),
                -40));
        }

        [Test]
        public void BuildMeanDoseObjectiveAsHardConstraint_ThrowArgumentNullExceptionIfStructureIdIsNull_Test()
        {
            Assert.Throws<ArgumentNullException>(() =>
                ObjectivesFactory.BuildMeanDoseObjective(null, new UDose(60, UDose.UDoseUnit.Gy),
                    new Priority(500)));
        }

        [Test]
        public void BuildMeanDoseObjectiveAsSoftConstraint_ThrowArgumentNullExceptionIfStructureIdIsNull_Test()
        {
            Assert.Throws<ArgumentNullException>(() =>
                ObjectivesFactory.BuildMeanDoseObjective(null, new UDose(60, UDose.UDoseUnit.Gy),
                    new Priority(500)));
        }

        [Test]
        public void BuildPointObjectiveAsHardConstraint_ThrowArgumentExceptionIfVolumeUnitIsNotPercent_Test()
        {
            Assert.Throws<ArgumentException>(() => ObjectivesFactory.BuildPointObjective("s",
                ObjectiveOperator.UPPER, new Priority(100), new UDose(60, UDose.UDoseUnit.Gy),
                new UVolume(100, UVolume.VolumeUnit.ccm)));
        }

        [Test]
        public void BuildPointObjective_ThrowArgumentExceptionIfVolumeUnitIsNotPercent_Test()
        {
            Assert.Throws<ArgumentException>(() => ObjectivesFactory.BuildPointObjective("s",
                ObjectiveOperator.UPPER, new Priority(100), new UDose(60, UDose.UDoseUnit.Gy),
                new UVolume(100, UVolume.VolumeUnit.ccm)));
        }
    }
}
