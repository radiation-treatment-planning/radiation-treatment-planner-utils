﻿using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Patient;
using System.Windows.Media.Media3D;
using RadiationTreatmentPlanner.Utils.DVH;

namespace RadiationTreatmentPlanner.Utils.CApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var patient = BuildAPatient();
        }

        private static UPatient BuildAPatient()
        {
            var dvhFileParser = DVHFileParser.CreateFromFile("CumulativeDvhExampleFile.txt");
            var patient = new UPatient("123456", "Max Musterfrau");
            var structureSet = patient.AddStructureSetWithId("ss");
            structureSet.AddStructureWithId("GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            structureSet.AddStructureWithId("OAR", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());

            var course = patient.AddCourseWithId("C1");
            var planSetup = course.AddPlanSetupWithId("plan1", Option.Some<uint>(12), new UDose(60, UDose.UDoseUnit.Gy), structureSet);
            course.AddPlanSetupWithId("plan2", Option.Some<uint>(11), new UDose(62, UDose.UDoseUnit.Gy), structureSet);

            planSetup.AddDvhEstimate(dvhFileParser.GetStructure(), dvhFileParser.GetCoverage(), dvhFileParser.GetMinDose(),
                dvhFileParser.GetMedianDose(), dvhFileParser.GetMedianDose(), dvhFileParser.GetMaxDose(),
                dvhFileParser.GetSamplingCoverage(), dvhFileParser.GetStandardDeviationDose(),
                dvhFileParser.GetTotalVolume(), dvhFileParser.GetDVHCurve());

            return patient;
        }
    }
}
