﻿using System;
using System.Linq;
using System.Windows.Media.Media3D;
using NUnit.Framework;
using Optional;
using RadiationTreatmentPlanner.Utils.Patient;

namespace RadiationTreatmentPlanner.Utils.Tests.Patient
{
    [TestFixture]
    public class UStructureTests
    {
        [Test]
        public void Constructor_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSet = patient.AddStructureSetWithId("ss");
            var structure = structureSet.AddStructureWithId("s1", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var resultStructure = structureSet.Structures.FirstOrDefault(x => x.Id == "s1");
            Assert.NotNull(structure);
            Assert.NotNull(resultStructure);
            Assert.AreEqual("s1", resultStructure.Id);
            Assert.AreEqual(Option.None<(double, double, double)>(), resultStructure.CenterPoint);
            Assert.AreEqual(Option.None<string>(), resultStructure.DicomType);
            Assert.AreEqual(Option.None<double>(), resultStructure.Volume);
            Assert.AreEqual(Option.None<bool>(), resultStructure.HasSegment);
            Assert.AreEqual(Option.None<MeshGeometry3D>(), resultStructure.MeshGeometry3D);
            Assert.AreEqual(structureSet, resultStructure.StructureSet);
        }

        [Test]
        public void AddStructureWithId_ThrowArgumentExceptionIfVolumeIsLowerZero_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureSet = patient.AddStructureSet(structureSetDetached);
            Assert.Throws<ArgumentException>(() => structureSet.AddStructureWithId("s1", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.Some<double>(-1), Option.None<bool>(), Option.None<MeshGeometry3D>()));
        }

        [Test]
        public void Detach()
        {

            var patient = new UPatient("id", "name");
            var structureSet = patient.AddStructureSetWithId("ss");
            var resultStructure = structureSet.AddStructureWithId("s1", Option.None<(double, double, double)>(),
                    Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>())
                .Detach();

            Assert.NotNull(resultStructure);
            Assert.AreEqual("s1", resultStructure.Id);
            Assert.AreEqual(Option.None<(double, double, double)>(), resultStructure.CenterPoint);
            Assert.AreEqual(Option.None<string>(), resultStructure.DicomType);
            Assert.AreEqual(Option.None<double>(), resultStructure.Volume);
            Assert.AreEqual(Option.None<bool>(), resultStructure.HasSegment);
            Assert.AreEqual(Option.None<MeshGeometry3D>(), resultStructure.MeshGeometry3D);
        }
    }
}
