﻿using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.Tests.Volume
{
    [TestFixture]
    public class UVolumeTests
    {
        [Test]
        public void NegativeVolume_Test()
        {
            Assert.Throws<NegativeVolumeException>(() => new UVolume(-1.0, UVolume.VolumeUnit.ccm));
        }

        [Test]
        public void CreateUVolume_Test()
        {
            var volume = new UVolume(1.0, UVolume.VolumeUnit.ccm);
            Assert.AreEqual(1.0, volume.Value);
            Assert.AreEqual(UVolume.VolumeUnit.ccm, volume.Unit);
        }

        [Test]
        public void AreEqual_Test()
        {
            var volume1 = new UVolume(100, UVolume.VolumeUnit.Undefined);
            var volume2 = new UVolume(100, UVolume.VolumeUnit.Undefined);
            Assert.AreEqual(volume1, volume2);
        }


        [Test]
        public void ToString_Test()
        {
            var dose = new UVolume(3.141592, UVolume.VolumeUnit.ccm);
            var dose2 = new UVolume(3.141592, UVolume.VolumeUnit.cmm);
            var dose3 = new UVolume(3.141592, UVolume.VolumeUnit.percent);
            var dose4 = new UVolume(3.141592, UVolume.VolumeUnit.Undefined);

            Assert.AreEqual($"3.141592cm³", dose.ToString());
            Assert.AreEqual($"3.141592mm³", dose2.ToString());
            Assert.AreEqual($"3.141592%", dose3.ToString());
            Assert.AreEqual($"3.141592Undefined", dose4.ToString());
        }

        [TestCase(0u, "3cm³")]
        [TestCase(1u, "3.1cm³")]
        [TestCase(2u, "3.14cm³")]
        [TestCase(3u, "3.142cm³")]
        [TestCase(4u, "3.1416cm³")]
        [TestCase(5u, "3.14159cm³")]
        [TestCase(6u, "3.141592cm³")]
        [TestCase(7u, "3.141592cm³")]
        public void ToString_NumberOfFloatingPointDigits_AsParameter_Test(uint numberOfFloatingPointDigits, string expectedResult)
        {
            var volume = new UVolume(3.141592, UVolume.VolumeUnit.ccm);

            Assert.AreEqual(expectedResult, volume.ToString(numberOfFloatingPointDigits));
        }
    }
}
