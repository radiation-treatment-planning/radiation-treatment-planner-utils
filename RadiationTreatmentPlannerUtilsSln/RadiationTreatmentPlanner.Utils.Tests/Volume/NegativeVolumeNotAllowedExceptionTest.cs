﻿using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.Tests.Volume
{
    [TestFixture]
    public class NegativeVolumeNotAllowedExceptionTest
    {
        [Test]
        public void NegativeVolumeNotAllowedException_OneInputArgument_Test()
        {
            var number = -1.0;
            Assert.Throws<NegativeVolumeException>(() => ThrowNegativeVolumeNotAllowedException(number));
        }

        private void ThrowNegativeVolumeNotAllowedException(double number)
        {
            if (number < 0)
                throw new NegativeVolumeException(number);
        }
    }
}
