﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.Tests.DVH
{
    [TestFixture]
    public class DVHCurveConverterTests
    {
        [Test]
        public void ToDifferential_EmptyInput_Test()
        {
            var emptyCumulativeDVHCurve = new DVHCurve(new List<Tuple<UDose, UVolume>>(), DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.UNDEFINED);
            var differentialDVHCurve = emptyCumulativeDVHCurve.ToDifferential();

            Assert.IsEmpty(differentialDVHCurve.GetPoints());
            Assert.AreEqual(DVHCurve.Type.DIFFERENTIAL, differentialDVHCurve.DVHType);
            Assert.AreEqual(DVHCurve.DoseType.UNDEFINED, differentialDVHCurve.TypeOfDose);
        }

        [Test]
        public void ToDifferential_DifferentialDVHCurveInput_Test()
        {
            var cumulativeDoseVolumeTupleList = new List<Tuple<UDose, UVolume>>();
            cumulativeDoseVolumeTupleList.Add(Tuple.Create(new UDose(78.79, UDose.UDoseUnit.Gy),
                new UVolume(60.0707320493719, UVolume.VolumeUnit.ccm)));

            var dVHCurve = new DVHCurve(cumulativeDoseVolumeTupleList, DVHCurve.Type.DIFFERENTIAL, DVHCurve.DoseType.PHYSICAL);

            var result = dVHCurve.ToDifferential();

            Assert.AreEqual(dVHCurve, result);
        }

        [Test]
        public void ToDifferential_OneElementListAsInput_Test()
        {
            var cumulativeDoseVolumeTupleList = new List<Tuple<UDose, UVolume>>();
            cumulativeDoseVolumeTupleList.Add(Tuple.Create(new UDose(78.79, UDose.UDoseUnit.Gy),
                new UVolume(60.0707320493719, UVolume.VolumeUnit.ccm)));
            var cumulativeDvhCurve = new DVHCurve(cumulativeDoseVolumeTupleList, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            var expectedDifferentialDoseVolumeTupleList = new List<Tuple<UDose, UVolume>>();
            expectedDifferentialDoseVolumeTupleList.Add(Tuple.Create(new UDose(78.79, UDose.UDoseUnit.Gy),
                new UVolume(0, UVolume.VolumeUnit.ccm)));
            var expectedDvhCurve = new DVHCurve(expectedDifferentialDoseVolumeTupleList, DVHCurve.Type.DIFFERENTIAL, DVHCurve.DoseType.PHYSICAL);

            var result = cumulativeDvhCurve.ToDifferential();

            Assert.AreEqual(expectedDvhCurve.DVHType, result.DVHType);

            var resultsPointList = result.GetPoints().ToList();

            for (var i = 0; i < expectedDifferentialDoseVolumeTupleList.Count; i++)
            {
                Assert.AreEqual(expectedDifferentialDoseVolumeTupleList.ElementAt(i).Item1.Value,
                    resultsPointList.ElementAt(i).Item1.Value, 1E-4);
                Assert.AreEqual(expectedDifferentialDoseVolumeTupleList.ElementAt(i).Item1.Unit,
                    resultsPointList.ElementAt(i).Item1.Unit);
                Assert.AreEqual(expectedDifferentialDoseVolumeTupleList.ElementAt(i).Item2.Value,
                    resultsPointList.ElementAt(i).Item2.Value, 1E-4);
                Assert.AreEqual(expectedDifferentialDoseVolumeTupleList.ElementAt(i).Item2.Unit,
                    resultsPointList.ElementAt(i).Item2.Unit);
            }
        }

        [Test]
        public void ToDifferential_Test()
        {
            var cumulativeDoseVolumeTupleList = new List<Tuple<UDose, UVolume>>();
            cumulativeDoseVolumeTupleList.Add(Tuple.Create(new UDose(78.79, UDose.UDoseUnit.Gy),
                new UVolume(60.0707320493719, UVolume.VolumeUnit.ccm)));
            cumulativeDoseVolumeTupleList.Add(Tuple.Create(new UDose(78.8, UDose.UDoseUnit.Gy),
                new UVolume(60.0565814297748, UVolume.VolumeUnit.ccm)));
            cumulativeDoseVolumeTupleList.Add(Tuple.Create(new UDose(78.81, UDose.UDoseUnit.Gy),
                new UVolume(60.0420098242459, UVolume.VolumeUnit.ccm)));
            var cumulativeDVHCurve = new DVHCurve(cumulativeDoseVolumeTupleList, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            var expectedDifferentialDoseVolumeTupleList = new List<Tuple<UDose, UVolume>>();
            expectedDifferentialDoseVolumeTupleList.Add(Tuple.Create(new UDose(78.79, UDose.UDoseUnit.Gy),
                new UVolume(0.01415062, UVolume.VolumeUnit.ccm)));
            expectedDifferentialDoseVolumeTupleList.Add(Tuple.Create(new UDose(78.8, UDose.UDoseUnit.Gy),
                new UVolume(0.014571606, UVolume.VolumeUnit.ccm)));
            expectedDifferentialDoseVolumeTupleList.Add(Tuple.Create(new UDose(78.81, UDose.UDoseUnit.Gy),
                new UVolume(0, UVolume.VolumeUnit.ccm)));
            var expectedDVHCurve = new DVHCurve(expectedDifferentialDoseVolumeTupleList, DVHCurve.Type.DIFFERENTIAL, DVHCurve.DoseType.PHYSICAL);

            var result = cumulativeDVHCurve.ToDifferential();

            Assert.AreEqual(expectedDVHCurve.DVHType, result.DVHType);

            var resultsPointList = result.GetPoints().ToList();

            for (var i = 0; i < expectedDifferentialDoseVolumeTupleList.Count; i++)
            {
                Assert.AreEqual(expectedDifferentialDoseVolumeTupleList.ElementAt(i).Item1.Value,
                    resultsPointList.ElementAt(i).Item1.Value, 1E-4);
                Assert.AreEqual(expectedDifferentialDoseVolumeTupleList.ElementAt(i).Item1.Unit,
                    resultsPointList.ElementAt(i).Item1.Unit);
                Assert.AreEqual(expectedDifferentialDoseVolumeTupleList.ElementAt(i).Item2.Value,
                    resultsPointList.ElementAt(i).Item2.Value, 1E-4);
                Assert.AreEqual(expectedDifferentialDoseVolumeTupleList.ElementAt(i).Item2.Unit,
                    resultsPointList.ElementAt(i).Item2.Unit);
            }
        }

        [Test]
        public void ToVolumeInPercent_ReturnSameIfVolumeIsAlreadyInPercent_Test()
        {
            var cumulativeDoseVolumeTupleList = new List<Tuple<UDose, UVolume>>();
            cumulativeDoseVolumeTupleList.Add(Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy),
                new UVolume(100, UVolume.VolumeUnit.percent)));
            cumulativeDoseVolumeTupleList.Add(Tuple.Create(new UDose(20, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent)));
            cumulativeDoseVolumeTupleList.Add(Tuple.Create(new UDose(30, UDose.UDoseUnit.Gy),
                new UVolume(10, UVolume.VolumeUnit.percent)));
            cumulativeDoseVolumeTupleList.Add(Tuple.Create(new UDose(40, UDose.UDoseUnit.Gy),
                new UVolume(0, UVolume.VolumeUnit.percent)));
            var cumulativeDVHCurve = new DVHCurve(cumulativeDoseVolumeTupleList, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            var result = cumulativeDVHCurve.ToVolumeInPercent();
            Assert.AreEqual(cumulativeDVHCurve, result);
        }

        [Test]
        public void ToVolumeInPercent_EmptyInput_Test()
        {
            var cumulativeDoseVolumeTupleList = new List<Tuple<UDose, UVolume>>();
            var cumulativeDVHCurve = new DVHCurve(cumulativeDoseVolumeTupleList, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            var result = cumulativeDVHCurve.ToVolumeInPercent();
            Assert.AreEqual(cumulativeDVHCurve, result);
        }


        [Test]
        public void ToVolumeInPercent_Test()
        {
            var cumulativeDoseVolumeTupleList = new List<Tuple<UDose, UVolume>>();
            cumulativeDoseVolumeTupleList.Add(Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy),
                new UVolume(80, UVolume.VolumeUnit.ccm)));
            cumulativeDoseVolumeTupleList.Add(Tuple.Create(new UDose(20, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.ccm)));
            cumulativeDoseVolumeTupleList.Add(Tuple.Create(new UDose(30, UDose.UDoseUnit.Gy),
                new UVolume(10, UVolume.VolumeUnit.ccm)));
            cumulativeDoseVolumeTupleList.Add(Tuple.Create(new UDose(40, UDose.UDoseUnit.Gy),
                new UVolume(0, UVolume.VolumeUnit.ccm)));
            var cumulativeDVHCurve = new DVHCurve(cumulativeDoseVolumeTupleList, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            var expectedDvhCurve = new DVHCurve(new List<Tuple<UDose, UVolume>>()
            {
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(100, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(20, UDose.UDoseUnit.Gy), new UVolume(62.5, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(30, UDose.UDoseUnit.Gy), new UVolume(12.5, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(40, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.percent)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            var result = cumulativeDVHCurve.ToVolumeInPercent();
            Assert.AreEqual(expectedDvhCurve, result);
        }

        [Test]
        public void ToEqd2_PhysicalDoseAsInput_Test()
        {
            var numberOfFractions = 12u;
            var alphaOverBeta = 1.4;

            var dvhCurve = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(20, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(30, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            var expectedResult = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(6.56863, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(18.0392, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(34.4118, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.EQD2);

            var result = dvhCurve.ToEqd2(alphaOverBeta, numberOfFractions);

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void ToEqd2_Eqd0DoseAsInput_Test()
        {
            var numberOfFractions = 12u;
            var alphaOverBeta = 1.6;

            var dvhCurve = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(20, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(30, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.EQD0);

            var expectedResult = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(4.444444, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(8.88888, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(13.33333, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.EQD2);

            var result = dvhCurve.ToEqd2(alphaOverBeta, numberOfFractions);

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void ToEqd2_Throws_ArgumentOutOfRangeException_For_NumberOfFractions_EqualZero_Test()
        {
            var dvhCurve = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(20, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(30, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            Assert.Throws<ArgumentOutOfRangeException>(() => dvhCurve.ToEqd2(1.4, 0));
        }

        [Test]
        public void ToEqd2_Throws_ArgumentOutOfRangeException_For_AlphaOverBeta_LowerEqualZero_Test()
        {
            var dvhCurve = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(20, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(30, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            Assert.Throws<ArgumentOutOfRangeException>(() => dvhCurve.ToEqd2(0, 12u));
            Assert.Throws<ArgumentOutOfRangeException>(() => dvhCurve.ToEqd2(-1, 12u));
        }

        [Test]
        public void ToEqd2_ReturnsEqualCurve_ForEQD2DoseType_Test()
        {
            var numberOfFractions = 12u;
            var alphaOverBeta = 1.4;

            var dvhCurve = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(20, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(30, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.EQD2);

            var result = dvhCurve.ToEqd2(alphaOverBeta, numberOfFractions);
            Assert.AreEqual(dvhCurve, result);
        }

        [Test]
        public void ToEqd0__PhysicalDoseAsInput_Test()
        {
            var numberOfFractions = 12u;
            var alphaOverBeta = 1.4;

            var dvhCurve = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(20, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(30, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            var expectedResult = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(15.9524, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(43.8095, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(83.5714, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.EQD0);

            var result = dvhCurve.ToEqd0(alphaOverBeta, numberOfFractions);

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void ToEqd0__Eqd2DoseAsInput_Test()
        {
            var numberOfFractions = 12u;
            var alphaOverBeta = 1.6;

            var dvhCurve = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(20, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(30, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.EQD2);

            var expectedResult = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(22.5, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(45, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(67.5, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.EQD0);

            var result = dvhCurve.ToEqd0(alphaOverBeta, numberOfFractions);

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void ToEqd0_Throws_ArgumentOutOfRangeException_For_NumberOfFractions_EqualZero_Test()
        {
            var dvhCurve = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(20, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(30, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            Assert.Throws<ArgumentOutOfRangeException>(() => dvhCurve.ToEqd0(1.4, 0));
        }

        [Test]
        public void ToEqd0_Throws_ArgumentOutOfRangeException_For_AlphaOverBeta_LowerEqualZero_Test()
        {
            var dvhCurve = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(20, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(30, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            Assert.Throws<ArgumentOutOfRangeException>(() => dvhCurve.ToEqd0(0, 12u));
            Assert.Throws<ArgumentOutOfRangeException>(() => dvhCurve.ToEqd0(-1, 12u));
        }

        [Test]
        public void ToEqd0_ReturnsEqualCurve_ForEQD0DoseType_Test()
        {
            var numberOfFractions = 12u;
            var alphaOverBeta = 1.4;

            var dvhCurve = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(20, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(30, UDose.UDoseUnit.Gy), new UVolume(5, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.EQD0);

            var result = dvhCurve.ToEqd0(alphaOverBeta, numberOfFractions);
            Assert.AreEqual(dvhCurve, result);
        }
    }
}
