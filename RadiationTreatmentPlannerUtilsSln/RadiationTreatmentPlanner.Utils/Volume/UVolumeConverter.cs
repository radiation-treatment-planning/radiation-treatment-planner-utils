﻿namespace RadiationTreatmentPlanner.Utils.Volume
{
    public static class UVolumeConverter
    {
            public static UVolume ToUVolume(this string volumeAsString)
            {
                var volumeUnit = UVolume.VolumeUnit.Undefined;
                var volumeUnitExtractionResult = ExtractVolumeUnit(volumeAsString);
                if (volumeUnitExtractionResult.HasValue)
                {
                    volumeUnit = volumeUnitExtractionResult.Value;
                    volumeAsString = volumeAsString.Replace(volumeUnit.ToDescription(), "");
                }
                var value = UStringHelper.ConvertStringToDoubleOrDefault(volumeAsString);
                return new UVolume(value, volumeUnit);
            }

            private static UVolume.VolumeUnit? ExtractVolumeUnit(string volumeAsString)
            {
                if (volumeAsString.Contains(UVolume.VolumeUnit.ccm.ToDescription())) return UVolume.VolumeUnit.ccm;
                if (volumeAsString.Contains(UVolume.VolumeUnit.cmm.ToDescription())) return UVolume.VolumeUnit.cmm;
                if (volumeAsString.Contains(UVolume.VolumeUnit.percent.ToDescription())) return UVolume.VolumeUnit.percent;
                if (volumeAsString.Contains(UVolume.VolumeUnit.Undefined.ToDescription())) return UVolume.VolumeUnit.Undefined;
                return null;
            }
        
    }
}
