﻿namespace RadiationTreatmentPlanner.Utils.Patient
{
    public interface IUStructureSet
    {
        string Id { get; }
    }
}
