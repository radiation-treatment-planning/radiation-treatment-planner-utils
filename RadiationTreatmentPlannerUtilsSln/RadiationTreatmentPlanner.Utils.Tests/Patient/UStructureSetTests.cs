﻿using System;
using System.Linq;
using System.Windows.Media.Media3D;
using NUnit.Framework;
using Optional;
using RadiationTreatmentPlanner.Utils.Patient;

namespace RadiationTreatmentPlanner.Utils.Tests.Patient
{
    [TestFixture]
    public class UStructureSetTests
    {
        [Test]
        public void AddStructure_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureSet = patient.AddStructureSet(structureSetDetached);
            var structureDetached1 = new UStructureDetached("s1", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureDetached2 = new UStructureDetached("s2", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            structureSet.AddStructure(structureDetached1);
            structureSet.AddStructure(structureDetached2);

            var structures = structureSet.Structures;
            var structure1 = structures.FirstOrDefault(x => x.Id == structureDetached1.Id);
            var structure2 = structures.FirstOrDefault(x => x.Id == structureDetached2.Id);
            Assert.NotNull(structure1);
            Assert.NotNull(structure2);
            Assert.AreEqual("s1", structure1.Id);
            Assert.AreEqual(Option.None<(double, double, double)>(), structure1.CenterPoint);
            Assert.AreEqual(Option.None<string>(), structure1.DicomType);
            Assert.AreEqual(Option.None<double>(), structure1.Volume);
            Assert.AreEqual(Option.None<bool>(), structure1.HasSegment);
            Assert.AreEqual(Option.None<MeshGeometry3D>(), structure1.MeshGeometry3D);
            Assert.AreEqual("s2", structure2.Id);
            Assert.AreEqual(Option.None<(double, double, double)>(), structure2.CenterPoint);
            Assert.AreEqual(Option.None<string>(), structure2.DicomType);
            Assert.AreEqual(Option.None<double>(), structure2.Volume);
            Assert.AreEqual(Option.None<bool>(), structure2.HasSegment);
            Assert.AreEqual(Option.None<MeshGeometry3D>(), structure2.MeshGeometry3D);
        }

        [Test]
        public void AddStructure_ThrowArgumentExceptionIfStructureWithIdAlreadyExists_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureSet = patient.AddStructureSet(structureSetDetached);
            var structureDetached1 = new UStructureDetached("s1", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureDetached2 = new UStructureDetached("s1", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            structureSet.AddStructure(structureDetached1);
            Assert.Throws<ArgumentException>(() => structureSet.AddStructure(structureDetached2));
        }

        [Test]
        public void AddStructureWithId_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureSet = patient.AddStructureSet(structureSetDetached);
            structureSet.AddStructureWithId("s1", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            structureSet.AddStructureWithId("s2", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());

            var structures = structureSet.Structures;
            var structure1 = structures.FirstOrDefault(x => x.Id == "s1");
            var structure2 = structures.FirstOrDefault(x => x.Id == "s2");
            Assert.NotNull(structure1);
            Assert.NotNull(structure2);
            Assert.AreEqual("s1", structure1.Id);
            Assert.AreEqual(Option.None<(double, double, double)>(), structure1.CenterPoint);
            Assert.AreEqual(Option.None<string>(), structure1.DicomType);
            Assert.AreEqual(Option.None<double>(), structure1.Volume);
            Assert.AreEqual(Option.None<bool>(), structure1.HasSegment);
            Assert.AreEqual(Option.None<MeshGeometry3D>(), structure1.MeshGeometry3D);
            Assert.AreEqual("s2", structure2.Id);
            Assert.AreEqual(Option.None<(double, double, double)>(), structure2.CenterPoint);
            Assert.AreEqual(Option.None<string>(), structure2.DicomType);
            Assert.AreEqual(Option.None<double>(), structure2.Volume);
            Assert.AreEqual(Option.None<bool>(), structure2.HasSegment);
            Assert.AreEqual(Option.None<MeshGeometry3D>(), structure2.MeshGeometry3D);
        }

        [Test]
        public void AddStructureWithId_ThrowArgumentExceptionIfStructureWithIdAlreadyExists_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureSet = patient.AddStructureSet(structureSetDetached);
            structureSet.AddStructureWithId("s1", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            Assert.Throws<ArgumentException>(() => structureSet.AddStructureWithId("s1", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>()));
        }
    }
}
