﻿using RadiationTreatmentPlanner.Utils.Dose;

namespace RadiationTreatmentPlanner.Utils.OptimizationObjetives
{
    public interface IObjective
    {
        string StructureId { get; }
        Priority Priority { get; }
        UDose Dose { get; }
    }

    public enum ObjectiveOperator
    {
        LOWER = -1,
        EXACT = 0,
        UPPER = 1
    }
}
