﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Media3D;
using NUnit.Framework;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.Fields;
using RadiationTreatmentPlanner.Utils.OptimizationObjetives;
using RadiationTreatmentPlanner.Utils.Patient;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.Tests.Patient
{
    [TestFixture]
    public class UPlanSetupDetachedTests
    {
        [Test]
        public void SetTotalDose_Test()
        {
            var structureSet = new UStructureSetDetached("ss");
            var structureDetached = new UStructureDetached("s1", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            structureSet.AddStructure(structureDetached);
            var planSetup = new UPlanSetupDetached("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSet);
            planSetup.SetTotalDose(new UDose(1000, UDose.UDoseUnit.cGy));
            Assert.AreEqual(1000, planSetup.TotalPrescriptionDose.Value);
            Assert.AreEqual(UDose.UDoseUnit.cGy, planSetup.TotalPrescriptionDose.Unit);
        }

        [Test]
        public void SetNumberOfFractions_Test()
        {
            var structureSet = new UStructureSetDetached("ss");
            var structureDetached = new UStructureDetached("s1", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            structureSet.AddStructure(structureDetached);
            var planSetup = new UPlanSetupDetached("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSet);
            planSetup.SetNumberOfFractions(Option.Some<uint>(10));
            Assert.AreEqual(Option.Some<uint>(10), planSetup.NumberOfFractions);
        }


        [Test]
        public void AddOptimizationObjectiveRange_Test()
        {
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureGtvDetached = new UStructureDetached("GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureOarDetached = new UStructureDetached("OAR", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            structureSetDetached.AddStructure(structureGtvDetached);
            structureSetDetached.AddStructure(structureOarDetached);
            var planSetupDetached = new UPlanSetupDetached("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSetDetached);

            var objectives = new IObjective[]
            {
                new PointObjective("GTV", ObjectiveOperator.LOWER, new Priority(500), new UDose(66, UDose.UDoseUnit.Gy),
                    new UVolume(98, UVolume.VolumeUnit.percent)),
                new EUDObjective("OAR", ObjectiveOperator.LOWER, new Priority(500), new UDose(40, UDose.UDoseUnit.Gy),
                    40),
                new MeanDoseObjective("GTV", new UDose(72, UDose.UDoseUnit.Gy), new Priority(500))
            };

            var optimizationObjectiveAddResult = planSetupDetached.AddOptimizationObjectiveRange(objectives);

            var result = planSetupDetached.OptimizationObjectives.ToArray();
            var pointObjective = result[0];
            var eudObjective = result[1];
            var meanDoseObjective = result[2];


            Assert.AreEqual(structureGtvDetached.Id, pointObjective.StructureId);
            Assert.IsTrue(pointObjective is PointObjective);

            Assert.AreEqual(structureOarDetached.Id, eudObjective.StructureId);
            Assert.IsTrue(eudObjective is EUDObjective);

            Assert.AreEqual(structureGtvDetached.Id, meanDoseObjective.StructureId);
            Assert.IsTrue(meanDoseObjective is MeanDoseObjective);

            Assert.IsTrue(optimizationObjectiveAddResult.Success);
        }

        [Test]
        public void AddOptimizationObjectiveRange_SuccessIsFalse_IfStructureDoesNotExistInStructureSet_Test()
        {
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureGtvDetached = new UStructureDetached("GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureOarDetached = new UStructureDetached("OAR", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            structureSetDetached.AddStructure(structureGtvDetached);
            structureSetDetached.AddStructure(structureOarDetached);
            var planSetupDetached = new UPlanSetupDetached("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSetDetached);

            var objectives = new IObjective[]
            {
                new PointObjective("Not Available Structure", ObjectiveOperator.LOWER, new Priority(500), new UDose(66, UDose.UDoseUnit.Gy),
                    new UVolume(98, UVolume.VolumeUnit.percent)),
                new EUDObjective("OAR", ObjectiveOperator.LOWER, new Priority(500), new UDose(40, UDose.UDoseUnit.Gy),
                    40),
                new MeanDoseObjective("GTV", new UDose(72, UDose.UDoseUnit.Gy), new Priority(500))
            };

            var optimizationObjectiveAddResult = planSetupDetached.AddOptimizationObjectiveRange(objectives);
            Assert.Zero(planSetupDetached.OptimizationObjectives.Count());
            Assert.IsFalse(optimizationObjectiveAddResult.Success);
        }

        [Test]
        public void AddVMATArc_Test()
        {
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureGtvDetached = new UStructureDetached("GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureOarDetached = new UStructureDetached("OAR", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            structureSetDetached.AddStructure(structureGtvDetached);
            structureSetDetached.AddStructure(structureOarDetached);
            var planSetupDetached = new UPlanSetupDetached("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSetDetached);

            var id = "ARC1";
            var gantryRotation = new GantryRotation(new AngleInDegree(10), new AngleInDegree(170), RotationDirection.Clockwise);
            var collimatorRotation = new AngleInDegree(35);
            var couchRotation = new AngleInDegree(0);
            var arc = new VMATArc(id, gantryRotation, collimatorRotation, couchRotation);

            Assert.IsEmpty(planSetupDetached.VmatArcs.ToList());
            planSetupDetached.AddVMATArc(arc);
            var result = planSetupDetached.VmatArcs.ToList();
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(arc, result.First());
        }

        [Test]
        public void AddVMATArc_ThrowArgumentExceptionIfArcWithSameIdAlreadyExists_Test()
        {
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureGtvDetached = new UStructureDetached("GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureOarDetached = new UStructureDetached("OAR", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            structureSetDetached.AddStructure(structureGtvDetached);
            structureSetDetached.AddStructure(structureOarDetached);
            var planSetupDetached = new UPlanSetupDetached("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSetDetached);

            var id = "ARC1";
            var gantryRotation = new GantryRotation(new AngleInDegree(10), new AngleInDegree(170), RotationDirection.Clockwise);
            var collimatorRotation = new AngleInDegree(35);
            var couchRotation = new AngleInDegree(0);
            var arc = new VMATArc(id, gantryRotation, collimatorRotation, couchRotation);
            var arc2 = new VMATArc(id, gantryRotation, collimatorRotation, couchRotation);

            planSetupDetached.AddVMATArc(arc);
            Assert.Throws<ArgumentException>(() => planSetupDetached.AddVMATArc(arc2));
        }

        [Test]
        public void AddVMATArcRange_Test()
        {
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureGtvDetached = new UStructureDetached("GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureOarDetached = new UStructureDetached("OAR", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            structureSetDetached.AddStructure(structureGtvDetached);
            structureSetDetached.AddStructure(structureOarDetached);
            var planSetupDetached = new UPlanSetupDetached("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSetDetached);

            var id1 = "ARC1";
            var id2 = "ARC2";
            var gantryRotation = new GantryRotation(new AngleInDegree(10), new AngleInDegree(170), RotationDirection.Clockwise);
            var collimatorRotation = new AngleInDegree(5);
            var collimatorRotation2 = new AngleInDegree(355);
            var couchRotation = new AngleInDegree(0);
            var arc1 = new VMATArc(id1, gantryRotation, collimatorRotation, couchRotation);
            var arc2 = new VMATArc(id2, gantryRotation, collimatorRotation2, couchRotation);
            var arcs = new List<VMATArc> { arc1, arc2 };

            Assert.IsEmpty(planSetupDetached.VmatArcs.ToList());
            planSetupDetached.AddVMATArcRange(arcs);
            var result = planSetupDetached.VmatArcs.ToList();
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(arc1, result.First());
            Assert.AreEqual(arc2, result.Last());
        }

        [Test]
        public void AddVMATArcRange_ThrowArgumentExceptionIfArcWithSameIdAlreadyExists_Test()
        {
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureGtvDetached = new UStructureDetached("GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureOarDetached = new UStructureDetached("OAR", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            structureSetDetached.AddStructure(structureGtvDetached);
            structureSetDetached.AddStructure(structureOarDetached);
            var planSetupDetached = new UPlanSetupDetached("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSetDetached);

            var id = "ARC1";
            var gantryRotation = new GantryRotation(new AngleInDegree(10), new AngleInDegree(170), RotationDirection.Clockwise);
            var collimatorRotation = new AngleInDegree(5);
            var collimatorRotation2 = new AngleInDegree(355);
            var couchRotation = new AngleInDegree(0);
            var arc1 = new VMATArc(id, gantryRotation, collimatorRotation, couchRotation);
            var arc2 = new VMATArc(id, gantryRotation, collimatorRotation2, couchRotation);
            var arcs = new List<VMATArc> { arc1, arc2 };

            Assert.Throws<ArgumentException>(() => planSetupDetached.AddVMATArcRange(arcs));
        }

        [Test]
        public void AddDvhEstimate_Test()
        {
            var dvhFileParser = DVHFileParser.CreateFromFile("FilesForTesting/CumulativeDvhExampleFile.txt");
            var dvhFileParser2 = DVHFileParser.CreateFromFile("FilesForTesting/CumulativeDvhExampleFile3.txt");

            var structureSet = new UStructureSetDetached("S1");
            structureSet.AddStructure(new UStructureDetached("GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>()));
            structureSet.AddStructure(new UStructureDetached("OAR", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>()));
            var planSetup = new UPlanSetupDetached("P1", Option.Some<uint>(12), new UDose(66, UDose.UDoseUnit.Gy),
                structureSet);

            var dvhEstimate1 = new UDvhEstimateDetached("P1", dvhFileParser.GetStructure(), dvhFileParser.GetCoverage(),
                dvhFileParser.GetMinDose(),
                dvhFileParser.GetMedianDose(), dvhFileParser.GetMedianDose(), dvhFileParser.GetMaxDose(),
                dvhFileParser.GetSamplingCoverage(), dvhFileParser.GetStandardDeviationDose(),
                dvhFileParser.GetTotalVolume(), dvhFileParser.GetDVHCurve());
            var dvhEstimate2 = new UDvhEstimateDetached("P1", dvhFileParser2.GetStructure(), dvhFileParser2.GetCoverage(),
                dvhFileParser2.GetMinDose(),
                dvhFileParser2.GetMedianDose(), dvhFileParser2.GetMedianDose(), dvhFileParser2.GetMaxDose(),
                dvhFileParser2.GetSamplingCoverage(), dvhFileParser2.GetStandardDeviationDose(),
                dvhFileParser2.GetTotalVolume(), dvhFileParser2.GetDVHCurve());

            planSetup.AddDvhEstimate(dvhEstimate1);
            planSetup.AddDvhEstimate(dvhEstimate2);

            var result1 = planSetup.DvhEstimates.FirstOrDefault();
            Assert.NotNull(result1);
            Assert.AreEqual(dvhFileParser.GetStructure(), result1.StructureId);
            Assert.AreEqual(dvhFileParser.GetCoverage(), result1.Coverage);
            Assert.AreEqual(dvhFileParser.GetSamplingCoverage(), result1.SamplingCoverage);
            Assert.AreEqual(dvhFileParser.GetDVHCurve(), result1.DvhCurve);
            Assert.AreEqual(dvhFileParser.GetMinDose(), result1.MinDose);
            Assert.AreEqual(dvhFileParser.GetMedianDose(), result1.MedianDose);
            Assert.AreEqual(dvhFileParser.GetMedianDose(), result1.MeanDose);
            Assert.AreEqual(dvhFileParser.GetMaxDose(), result1.MaxDose);
            Assert.AreEqual(dvhFileParser.GetStandardDeviationDose(), result1.StandardDeviationDose);

            var result2 = planSetup.DvhEstimates.LastOrDefault();
            Assert.NotNull(result2);
            Assert.AreEqual(dvhFileParser2.GetStructure(), result2.StructureId);
            Assert.AreEqual(dvhFileParser2.GetCoverage(), result2.Coverage);
            Assert.AreEqual(dvhFileParser2.GetSamplingCoverage(), result2.SamplingCoverage);
            Assert.AreEqual(dvhFileParser2.GetDVHCurve(), result2.DvhCurve);
            Assert.AreEqual(dvhFileParser2.GetMinDose(), result2.MinDose);
            Assert.AreEqual(dvhFileParser2.GetMedianDose(), result2.MedianDose);
            Assert.AreEqual(dvhFileParser2.GetMedianDose(), result2.MeanDose);
            Assert.AreEqual(dvhFileParser2.GetMaxDose(), result2.MaxDose);
            Assert.AreEqual(dvhFileParser2.GetStandardDeviationDose(), result2.StandardDeviationDose);
        }

        [Test]
        public void AddDvhEstimate_ThrowArgumentException_IfStructureDoesNotExistInStructureSet_Test()
        {
            var dvhFileParser = DVHFileParser.CreateFromFile("FilesForTesting/CumulativeDvhExampleFile.txt");

            var structureSet = new UStructureSetDetached("S1");
            var planSetup = new UPlanSetupDetached("P1", Option.Some<uint>(12), new UDose(66, UDose.UDoseUnit.Gy),
                structureSet);

            var dvhEstimate1 = new UDvhEstimateDetached("P1", dvhFileParser.GetStructure(), dvhFileParser.GetCoverage(),
                dvhFileParser.GetMinDose(),
                dvhFileParser.GetMedianDose(), dvhFileParser.GetMedianDose(), dvhFileParser.GetMaxDose(),
                dvhFileParser.GetSamplingCoverage(), dvhFileParser.GetStandardDeviationDose(),
                dvhFileParser.GetTotalVolume(), dvhFileParser.GetDVHCurve());

            Assert.Throws<ArgumentException>(() => planSetup.AddDvhEstimate(dvhEstimate1));
        }

        [Test]
        public void AddDvhEstimate_ThrowArgumentException_IfDvhEstimateForSameStructureAlreadyExists_Test()
        {
            var dvhFileParser = DVHFileParser.CreateFromFile("FilesForTesting/CumulativeDvhExampleFile.txt");

            var structureSet = new UStructureSetDetached("S1");
            structureSet.AddStructure(new UStructureDetached("GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>()));
            var planSetup = new UPlanSetupDetached("P1", Option.Some<uint>(12), new UDose(66, UDose.UDoseUnit.Gy),
                structureSet);

            var dvhEstimate1 = new UDvhEstimateDetached("P1", dvhFileParser.GetStructure(), dvhFileParser.GetCoverage(),
                dvhFileParser.GetMinDose(),
                dvhFileParser.GetMedianDose(), dvhFileParser.GetMedianDose(), dvhFileParser.GetMaxDose(),
                dvhFileParser.GetSamplingCoverage(), dvhFileParser.GetStandardDeviationDose(),
                dvhFileParser.GetTotalVolume(), dvhFileParser.GetDVHCurve());

            planSetup.AddDvhEstimate(dvhEstimate1);

            Assert.Throws<ArgumentException>(() => planSetup.AddDvhEstimate(dvhEstimate1));
        }

        [Test]
        public void AddDvhEstimate_ThrowArgumentNullException_IfArgumentIsNull_Test()
        {
            var dvhFileParser = DVHFileParser.CreateFromFile("FilesForTesting/CumulativeDvhExampleFile.txt");

            var structureSet = new UStructureSetDetached("S1");
            var planSetup = new UPlanSetupDetached("P1", Option.Some<uint>(12), new UDose(66, UDose.UDoseUnit.Gy),
                structureSet);

            Assert.Throws<ArgumentNullException>(() => planSetup.AddDvhEstimate(null));
        }

        [Test]
        public void Equals_Test()
        {
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureGtvDetached = new UStructureDetached("GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureOarDetached = new UStructureDetached("OAR", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            structureSetDetached.AddStructure(structureGtvDetached);
            structureSetDetached.AddStructure(structureOarDetached);

            var planSetupDetached = new UPlanSetupDetached("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSetDetached);
            var planSetupDetached2 = new UPlanSetupDetached("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSetDetached);
            var planSetupDetached3 = new UPlanSetupDetached("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSetDetached);
            var planSetupDetached4 = new UPlanSetupDetached("plan4", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSetDetached);
            var planSetupDetached5 = new UPlanSetupDetached("plan4", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSetDetached);

            var id1 = "ARC1";
            var id2 = "ARC2";
            var gantryRotation = new GantryRotation(new AngleInDegree(10), new AngleInDegree(170), RotationDirection.Clockwise);
            var collimatorRotation = new AngleInDegree(5);
            var collimatorRotation2 = new AngleInDegree(355);
            var couchRotation = new AngleInDegree(0);
            var arc1 = new VMATArc(id1, gantryRotation, collimatorRotation, couchRotation);
            var arc2 = new VMATArc(id2, gantryRotation, collimatorRotation2, couchRotation);
            var arcs = new List<VMATArc> { arc1, arc2 };

            var objectives = new IObjective[]
            {
                new PointObjective("GTV", ObjectiveOperator.LOWER, new Priority(500), new UDose(66, UDose.UDoseUnit.Gy),
                    new UVolume(98, UVolume.VolumeUnit.percent)),
                new PointObjective("OAR", ObjectiveOperator.UPPER, new Priority(100),
                    new UDose(47.4, UDose.UDoseUnit.Gy), new UVolume(100, UVolume.VolumeUnit.percent)),
            };

            planSetupDetached.AddVMATArcRange(arcs);
            planSetupDetached2.AddVMATArcRange(arcs);
            planSetupDetached3.AddVMATArcRange(arcs);

            planSetupDetached.AddOptimizationObjectiveRange(objectives);
            planSetupDetached2.AddOptimizationObjectiveRange(objectives);
            planSetupDetached4.AddOptimizationObjectiveRange(objectives);

            Assert.AreEqual(planSetupDetached, planSetupDetached2);
            Assert.AreNotEqual(planSetupDetached, planSetupDetached3);
            Assert.AreNotEqual(planSetupDetached, planSetupDetached4);
        }
    }
}
