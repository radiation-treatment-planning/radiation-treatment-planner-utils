﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.DVH
{
    public class DVHFileParser
    {
        protected Dictionary<Enum, object> _dictionary;

        protected DVHFileParser(string fileName, Dictionary<Enum, object> dictionary)
        {
            _dictionary = dictionary ?? throw new ArgumentNullException(nameof(dictionary));
            FileName = fileName ?? throw new ArgumentNullException(nameof(fileName));
        }

        public string FileName { get; }

        public static string ReadOnlyPiz(string absoluteFilePath)
        {
            string piz = "N/A";
            using (StreamReader streamReader = new StreamReader(absoluteFilePath))
            {
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    if (line.Contains(DVHVersion101.PIZ.ToDescription()))
                    {
                        var (key, value) = GetLineDataSeparatedBySemiColumn(line);
                        piz = value;
                        break;
                    }
                }
            }
            return piz;
        }

        public static void CreateFiles_DVHVersion1_0_1(string patientPiz, string planId, string structureId, UDose prescriptionDose, uint numberOfFractions, UDvhEstimate dvhEstimate, DirectoryInfo totalPathToRootDir)
        {
            var patientDirectory = new DirectoryInfo(Path.Combine(totalPathToRootDir.FullName, patientPiz));
            Directory.CreateDirectory(patientDirectory.FullName);
            var filePath = Path.Combine(patientDirectory.FullName, $"{patientPiz}_{planId}_{structureId}.txt");

            var content = new List<string>();
            content.Add(ConcatenateWithColon(DVHVersion101.VERSION.ToDescription(), "1.0.1"));
            content.Add(ConcatenateWithColon(DVHVersion101.PIZ.ToDescription(), patientPiz));
            content.Add(ConcatenateWithColon(DVHVersion101.PLAN.ToDescription(), planId));
            content.Add(ConcatenateWithColon(DVHVersion101.STRUCTURE.ToDescription(), structureId));
            content.Add(ConcatenateWithColon(DVHVersion101.TOTAL_VOLUME.ToDescription(), dvhEstimate.Volume.Value.ToString(), dvhEstimate.Volume.Unit.ToDescription()));
            content.Add(ConcatenateWithColon(DVHVersion101.COVERAGE.ToDescription(), dvhEstimate.Coverage.ToString()));
            content.Add(ConcatenateWithColon(DVHVersion101.SAMPLING_COVERAGE.ToDescription(), dvhEstimate.SamplingCoverage.ToString()));
            content.Add(ConcatenateWithColon(DVHVersion101.MAX_DOSE.ToDescription(), dvhEstimate.MaxDose.Value.ToString(), dvhEstimate.MaxDose.Unit.ToDescription()));
            content.Add(ConcatenateWithColon(DVHVersion101.MIN_DOSE.ToDescription(), dvhEstimate.MinDose.Value.ToString(), dvhEstimate.MinDose.Unit.ToDescription()));
            content.Add(ConcatenateWithColon(DVHVersion101.MEDIAN_DOSE.ToDescription(), dvhEstimate.MedianDose.Value.ToString(), dvhEstimate.MedianDose.Unit.ToDescription()));
            content.Add(ConcatenateWithColon(DVHVersion101.STANDARD_DEVIATION.ToDescription(), dvhEstimate.StandardDeviationDose.Value.ToString(), dvhEstimate.StandardDeviationDose.Unit.ToDescription()));
            content.Add(ConcatenateWithColon(DVHVersion101.PRESCRIPTION_DOSE.ToDescription(), prescriptionDose.Value.ToString(), prescriptionDose.Unit.ToDescription()));
            content.Add(ConcatenateWithColon(DVHVersion101.FRACTION_NUMBER.ToDescription(), numberOfFractions.ToString()));
            content.Add("");
            content = ConcatenateAllDoseVolumeTuples(content, dvhEstimate);

            File.WriteAllLines(filePath, content);
        }
        public static void CreateDvhFile(string patientPiz, UDose prescriptionDose, uint numberOfFractions, UDvhEstimateDetached dvhEstimate, FileInfo totalFileName)
        {
            var dir = totalFileName.DirectoryName;
            if (dir != null) Directory.CreateDirectory(dir);

            var content = new List<string>();
            content.Add(ConcatenateWithColon(DVHVersion101.VERSION.ToDescription(), "1.0.1"));
            content.Add(ConcatenateWithColon(DVHVersion101.PIZ.ToDescription(), patientPiz));
            content.Add(ConcatenateWithColon(DVHVersion101.PLAN.ToDescription(), dvhEstimate.PlanSetupId));
            content.Add(ConcatenateWithColon(DVHVersion101.STRUCTURE.ToDescription(), dvhEstimate.StructureId));
            content.Add(ConcatenateWithColon(DVHVersion101.TOTAL_VOLUME.ToDescription(), dvhEstimate.Volume.Value.ToString(), dvhEstimate.Volume.Unit.ToDescription()));
            content.Add(ConcatenateWithColon(DVHVersion101.COVERAGE.ToDescription(), dvhEstimate.Coverage.ToString()));
            content.Add(ConcatenateWithColon(DVHVersion101.SAMPLING_COVERAGE.ToDescription(), dvhEstimate.SamplingCoverage.ToString()));
            content.Add(ConcatenateWithColon(DVHVersion101.MAX_DOSE.ToDescription(), dvhEstimate.MaxDose.Value.ToString(), dvhEstimate.MaxDose.Unit.ToDescription()));
            content.Add(ConcatenateWithColon(DVHVersion101.MIN_DOSE.ToDescription(), dvhEstimate.MinDose.Value.ToString(), dvhEstimate.MinDose.Unit.ToDescription()));
            content.Add(ConcatenateWithColon(DVHVersion101.MEDIAN_DOSE.ToDescription(), dvhEstimate.MedianDose.Value.ToString(), dvhEstimate.MedianDose.Unit.ToDescription()));
            content.Add(ConcatenateWithColon(DVHVersion101.STANDARD_DEVIATION.ToDescription(), dvhEstimate.StandardDeviationDose.Value.ToString(), dvhEstimate.StandardDeviationDose.Unit.ToDescription()));
            content.Add(ConcatenateWithColon(DVHVersion101.PRESCRIPTION_DOSE.ToDescription(), prescriptionDose.Value.ToString(), prescriptionDose.Unit.ToDescription()));
            content.Add(ConcatenateWithColon(DVHVersion101.FRACTION_NUMBER.ToDescription(), numberOfFractions.ToString()));
            content.Add("");
            content = ConcatenateAllDoseVolumeTuples(content, dvhEstimate);

            File.WriteAllLines(totalFileName.FullName, content);
        }

        private static List<string> ConcatenateAllDoseVolumeTuples(List<string> content, UDvhEstimate dvhEstimate)
        {
            content.Add(
                $"Dose [{dvhEstimate.MaxDose.Unit.ToDescription()}]\tVolume [{dvhEstimate.Volume.Unit.ToDescription()}]");
            foreach (var tuple in dvhEstimate.DvhCurve.GetPoints())
            {
                var dose = tuple.Item1;
                var volume = tuple.Item2;
                content.Add($"{dose.Value}\t{volume.Value}");
            }

            return content;
        }

        private static List<string> ConcatenateAllDoseVolumeTuples(List<string> content, UDvhEstimateDetached dvhEstimate)
        {
            content.Add(
                $"Dose [{dvhEstimate.MaxDose.Unit.ToDescription()}]\tVolume [{dvhEstimate.Volume.Unit.ToDescription()}]");
            foreach (var tuple in dvhEstimate.DvhCurve.GetPoints())
            {
                var dose = tuple.Item1;
                var volume = tuple.Item2;
                content.Add($"{dose.Value}\t{volume.Value}");
            }

            return content;
        }

        private static string ConcatenateWithColon(string attribute, string value) => $"{attribute}: {value}\n";
        private static string ConcatenateWithColon(string attribute, string value, string unit) => $"{attribute}: {value} {unit}\n";

        public static DVHFileParser CreateFromFile(string fileName)
        {
            var content = new Dictionary<Enum, object>();
            var executableLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var absFilePath = Path.Combine(executableLocation, fileName);
            var allLines = File.ReadAllLines(absFilePath);

            var mapping = new DataExtractionMapper();
            mapping.Map(DVHVersion101.PIZ, GetValueByName<string>);
            mapping.Map(DVHVersion101.VERSION, GetValueByName<string>);
            mapping.Map(DVHVersion101.STRUCTURE, GetValueByName<string>);
            mapping.Map(DVHVersion101.PLAN, GetValueByName<string>);
            mapping.Map(DVHVersion101.FRACTION_NUMBER, GetValueByName<object>);
            mapping.Map(DVHVersion101.SAMPLING_COVERAGE, GetValueByName<object>);
            mapping.Map(DVHVersion101.COVERAGE, GetValueByName<object>);
            mapping.Map(DVHVersion101.TOTAL_VOLUME, GetValueByName<UVolume>);
            mapping.Map(DVHVersion101.PRESCRIPTION_DOSE, GetValueByName<UDose>);
            mapping.Map(DVHVersion101.MIN_DOSE, GetValueByName<UDose>);
            mapping.Map(DVHVersion101.MAX_DOSE, GetValueByName<UDose>);
            mapping.Map(DVHVersion101.MEDIAN_DOSE, GetValueByName<UDose>);
            mapping.Map(DVHVersion101.STANDARD_DEVIATION, GetValueByName<UDose>);
            mapping.Map(DVHVersion101.DVHCURVE, GetDVHCurve);


            foreach (var tuple in mapping.GetAll())
            {
                var func = tuple.Item2;
                var value = func(tuple.Item1.ToDescription(), allLines);
                content.Add(tuple.Item1, value);
            }


            return new DVHFileParser(fileName, content);
        }

        private static DVHCurve GetDVHCurve(string keyName, string[] allLines)
        {
            var version = GetValueByName<string>(DVHVersion101.VERSION.ToDescription(), allLines);

            var curvePoints = new List<Tuple<UDose, UVolume>>();

            var pos = FindLineNumberByKey(keyName, allLines);
            if (pos.HasValue)
            {
                var (doseUnit, volumeUnit) = ExtractDoseAndVolumeUnitsFromLine(allLines[pos.Value]);
                for (var i = pos.Value + 1; i < allLines.Length; i++)
                {
                    var line = allLines[i];
                    var items = line.Split('\t');
                    if (items.Length == 2)
                    {
                        var doseValue = UStringHelper.ConvertStringToDoubleOrDefault(items[0]);
                        var volumeValue = UStringHelper.ConvertStringToDoubleOrDefault(items[1]);
                        curvePoints.Add(new Tuple<UDose, UVolume>(
                            new UDose(doseValue, doseUnit),
                            new UVolume(volumeValue, volumeUnit)));
                    }
                }
            }

            // The 1.0.1 version DVH exporter delivers always cumulative DVHs
            if (version == "1.0.1")
            {
                return new DVHCurve(curvePoints, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
            }

            return new DVHCurve(curvePoints, DVHCurve.Type.UNDEFINED, DVHCurve.DoseType.UNDEFINED);
        }

        private static (UDose.UDoseUnit, UVolume.VolumeUnit) ExtractDoseAndVolumeUnitsFromLine(string line)
        {
            var stringsBetweenBrackets = line.Split().Where(x => x.StartsWith("[") && x.EndsWith("]"))
                .Select(x => x.Replace("[", "").Replace("]", "")).ToArray();
            if (stringsBetweenBrackets.Length != 2) return (UDose.UDoseUnit.Unknown, UVolume.VolumeUnit.Undefined);
            var doseUnit = ConvertToDoseUnit(stringsBetweenBrackets[0]);
            var volumeUnit = ConvertToVolumeUnit(stringsBetweenBrackets[1]);
            return (doseUnit, volumeUnit);
        }

        private static UVolume.VolumeUnit ConvertToVolumeUnit(string unitAsString)
        {
            if (unitAsString == UVolume.VolumeUnit.ccm.ToDescription()) return UVolume.VolumeUnit.ccm;
            if (unitAsString == UVolume.VolumeUnit.cmm.ToDescription()) return UVolume.VolumeUnit.cmm;
            if (unitAsString == UVolume.VolumeUnit.percent.ToDescription()) return UVolume.VolumeUnit.percent;
            return UVolume.VolumeUnit.Undefined;
        }

        private static UDose.UDoseUnit ConvertToDoseUnit(string unitAsString)
        {
            if (unitAsString == UDose.UDoseUnit.Gy.ToDescription()) return UDose.UDoseUnit.Gy;
            if (unitAsString == UDose.UDoseUnit.cGy.ToDescription()) return UDose.UDoseUnit.cGy;
            if (unitAsString == UDose.UDoseUnit.Percent.ToDescription()) return UDose.UDoseUnit.Percent;
            return UDose.UDoseUnit.Unknown;
        }

        private static int? FindLineNumberByKey(string keyName, string[] allLines)
        {
            for (var i = 0; i < allLines.Length; i++)
                if (allLines[i].Contains(keyName))
                    return i;

            return null;
        }

        private static (string firstItem, string secondItem) GetLineDataSeparatedBySemiColumn(string line)
        {
            var items = line.Split(':');
            if (items.Length == 2) return (items[0].Trim(), items[1].Trim());

            return ("N/A", "N/A");
        }

        private static T GetValueByName<T>(string keyName, string[] allLines)
        {
            foreach (var line in allLines)
            {
                var (firstItem, secondItem) = GetLineDataSeparatedBySemiColumn(line);
                if (firstItem == keyName) return (dynamic)secondItem;
            }

            return default;
        }

        public string GetVersion()
        {
            return (string)_dictionary[DVHVersion101.VERSION];
        }

        public string GetPiz()
        {
            return _dictionary[DVHVersion101.PIZ].ToString();
        }

        public string GetPlan()
        {
            return (string)_dictionary[DVHVersion101.PLAN];
        }

        public string GetStructure()
        {
            return (string)_dictionary[DVHVersion101.STRUCTURE];
        }

        public UVolume GetTotalVolume()
        {
            return (UVolume)_dictionary[DVHVersion101.TOTAL_VOLUME];
        }

        public double GetCoverage()
        {
            return UStringHelper.ConvertStringToDoubleOrDefault(_dictionary[DVHVersion101.COVERAGE]
                .ToString());
        }

        public double GetSamplingCoverage()
        {
            return UStringHelper.ConvertStringToDoubleOrDefault(
                _dictionary[DVHVersion101.SAMPLING_COVERAGE]
                    .ToString());
        }

        public UDose GetMaxDose()
        {
            return (UDose)_dictionary[DVHVersion101.MAX_DOSE];
        }

        public UDose GetMinDose()
        {
            return (UDose)_dictionary[DVHVersion101.MIN_DOSE];
        }

        public UDose GetMedianDose()
        {
            return (UDose)_dictionary[DVHVersion101.MEDIAN_DOSE];
        }

        public UDose GetStandardDeviationDose()
        {
            return (UDose)_dictionary[DVHVersion101.STANDARD_DEVIATION];
        }

        public UDose GetPrescriptionDose()
        {
            return (UDose)_dictionary[DVHVersion101.PRESCRIPTION_DOSE];
        }

        public uint GetNumberOfFractions()
        {
            return Convert.ToUInt32(_dictionary[DVHVersion101.FRACTION_NUMBER].ToString());
        }

        public DVHCurve GetDVHCurve()
        {
            return (DVHCurve)_dictionary[DVHVersion101.DVHCURVE];
        }
    }
}
