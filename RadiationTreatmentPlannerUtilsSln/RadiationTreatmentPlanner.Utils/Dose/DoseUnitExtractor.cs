﻿namespace RadiationTreatmentPlanner.Utils.Dose
{
    public static class DoseUnitExtractor
    {
        public static DoseUnitExtractionResult Extract(string doseAsString)
        {
            if (doseAsString.Contains(UDose.UDoseUnit.cGy.ToDescription()))
                return new DoseUnitExtractionResult(true, UDose.UDoseUnit.cGy);
            if (doseAsString.Contains(UDose.UDoseUnit.Gy.ToDescription()))
                return new DoseUnitExtractionResult(true, UDose.UDoseUnit.Gy);
            if (doseAsString.Contains(UDose.UDoseUnit.Percent.ToDescription()))
                return new DoseUnitExtractionResult(true, UDose.UDoseUnit.Percent);
            if (doseAsString.Contains(UDose.UDoseUnit.Unknown.ToDescription()))
                return new DoseUnitExtractionResult(true, UDose.UDoseUnit.Unknown);
            return new DoseUnitExtractionResult(false, UDose.UDoseUnit.Unknown);
        }
    }
}
