﻿using System;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Patient;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.OptimizationObjetives
{
    public class PointObjective : IObjective
    {
        public string StructureId { get; }
        public ObjectiveOperator ObjectiveOperator { get; }
        public Priority Priority { get; }
        public UDose Dose { get; }
        public UVolume VolumeInPercent { get; }

        public PointObjective(string structureId, ObjectiveOperator objectiveOperator, Priority priority, UDose dose, UVolume volumeInPercent)
        {
            if (volumeInPercent.Unit != UVolume.VolumeUnit.percent)
                throw new ArgumentException(
                    $"Volume unit must be {UVolume.VolumeUnit.percent}, but was {volumeInPercent.Unit}.");
            StructureId = structureId ?? throw new ArgumentNullException(nameof(structureId));
            ObjectiveOperator = objectiveOperator;
            Priority = priority;
            Dose = dose;
            VolumeInPercent = volumeInPercent;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as PointObjective);
        }

        public bool Equals(PointObjective other)
        {
            return other != null
                   && StructureId == other.StructureId
                   && ObjectiveOperator == other.ObjectiveOperator
                   && Priority.Equals(other.Priority)
                   && Dose.Equals(other.Dose)
                   && VolumeInPercent.Equals(other.VolumeInPercent);
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(StructureId, ObjectiveOperator, Dose, Priority, VolumeInPercent);
        }
    }
}
