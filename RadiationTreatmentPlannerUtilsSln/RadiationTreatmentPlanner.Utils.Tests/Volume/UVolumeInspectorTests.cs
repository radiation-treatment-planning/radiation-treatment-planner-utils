﻿using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.Tests.Volume
{
    [TestFixture]
    public class UVolumeInspectorTests
    {
        [Test]
        public void IsAbsolute_ReturnTrueIfVolumeUnitIsCcm_Test()
        {
            var volume = new UVolume(30, UVolume.VolumeUnit.ccm);
            Assert.IsTrue(volume.IsAbsolute());
        }

        [Test]
        public void IsAbsolute_ReturnTrueIfVolumeUnitIsCmm_Test()
        {
            var volume = new UVolume(30, UVolume.VolumeUnit.cmm);
            Assert.IsTrue(volume.IsAbsolute());
        }

        [Test]
        public void IsAbsolute_ReturnFalseIfVolumeUnitIsPercent_Test()
        {
            var volume = new UVolume(30, UVolume.VolumeUnit.percent);
            Assert.IsFalse(volume.IsAbsolute());
        }

        [Test]
        public void IsAbsolute_ReturnFalseIfVolumeUnitIsUndefined_Test()
        {
            var volume = new UVolume(30, UVolume.VolumeUnit.Undefined);
            Assert.IsFalse(volume.IsAbsolute());
        }

        [Test]
        public void IsRelative_ReturnTrueIfVolumeUnitIsPercent_Test()
        {
            var volume = new UVolume(30, UVolume.VolumeUnit.percent);
            Assert.IsTrue(volume.IsRelative());
        }

        [Test]
        public void IsRelative_ReturnFalseIfVolumeUnitIsCcm_Test()
        {
            var volume = new UVolume(30, UVolume.VolumeUnit.ccm);
            Assert.IsFalse(volume.IsRelative());
        }

        [Test]
        public void IsRelative_ReturnFalseIfVolumeUnitIsCmm_Test()
        {
            var volume = new UVolume(30, UVolume.VolumeUnit.cmm);
            Assert.IsFalse(volume.IsRelative());
        }

        [Test]
        public void IsRelative_ReturnFalseIfVolumeUnitIsUndefined_Test()
        {
            var volume = new UVolume(30, UVolume.VolumeUnit.Undefined);
            Assert.IsFalse(volume.IsRelative());
        }
    }
}
