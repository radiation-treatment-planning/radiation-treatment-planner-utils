﻿using System;
using System.Collections.Generic;
using System.Linq;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.Normalization
{
    public class DvhNormalizer
    {
        /// <summary>
        /// Normalize DVH Curves so that <paramref name="x"/>% of prescription dose 
        /// cover <paramref name="y"/> percent of target volume.
        /// </summary>
        /// <param name="targetStructureId">ID of target structure.</param>
        /// <param name="dvhEstimates">DVH Estimates to normalize.</param>
        /// <param name="x">Percent of prescription dose (0, 100].</param>
        /// <param name="y">Percent of target volume (0, 100].</param>
        /// <returns></returns>
        public NormalizationResult NormalizeSoThatXPercentOfPrescriptionDoseCoversYPercentOfTargetVolume(
            string targetStructureId,
            IEnumerable<UDvhEstimateDetached> dvhEstimates, double x, double y, UDose totalPrescriptionDose)
        {
            CheckInput(targetStructureId, dvhEstimates, x, y, totalPrescriptionDose);
            var scaleFactor = CalculateScaleFactor(targetStructureId, dvhEstimates, x, y, totalPrescriptionDose);
            var scaledDvhs = ScaleAllDvhs(dvhEstimates, scaleFactor);
            return new NormalizationResult(scaledDvhs, scaleFactor);
        }

        private IEnumerable<UDvhEstimateDetached> ScaleAllDvhs(IEnumerable<UDvhEstimateDetached> dvhEstimates, double scaleFactor)
        {
            var scaledDvhEstimates = new List<UDvhEstimateDetached>(dvhEstimates.Count());
            foreach (var dvhEstimate in dvhEstimates)
            {
                var dvhCurve = dvhEstimate.DvhCurve;
                var scaledDvhCurvePoints = dvhCurve.GetPoints().Select(x =>
                    Tuple.Create(new UDose(x.Item1.Value * scaleFactor, x.Item1.Unit), x.Item2)).ToList();
                var totalVolume = dvhCurve.TotalVolume();
                totalVolume = totalVolume.Unit == UVolume.VolumeUnit.ccm || totalVolume.Unit == UVolume.VolumeUnit.cmm
                    ? totalVolume
                    : null;
                var scaledDvhCurve = new DVHCurve(scaledDvhCurvePoints, dvhCurve.DVHType, dvhCurve.TypeOfDose,
                    totalVolume);
                var scaledDvhEstimate = new UDvhEstimateDetached(dvhEstimate.PlanSetupId, dvhEstimate.StructureId,
                    dvhEstimate.Coverage, scaledDvhCurve.GetMinimumDose(), scaledDvhCurve.GetMeanDose(),
                    scaledDvhCurve.GetMedianDose(), scaledDvhCurve.GetMaximumDose(), dvhEstimate.SamplingCoverage,
                    scaledDvhCurve.GetStandardDeviationDose(), scaledDvhCurve.TotalVolume(), scaledDvhCurve);
                scaledDvhEstimates.Add(scaledDvhEstimate);
            }

            return scaledDvhEstimates;
        }

        public double CalculateScaleFactor(string targetStructureId, IEnumerable<UDvhEstimateDetached> dvhEstimates,
            double x, double y, UDose totalPrescriptionDose)
        {
            var targetDvh = dvhEstimates.First(z => z.StructureId == targetStructureId).DvhCurve;
            if (targetDvh.GetVolumeUnit() != UVolume.VolumeUnit.percent) targetDvh = targetDvh.ToVolumeInPercent();
            var xPercentDoseOfTotalDose = new UDose(totalPrescriptionDose.Value * (x / 100), totalPrescriptionDose.Unit);
            var volumeAtXPercentDose = targetDvh.GetVolumeAtDose(xPercentDoseOfTotalDose);
            var targetDoseAtYPercent = targetDvh.GetDoseAtVolume(new UVolume(y, UVolume.VolumeUnit.percent));
            return xPercentDoseOfTotalDose.Value / targetDoseAtYPercent.Value;
        }

        private void CheckInput(string targetStructureId, IEnumerable<UDvhEstimateDetached> dvhEstimates, double x,
            double y, UDose totalPrescriptionDose)
        {
            if (targetStructureId == null) throw new ArgumentNullException(nameof(targetStructureId));
            if (dvhEstimates == null) throw new ArgumentNullException(nameof(dvhEstimates));
            if (x <= 0 || x > 100) throw new ArgumentOutOfRangeException(nameof(x));
            if (y <= 0 || y > 100) throw new ArgumentOutOfRangeException(nameof(y));
            var numberOfDvhsForTargetStructure = dvhEstimates.Count(z => z.StructureId == targetStructureId);
            if (numberOfDvhsForTargetStructure == 0)
                throw new ArgumentException($"No DVH Estimate exists with structure ID {targetStructureId}.");
            if (numberOfDvhsForTargetStructure > 1)
                throw new ArgumentException($"More than one DVH Estimate exists for structure {targetStructureId}.");
            if (dvhEstimates.Any(z => z.DvhCurve.GetDoseUnit() != totalPrescriptionDose.Unit))
                throw new ArgumentException($"DVHs and {nameof(totalPrescriptionDose)} must have equal dose units.");
        }
    }
}
