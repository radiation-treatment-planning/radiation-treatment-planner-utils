﻿using System;

namespace RadiationTreatmentPlanner.Utils.Fields
{
    public class VMATArc
    {
        public string Id { get; }
        public GantryRotation GantryRotation { get; }
        public AngleInDegree CollimatorRotation { get; }
        public AngleInDegree CouchRotation { get; }

        public VMATArc(string id, GantryRotation gantryRotation, AngleInDegree collimatorRotation, AngleInDegree couchRotation)
        {
            Id = id ?? throw new ArgumentNullException(nameof(id));
            GantryRotation = gantryRotation ?? throw new ArgumentNullException(nameof(gantryRotation));
            CollimatorRotation = collimatorRotation;
            CouchRotation = couchRotation;
        }

        public override string ToString()
        {
            return
                $"ID: {Id}, gantry rotation: {GantryRotation}, collimator rotation:" +
                $" {CollimatorRotation}, couch rotation: {CouchRotation}";
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as VMATArc);
        }

        public bool Equals(VMATArc other)
        {
            return other != null
                   && Id.Equals(other.Id)
                   && GantryRotation.Equals(other.GantryRotation)
                   && CollimatorRotation.Equals(other.CollimatorRotation)
                   && CouchRotation.Equals(other.CouchRotation);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, GantryRotation, CollimatorRotation, CouchRotation);
        }
    }
}
