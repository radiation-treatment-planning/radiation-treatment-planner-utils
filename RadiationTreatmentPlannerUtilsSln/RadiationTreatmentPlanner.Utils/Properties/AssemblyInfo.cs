﻿using System.Resources;
using System.Reflection;
using System.Runtime.InteropServices;

// Allgemeine Informationen über eine Assembly werden über die folgenden
// Attribute gesteuert. Ändern Sie diese Attributwerte, um die Informationen zu ändern,
// die einer Assembly zugeordnet sind.
[assembly: AssemblyTitle("RadiationTreatmentPlanner.Utils")]
[assembly: AssemblyDescription("A utilities package for automatic radiation treatment planning. For source code and documentation see https://gitlab.com/radiation-treatment-planning/radiation-treatment-planner-utils/")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Dejan Kuhn, Medical Center University of Freiburg, Germany")]
[assembly: AssemblyProduct("RadiationTreatmentPlanner.Utils")]
[assembly: AssemblyCopyright("Copyright © Dejan Kuhn 2024")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Durch Festlegen von ComVisible auf FALSE werden die Typen in dieser Assembly
// für COM-Komponenten unsichtbar.  Wenn Sie auf einen Typ in dieser Assembly von
// COM aus zugreifen müssen, sollten Sie das ComVisible-Attribut für diesen Typ auf "True" festlegen.
[assembly: ComVisible(true)]

// Die folgende GUID bestimmt die ID der Typbibliothek, wenn dieses Projekt für COM verfügbar gemacht wird
[assembly: Guid("675c9d87-ee10-4d6f-8a5b-241ae45f7490")]

// Versionsinformationen für eine Assembly bestehen aus den folgenden vier Werten:
//
//      Hauptversion
//      Nebenversion
//      Buildnummer
//      Revision
//
// Sie können alle Werte angeben oder Standardwerte für die Build- und Revisionsnummern verwenden,
// indem Sie "*" wie unten gezeigt eingeben:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("5.0.0")]
[assembly: AssemblyFileVersion("5.0.0")]
[assembly: NeutralResourcesLanguage("en")]
