﻿using System;
using System.Windows.Media.Media3D;
using Optional;

namespace RadiationTreatmentPlanner.Utils.Patient
{
    public class UStructureDetached : IUStructure
    {
        public string Id { get; }
        public Option<(double, double, double)> CenterPoint { get; }
        public Option<string> DicomType { get; }
        public Option<double> Volume { get; }
        public Option<bool> HasSegment { get; }
        public Option<MeshGeometry3D> MeshGeometry3D { get; }

        public UStructureDetached(string id, Option<(double, double, double)> centerPoint, Option<string> dicomType, Option<double> volume, Option<bool> hasSegment, Option<MeshGeometry3D> meshGeometry3D)
        {
            Id = id;
            CenterPoint = centerPoint;
            DicomType = dicomType;
            Volume = volume;
            HasSegment = hasSegment;
            MeshGeometry3D = meshGeometry3D;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as UStructureDetached);
        }

        public bool Equals(UStructureDetached other)
        {
            if (other == null) return false;
            return Id == other.Id
                   && CenterPoint.Equals(other.CenterPoint)
                   && DicomType.Equals(other.DicomType)
                   && Volume.Equals(other.Volume)
                   && HasSegment.Equals(other.HasSegment)
                   && MeshGeometry3D.Equals(other.MeshGeometry3D);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, CenterPoint, DicomType, Volume, HasSegment, MeshGeometry3D);
        }
    }
}
