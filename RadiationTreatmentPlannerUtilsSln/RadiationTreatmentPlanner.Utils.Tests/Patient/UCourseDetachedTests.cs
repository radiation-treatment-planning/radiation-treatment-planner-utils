﻿using System;
using System.Linq;
using NUnit.Framework;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Patient;

namespace RadiationTreatmentPlanner.Utils.Tests.Patient
{
    [TestFixture]
    public class UCourseDetachedTests
    {
        [Test]
        public void Constructor_Test()
        {
            var course = new UCourseDetached("C1");
            Assert.AreEqual("C1", course.Id);
            Assert.AreEqual(0, course.PlanSetups.Count());
        }

        [Test]
        public void AddPlanSetup_Test()
        {
            var course = new UCourseDetached("C1");
            var structureSet = new UStructureSetDetached("ss");
            var planSetup1 =
                new UPlanSetupDetached("plan1", Option.Some<uint>(12), new UDose(60, UDose.UDoseUnit.Gy), structureSet);
            var planSetup2 =
                new UPlanSetupDetached("plan2", Option.Some<uint>(12), new UDose(60, UDose.UDoseUnit.Gy), structureSet);
            course.AddPlanSetup(planSetup1);
            course.AddPlanSetup(planSetup2);

            var resultPlanSetups = course.PlanSetups;
            var resultPlan1 = resultPlanSetups?.FirstOrDefault(x => x.Id == planSetup1.Id);
            var resultPlan2 = resultPlanSetups?.FirstOrDefault(x => x.Id == planSetup2.Id);

            Assert.NotNull(resultPlan1);
            Assert.NotNull(resultPlan2);
            Assert.AreEqual(planSetup1.Id, resultPlan1.Id);
            Assert.AreEqual(planSetup1.StructureSet.Id, resultPlan1.StructureSet.Id);
            Assert.AreEqual(planSetup1.NumberOfFractions, resultPlan1.NumberOfFractions);
            Assert.AreEqual(planSetup1.TotalPrescriptionDose.Value, resultPlan1.TotalPrescriptionDose.Value);
            Assert.AreEqual(planSetup1.TotalPrescriptionDose.Unit, resultPlan1.TotalPrescriptionDose.Unit);
            Assert.AreEqual(planSetup2.Id, resultPlan2.Id);
            Assert.AreEqual(planSetup2.StructureSet.Id, resultPlan2.StructureSet.Id);
            Assert.AreEqual(planSetup2.NumberOfFractions, resultPlan2.NumberOfFractions);
            Assert.AreEqual(planSetup2.TotalPrescriptionDose.Value, resultPlan2.TotalPrescriptionDose.Value);
            Assert.AreEqual(planSetup2.TotalPrescriptionDose.Unit, resultPlan2.TotalPrescriptionDose.Unit);
        }

        [Test]
        public void AddPlanSetup_ThrowArgumentErrorIfPlanWithIdAlreadyExists_Test()
        {
            var course = new UCourseDetached("C1");
            var structureSet = new UStructureSetDetached("ss");
            var planSetup1 =
                new UPlanSetupDetached("plan1", Option.Some<uint>(12), new UDose(60, UDose.UDoseUnit.Gy), structureSet);
            var planSetup2 =
                new UPlanSetupDetached("plan1", Option.Some<uint>(12), new UDose(60, UDose.UDoseUnit.Gy), structureSet);
            course.AddPlanSetup(planSetup1);
            Assert.Throws<ArgumentException>(() => course.AddPlanSetup(planSetup2));
        }

        [Test]
        public void AddPlanSetupWithId_Test()
        {
            var course = new UCourseDetached("C1");
            var structureSet = new UStructureSetDetached("ss");
            var planSetup1 = course.AddPlanSetupWithId("plan1", Option.Some<uint>(12), new UDose(60, UDose.UDoseUnit.Gy), structureSet);
            var planSetup2 = course.AddPlanSetupWithId("plan2", Option.Some<uint>(12), new UDose(60, UDose.UDoseUnit.Gy), structureSet);

            var resultPlanSetups = course.PlanSetups;
            var resultPlan1 = resultPlanSetups?.FirstOrDefault(x => x.Id == "plan1");
            var resultPlan2 = resultPlanSetups?.FirstOrDefault(x => x.Id == "plan2");

            Assert.AreEqual(2, resultPlanSetups?.Count());
            Assert.NotNull(resultPlan1);
            Assert.NotNull(resultPlan2);
            Assert.AreEqual(planSetup1.Id, resultPlan1.Id);
            Assert.AreEqual(planSetup1.StructureSet.Id, resultPlan1.StructureSet.Id);
            Assert.AreEqual(planSetup1.NumberOfFractions, resultPlan1.NumberOfFractions);
            Assert.AreEqual(planSetup1.TotalPrescriptionDose.Value, resultPlan1.TotalPrescriptionDose.Value);
            Assert.AreEqual(planSetup1.TotalPrescriptionDose.Unit, resultPlan1.TotalPrescriptionDose.Unit);
            Assert.AreEqual(planSetup2.Id, resultPlan2.Id);
            Assert.AreEqual(planSetup2.StructureSet.Id, resultPlan2.StructureSet.Id);
            Assert.AreEqual(planSetup2.NumberOfFractions, resultPlan2.NumberOfFractions);
            Assert.AreEqual(planSetup2.TotalPrescriptionDose.Value, resultPlan2.TotalPrescriptionDose.Value);
            Assert.AreEqual(planSetup2.TotalPrescriptionDose.Unit, resultPlan2.TotalPrescriptionDose.Unit);
        }

        [Test]
        public void AddPlanSetupWithId_ThrowArgumentErrorIfPlanWithIdAlreadyExists_Test()
        {
            var course = new UCourseDetached("C1");
            var structureSet = new UStructureSetDetached("ss");
            var planSetup1 = course.AddPlanSetupWithId("plan1", Option.Some<uint>(12),
                new UDose(60, UDose.UDoseUnit.Gy), structureSet);
            Assert.Throws<ArgumentException>(() => course.AddPlanSetupWithId("plan1", Option.Some<uint>(12),
                new UDose(60, UDose.UDoseUnit.Gy), structureSet));
        }
    }
}
