﻿using System;

namespace RadiationTreatmentPlanner.Utils.Dose
{
    public static class UDoseConverter
    {
        public static UDose ToUDose(this string doseAsString)
        {
            var doseUnit = UDose.UDoseUnit.Unknown;
            var doseUnitExtractionResult = DoseUnitExtractor.Extract(doseAsString);
            if (doseUnitExtractionResult.Success)
            {
                doseUnit = doseUnitExtractionResult.DoseUnit;
                doseAsString = doseAsString.Replace(doseUnit.ToDescription(), "");
            }
            var value = UStringHelper.ConvertStringToDoubleOrDefault(doseAsString);
            return new UDose(value, doseUnit);
        }

        /// <summary>
        /// Convert physical dose to equivalent dose in 2Gy fractions with
        /// the linear-quadratic model which describes the cell survival curve after
        /// exposure to radiation.
        /// </summary>
        /// <param name="dose">Physical dose.</param>
        /// <param name="alphaOverBeta">Alpha/Beta ratio of the LQ model (> 0).</param>
        /// <param name="numberOfFractions">Number of fractions (> 0).</param>
        /// <returns>UDose in EQD2</returns>
        public static UDose PhysicalToEqd2(this UDose dose, double alphaOverBeta, uint numberOfFractions)
        {
            if (alphaOverBeta <= 0)
                throw new ArgumentOutOfRangeException(
                    $"{nameof(alphaOverBeta)} must be larger zero, but was {alphaOverBeta}.");
            if (numberOfFractions == 0)
                throw new ArgumentOutOfRangeException(
                    $"{nameof(numberOfFractions)} must be larger zero, but was {numberOfFractions}.");

            var dosePerFraction = dose.Value / numberOfFractions;
            var dividend = dose.Value * (1 + dosePerFraction / alphaOverBeta);
            var divisor = 1 + 2 / alphaOverBeta;
            var eqd2 = dividend / divisor;
            return new UDose(eqd2, dose.Unit);
        }

        /// <summary>
        /// Convert equivalent dose in 0Gy fractions to equivalent dose in 2Gy fractions with
        /// the linear-quadratic model which describes the cell survival curve after
        /// exposure to radiation.
        /// </summary>
        /// <param name="dose">EQD0 dose.</param>
        /// <param name="alphaOverBeta">Alpha/Beta ratio of the LQ model (> 0).</param>
        /// <returns>UDose in EQD2</returns>
        public static UDose Eqd0ToEqd2(this UDose dose, double alphaOverBeta)
        {
            if (alphaOverBeta <= 0)
                throw new ArgumentOutOfRangeException(
                    $"{nameof(alphaOverBeta)} must be larger zero, but was {alphaOverBeta}.");

            var eqd2 = dose.Value / (1.0 + 2.0 / alphaOverBeta);
            return new UDose(eqd2, dose.Unit);
        }

        /// <summary>
        /// Convert physical dose to equivalent dose in 0Gy fractions with
        /// the linear-quadratic model which describes the cell survival curve after
        /// exposure to radiation.
        /// </summary>
        /// <param name="dose">Physical dose.</param>
        /// <param name="alphaOverBeta">Alpha/Beta ratio of the LQ model (> 0).</param>
        /// <param name="numberOfFractions">Number of fractions (> 0).</param>
        /// <returns>UDose in EQD0</returns>
        public static UDose PhysicalToEqd0(this UDose dose, double alphaOverBeta, uint numberOfFractions)
        {
            if (alphaOverBeta <= 0)
                throw new ArgumentOutOfRangeException(
                    $"{nameof(alphaOverBeta)} must be larger zero, but was {alphaOverBeta}.");
            if (numberOfFractions == 0)
                throw new ArgumentOutOfRangeException(
                    $"{nameof(numberOfFractions)} must be larger zero, but was {numberOfFractions}.");

            var dosePerFraction = dose.Value / numberOfFractions;
            var eqd0 = dose.Value * (1 + dosePerFraction / alphaOverBeta);
            return new UDose(eqd0, dose.Unit);
        }

        /// <summary>
        /// Convert equivalent dose in 2Gy fractions to equivalent dose in 0Gy fractions with
        /// the linear-quadratic model which describes the cell survival curve after
        /// exposure to radiation.
        /// </summary>
        /// <param name="dose">EQD2 dose.</param>
        /// <param name="alphaOverBeta">Alpha/Beta ratio of the LQ model (> 0).</param>
        /// <returns>UDose in EQD0</returns>
        public static UDose Eqd2ToEqd0(this UDose dose, double alphaOverBeta)
        {
            if (alphaOverBeta <= 0)
                throw new ArgumentOutOfRangeException(
                    $"{nameof(alphaOverBeta)} must be larger zero, but was {alphaOverBeta}.");

            var eqd2 = dose.Value * (1.0 + 2.0 / alphaOverBeta);
            return new UDose(eqd2, dose.Unit);
        }
    }
}
