﻿using System.Linq;
using NUnit.Framework;

namespace RadiationTreatmentPlanner.Utils.Tests
{
    [TestFixture]
    public class PIZManagerTests
    {
        [Test]
        public void PIZManager_Is_Empty_Test()
        {
            var pmgr = new PIZManager();

            Assert.AreEqual(0, pmgr.Size());
        }

        [Test]
        public void PIZManager_IsNot_Empty_Test()
        {
            var pmgr_nonempty = new PIZManager("1", "2", "3");

            Assert.AreEqual(3, pmgr_nonempty.Size());
        }

        [Test]
        public void PIZManager_AddPIZ_Test()
        {
            var pmgr = new PIZManager();
            pmgr.AddPIZ("76276");

            Assert.AreEqual(true, pmgr.GetAllPIZs().Contains("76276"));
        }

        [Test]
        public void PIZManager_GetNextPIZ_Test()
        {
            var pmgr = new PIZManager("1", "2");

            Assert.AreEqual(true, pmgr.GetAllPIZs().Contains("1"));
            Assert.AreEqual(true, pmgr.GetAllPIZs().Contains("2"));
        }
    }
}
