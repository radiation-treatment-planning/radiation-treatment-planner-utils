﻿using System;

namespace RadiationTreatmentPlanner.Utils.OptimizationObjetives
{
    /// <summary>
    /// A priority in the range of [0, 1000].
    /// </summary>
    public readonly struct Priority
    {
        public const double MaxValue = 1000;
        public const double MinValue = 0;
        public double Value { get; }

        public Priority(double value)
        {
            if (value < MinValue || value > MaxValue)
                throw new ArgumentException($"Priority must be a value between {MinValue} and {MaxValue}, but was {value}.");
            Value = value;
        }

        public static class Factory
        {
            public static Priority BuildPriority(double value) => new Priority(value);
            public static Priority BuildMaximumPriority() => new Priority(MaxValue);
        }
    }
}
