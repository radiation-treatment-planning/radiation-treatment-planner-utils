﻿using System;
using System.Collections.Generic;
using System.Linq;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;

namespace RadiationTreatmentPlanner.Utils.Patient
{
    public class UCourseDetached : IUCourse
    {
        public string Id { get; }

        private readonly List<UPlanSetupDetached> _planSetups;
        public IEnumerable<UPlanSetupDetached> PlanSetups => _planSetups.AsEnumerable();

        public UCourseDetached(string id)
        {
            Id = id;
            _planSetups = new List<UPlanSetupDetached>();
        }

        public void AddPlanSetup(UPlanSetupDetached planSetup)
        {
            ThrowExceptionIfPlanWithIdAlreadyExists(planSetup.Id);
            _planSetups.Add(planSetup);
        }

        public UPlanSetupDetached AddPlanSetupWithId(string planSetupId, Option<uint> numberOfFractions,
            UDose totalPrescriptionDose,
            UStructureSetDetached structureSet)
        {
            ThrowExceptionIfPlanWithIdAlreadyExists(planSetupId);
            var planSetup = new UPlanSetupDetached(planSetupId, numberOfFractions, totalPrescriptionDose, structureSet);
            _planSetups.Add(planSetup);
            return planSetup;
        }

        private void ThrowExceptionIfPlanWithIdAlreadyExists(string id)
        {
            if (_planSetups.Any(x => x.Id == id))
                throw new ArgumentException(
                    $"Plan setup with id {id} already exists and hence cannot be added.");
        }
    }
}
