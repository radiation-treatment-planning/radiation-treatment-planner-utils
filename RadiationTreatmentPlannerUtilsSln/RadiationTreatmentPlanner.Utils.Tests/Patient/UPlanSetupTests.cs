﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Media3D;
using NUnit.Framework;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.Fields;
using RadiationTreatmentPlanner.Utils.OptimizationObjetives;
using RadiationTreatmentPlanner.Utils.Patient;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.Tests.Patient
{
    [TestFixture]
    public class UPlanSetupTests
    {

        [Test]
        public void SetTotalDose_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureDetached = new UStructureDetached("s1", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            structureSetDetached.AddStructure(structureDetached);
            var structureSet = patient.AddStructureSet(structureSetDetached);
            var course = patient.AddCourseWithId("C1");
            var planSetup = course.AddPlanSetupWithId("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSet);
            planSetup.SetTotalDose(new UDose(1000, UDose.UDoseUnit.cGy));
            Assert.AreEqual(1000, planSetup.TotalPrescriptionDose.Value);
            Assert.AreEqual(UDose.UDoseUnit.cGy, planSetup.TotalPrescriptionDose.Unit);
        }

        [Test]
        public void SetNumberOfFractions_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureDetached = new UStructureDetached("s1", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            structureSetDetached.AddStructure(structureDetached);
            var structureSet = patient.AddStructureSet(structureSetDetached);
            var course = patient.AddCourseWithId("C1");
            var planSetup = course.AddPlanSetupWithId("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSet);
            planSetup.SetNumberOfFractions(Option.Some<uint>(10));
            Assert.AreEqual(Option.Some<uint>(10), planSetup.NumberOfFractions);
        }

        [Test]
        public void AddDvhEstimate_DvhPropertiesAsArguments_Test()
        {
            var dvhFileParser = DVHFileParser.CreateFromFile("FilesForTesting/CumulativeDvhExampleFile.txt");
            var dvhFileParser2 = DVHFileParser.CreateFromFile("FilesForTesting/CumulativeDvhExampleFile3.txt");

            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureGtv = new UStructureDetached("GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureOar = new UStructureDetached("OAR", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            structureSetDetached.AddStructure(structureGtv);
            structureSetDetached.AddStructure(structureOar);
            var structureSet = patient.AddStructureSet(structureSetDetached);
            var course = patient.AddCourseWithId("C1");
            var planSetup = course.AddPlanSetupWithId("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSet);

            Assert.IsEmpty(planSetup.DvhEstimates);

            planSetup.AddDvhEstimate(dvhFileParser.GetStructure(), dvhFileParser.GetCoverage(),
                dvhFileParser.GetMinDose(),
                dvhFileParser.GetMedianDose(), dvhFileParser.GetMedianDose(), dvhFileParser.GetMaxDose(),
                dvhFileParser.GetSamplingCoverage(), dvhFileParser.GetStandardDeviationDose(),
                dvhFileParser.GetTotalVolume(), dvhFileParser.GetDVHCurve());

            planSetup.AddDvhEstimate(dvhFileParser2.GetStructure(), dvhFileParser2.GetCoverage(),
                dvhFileParser2.GetMinDose(),
                dvhFileParser2.GetMedianDose(), dvhFileParser2.GetMedianDose(), dvhFileParser2.GetMaxDose(),
                dvhFileParser2.GetSamplingCoverage(), dvhFileParser2.GetStandardDeviationDose(),
                dvhFileParser2.GetTotalVolume(), dvhFileParser2.GetDVHCurve());

            var dvhEstimate1 = planSetup.DvhEstimates.FirstOrDefault();
            Assert.NotNull(dvhEstimate1);
            Assert.AreEqual(dvhFileParser.GetStructure(), dvhEstimate1.Structure.Id);
            Assert.AreEqual(dvhFileParser.GetCoverage(), dvhEstimate1.Coverage);
            Assert.AreEqual(dvhFileParser.GetSamplingCoverage(), dvhEstimate1.SamplingCoverage);
            Assert.AreEqual(dvhFileParser.GetDVHCurve(), dvhEstimate1.DvhCurve);
            Assert.AreEqual(dvhFileParser.GetMinDose(), dvhEstimate1.MinDose);
            Assert.AreEqual(dvhFileParser.GetMedianDose(), dvhEstimate1.MedianDose);
            Assert.AreEqual(dvhFileParser.GetMedianDose(), dvhEstimate1.MeanDose);
            Assert.AreEqual(dvhFileParser.GetMaxDose(), dvhEstimate1.MaxDose);
            Assert.AreEqual(dvhFileParser.GetStandardDeviationDose(), dvhEstimate1.StandardDeviationDose);

            var dvhEstimate2 = planSetup.DvhEstimates.LastOrDefault();
            Assert.NotNull(dvhEstimate2);
            Assert.AreEqual(dvhFileParser2.GetStructure(), dvhEstimate2.Structure.Id);
            Assert.AreEqual(dvhFileParser2.GetCoverage(), dvhEstimate2.Coverage);
            Assert.AreEqual(dvhFileParser2.GetSamplingCoverage(), dvhEstimate2.SamplingCoverage);
            Assert.AreEqual(dvhFileParser2.GetDVHCurve(), dvhEstimate2.DvhCurve);
            Assert.AreEqual(dvhFileParser2.GetMinDose(), dvhEstimate2.MinDose);
            Assert.AreEqual(dvhFileParser2.GetMedianDose(), dvhEstimate2.MedianDose);
            Assert.AreEqual(dvhFileParser2.GetMedianDose(), dvhEstimate2.MeanDose);
            Assert.AreEqual(dvhFileParser2.GetMaxDose(), dvhEstimate2.MaxDose);
            Assert.AreEqual(dvhFileParser2.GetStandardDeviationDose(), dvhEstimate2.StandardDeviationDose);
        }

        [Test]
        public void AddOptimizationObjectiveRange_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureGtvDetached = new UStructureDetached("GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureOarDetached = new UStructureDetached("OAR", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureSet = patient.AddStructureSet(structureSetDetached);
            var structureGtv = structureSet.AddStructure(structureGtvDetached);
            var structureOar = structureSet.AddStructure(structureOarDetached);
            var course = patient.AddCourseWithId("C1");
            var planSetup = course.AddPlanSetupWithId("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSet);

            var objectives = new IObjective[]
            {
                new PointObjective("GTV", ObjectiveOperator.LOWER, new Priority(500), new UDose(66, UDose.UDoseUnit.Gy),
                    new UVolume(98, UVolume.VolumeUnit.percent)),
                new EUDObjective("OAR", ObjectiveOperator.LOWER, new Priority(500), new UDose(40, UDose.UDoseUnit.Gy),
                    40),
                new MeanDoseObjective("GTV", new UDose(72, UDose.UDoseUnit.Gy), new Priority(500))
            };

            var optimizationObjectiveAddResult = planSetup.AddOptimizationObjectiveRange(objectives);

            var result = planSetup.OptimizationObjectives.ToArray();
            var pointObjective = result[0];
            var eudObjective = result[1];
            var meanDoseObjective = result[2];


            Assert.AreEqual(structureGtv.Id, pointObjective.StructureId);
            Assert.IsTrue(pointObjective is PointObjective);

            Assert.AreEqual(structureOar.Id, eudObjective.StructureId);
            Assert.IsTrue(eudObjective is EUDObjective);

            Assert.AreEqual(structureGtv.Id, meanDoseObjective.StructureId);
            Assert.IsTrue(meanDoseObjective is MeanDoseObjective);

            Assert.IsTrue(optimizationObjectiveAddResult.Success);
        }

        [Test]
        public void AddOptimizationObjectiveRange_SuccessIsFalse_IfStructureDoesNotExistInStructureSet_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureGtvDetached = new UStructureDetached("GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureOarDetached = new UStructureDetached("OAR", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureSet = patient.AddStructureSet(structureSetDetached);
            var structureGtv = structureSet.AddStructure(structureGtvDetached);
            var structureOar = structureSet.AddStructure(structureOarDetached);
            var course = patient.AddCourseWithId("C1");
            var planSetup = course.AddPlanSetupWithId("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSet);

            var objectives = new IObjective[]
            {
                new PointObjective("Not Available Structure", ObjectiveOperator.LOWER, new Priority(500), new UDose(66, UDose.UDoseUnit.Gy),
                    new UVolume(98, UVolume.VolumeUnit.percent)),
                new EUDObjective("OAR", ObjectiveOperator.LOWER, new Priority(500), new UDose(40, UDose.UDoseUnit.Gy),
                    40),
                new MeanDoseObjective("GTV", new UDose(72, UDose.UDoseUnit.Gy), new Priority(500))
            };

            var optimizationObjectiveAddResult = planSetup.AddOptimizationObjectiveRange(objectives);
            Assert.Zero(planSetup.OptimizationObjectives.Count());
            Assert.IsFalse(optimizationObjectiveAddResult.Success);
        }

        [Test]
        public void AddDvhEstimate_UDvhEstimateDetachedAsArgument_Test()
        {
            var dvhFileParser = DVHFileParser.CreateFromFile("FilesForTesting/CumulativeDvhExampleFile.txt");
            var dvhFileParser2 = DVHFileParser.CreateFromFile("FilesForTesting/CumulativeDvhExampleFile3.txt");

            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureGtv = new UStructureDetached("GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureOar = new UStructureDetached("OAR", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            structureSetDetached.AddStructure(structureGtv);
            structureSetDetached.AddStructure(structureOar);
            var structureSet = patient.AddStructureSet(structureSetDetached);
            var course = patient.AddCourseWithId("C1");
            var planSetup = course.AddPlanSetupWithId("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSet);

            Assert.IsEmpty(planSetup.DvhEstimates);

            var dvhEstimateDetached1 = new UDvhEstimateDetached("plan1", dvhFileParser.GetStructure(), dvhFileParser.GetCoverage(), dvhFileParser.GetMinDose(),
                dvhFileParser.GetMedianDose(), dvhFileParser.GetMedianDose(), dvhFileParser.GetMaxDose(),
                dvhFileParser.GetSamplingCoverage(), dvhFileParser.GetStandardDeviationDose(),
                dvhFileParser.GetTotalVolume(), dvhFileParser.GetDVHCurve());

            var dvhEstimateDetached2 = new UDvhEstimateDetached("plan1", dvhFileParser2.GetStructure(),
                dvhFileParser2.GetCoverage(), dvhFileParser2.GetMinDose(),
                dvhFileParser2.GetMedianDose(), dvhFileParser2.GetMedianDose(), dvhFileParser2.GetMaxDose(),
                dvhFileParser2.GetSamplingCoverage(), dvhFileParser2.GetStandardDeviationDose(),
                dvhFileParser2.GetTotalVolume(), dvhFileParser2.GetDVHCurve());

            planSetup.AddDvhEstimate(dvhEstimateDetached1);
            planSetup.AddDvhEstimate(dvhEstimateDetached2);

            var dvhEstimate1 = planSetup.DvhEstimates.FirstOrDefault();
            Assert.NotNull(dvhEstimate1);
            Assert.AreEqual(dvhFileParser.GetStructure(), dvhEstimate1.Structure.Id);
            Assert.AreEqual(dvhFileParser.GetCoverage(), dvhEstimate1.Coverage);
            Assert.AreEqual(dvhFileParser.GetSamplingCoverage(), dvhEstimate1.SamplingCoverage);
            Assert.AreEqual(dvhFileParser.GetDVHCurve(), dvhEstimate1.DvhCurve);
            Assert.AreEqual(dvhFileParser.GetMinDose(), dvhEstimate1.MinDose);
            Assert.AreEqual(dvhFileParser.GetMedianDose(), dvhEstimate1.MedianDose);
            Assert.AreEqual(dvhFileParser.GetMedianDose(), dvhEstimate1.MeanDose);
            Assert.AreEqual(dvhFileParser.GetMaxDose(), dvhEstimate1.MaxDose);
            Assert.AreEqual(dvhFileParser.GetStandardDeviationDose(), dvhEstimate1.StandardDeviationDose);

            var dvhEstimate2 = planSetup.DvhEstimates.LastOrDefault();
            Assert.NotNull(dvhEstimate2);
            Assert.AreEqual(dvhFileParser2.GetStructure(), dvhEstimate2.Structure.Id);
            Assert.AreEqual(dvhFileParser2.GetCoverage(), dvhEstimate2.Coverage);
            Assert.AreEqual(dvhFileParser2.GetSamplingCoverage(), dvhEstimate2.SamplingCoverage);
            Assert.AreEqual(dvhFileParser2.GetDVHCurve(), dvhEstimate2.DvhCurve);
            Assert.AreEqual(dvhFileParser2.GetMinDose(), dvhEstimate2.MinDose);
            Assert.AreEqual(dvhFileParser2.GetMedianDose(), dvhEstimate2.MedianDose);
            Assert.AreEqual(dvhFileParser2.GetMedianDose(), dvhEstimate2.MeanDose);
            Assert.AreEqual(dvhFileParser2.GetMaxDose(), dvhEstimate2.MaxDose);
            Assert.AreEqual(dvhFileParser2.GetStandardDeviationDose(), dvhEstimate2.StandardDeviationDose);
        }

        [Test]
        public void AddVMATArc_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureGtvDetached = new UStructureDetached("GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureOarDetached = new UStructureDetached("OAR", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureSet = patient.AddStructureSet(structureSetDetached);
            var structureGtv = structureSet.AddStructure(structureGtvDetached);
            var structureOar = structureSet.AddStructure(structureOarDetached);
            var course = patient.AddCourseWithId("C1");
            var planSetup = course.AddPlanSetupWithId("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSet);

            var id = "ARC1";
            var gantryRotation = new GantryRotation(new AngleInDegree(10), new AngleInDegree(170), RotationDirection.Clockwise);
            var collimatorRotation = new AngleInDegree(35);
            var couchRotation = new AngleInDegree(0);
            var arc = new VMATArc(id, gantryRotation, collimatorRotation, couchRotation);

            Assert.IsEmpty(planSetup.VmatArcs.ToList());
            planSetup.AddVMATArc(arc);
            var result = planSetup.VmatArcs.ToList();
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(arc, result.First());
        }

        [Test]
        public void AddVMATArc_ThrowArgumentExceptionIfArcWithSameIdAlreadyExists_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureGtvDetached = new UStructureDetached("GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureOarDetached = new UStructureDetached("OAR", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureSet = patient.AddStructureSet(structureSetDetached);
            var structureGtv = structureSet.AddStructure(structureGtvDetached);
            var structureOar = structureSet.AddStructure(structureOarDetached);
            var course = patient.AddCourseWithId("C1");
            var planSetup = course.AddPlanSetupWithId("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSet);

            var id = "ARC1";
            var gantryRotation = new GantryRotation(new AngleInDegree(10), new AngleInDegree(170), RotationDirection.Clockwise);
            var collimatorRotation = new AngleInDegree(35);
            var couchRotation = new AngleInDegree(0);
            var arc = new VMATArc(id, gantryRotation, collimatorRotation, couchRotation);
            var arc2 = new VMATArc(id, gantryRotation, collimatorRotation, couchRotation);

            planSetup.AddVMATArc(arc);
            Assert.Throws<ArgumentException>(() => planSetup.AddVMATArc(arc2));
        }

        [Test]
        public void AddVMATArcRange_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureGtvDetached = new UStructureDetached("GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureOarDetached = new UStructureDetached("OAR", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureSet = patient.AddStructureSet(structureSetDetached);
            var structureGtv = structureSet.AddStructure(structureGtvDetached);
            var structureOar = structureSet.AddStructure(structureOarDetached);
            var course = patient.AddCourseWithId("C1");
            var planSetup = course.AddPlanSetupWithId("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSet);

            var id1 = "ARC1";
            var id2 = "ARC2";
            var gantryRotation = new GantryRotation(new AngleInDegree(10), new AngleInDegree(170), RotationDirection.Clockwise);
            var collimatorRotation = new AngleInDegree(5);
            var collimatorRotation2 = new AngleInDegree(355);
            var couchRotation = new AngleInDegree(0);
            var arc1 = new VMATArc(id1, gantryRotation, collimatorRotation, couchRotation);
            var arc2 = new VMATArc(id2, gantryRotation, collimatorRotation2, couchRotation);
            var arcs = new List<VMATArc> { arc1, arc2 };

            Assert.IsEmpty(planSetup.VmatArcs.ToList());
            planSetup.AddVMATArcRange(arcs);
            var result = planSetup.VmatArcs.ToList();
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(arc1, result.First());
            Assert.AreEqual(arc2, result.Last());
        }

        [Test]
        public void AddVMATArcRange_ThrowArgumentExceptionIfArcWithSameIdAlreadyExists_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureGtvDetached = new UStructureDetached("GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureOarDetached = new UStructureDetached("OAR", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureSet = patient.AddStructureSet(structureSetDetached);
            var structureGtv = structureSet.AddStructure(structureGtvDetached);
            var structureOar = structureSet.AddStructure(structureOarDetached);
            var course = patient.AddCourseWithId("C1");
            var planSetup = course.AddPlanSetupWithId("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSet);

            var id = "ARC1";
            var gantryRotation = new GantryRotation(new AngleInDegree(10), new AngleInDegree(170), RotationDirection.Clockwise);
            var collimatorRotation = new AngleInDegree(5);
            var collimatorRotation2 = new AngleInDegree(355);
            var couchRotation = new AngleInDegree(0);
            var arc1 = new VMATArc(id, gantryRotation, collimatorRotation, couchRotation);
            var arc2 = new VMATArc(id, gantryRotation, collimatorRotation2, couchRotation);
            var arcs = new List<VMATArc> { arc1, arc2 };

            Assert.Throws<ArgumentException>(() => planSetup.AddVMATArcRange(arcs));
        }

        [Test]
        public void Detach_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureGtvDetached = new UStructureDetached("GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureOarDetached = new UStructureDetached("OAR", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureSet = patient.AddStructureSet(structureSetDetached);
            var structureGtv = structureSet.AddStructure(structureGtvDetached);
            var structureOar = structureSet.AddStructure(structureOarDetached);
            var course = patient.AddCourseWithId("C1");
            var planSetup = course.AddPlanSetupWithId("plan1", Option.Some<uint>(12), new UDose(12, UDose.UDoseUnit.Gy),
                structureSet);

            // Add ARCs.
            var id1 = "ARC1";
            var id2 = "ARC2";
            var gantryRotation = new GantryRotation(new AngleInDegree(10), new AngleInDegree(170), RotationDirection.Clockwise);
            var collimatorRotation = new AngleInDegree(5);
            var collimatorRotation2 = new AngleInDegree(355);
            var couchRotation = new AngleInDegree(0);
            var arc1 = new VMATArc(id1, gantryRotation, collimatorRotation, couchRotation);
            var arc2 = new VMATArc(id2, gantryRotation, collimatorRotation2, couchRotation);
            var arcs = new List<VMATArc> { arc1, arc2 };
            planSetup.AddVMATArcRange(arcs);

            // Add optimization objectives.
            var objectives = new IObjective[]
            {
                new PointObjective("GTV", ObjectiveOperator.LOWER, new Priority(500), new UDose(66, UDose.UDoseUnit.Gy),
                    new UVolume(98, UVolume.VolumeUnit.percent)),
                new EUDObjective("OAR", ObjectiveOperator.LOWER, new Priority(500), new UDose(40, UDose.UDoseUnit.Gy),
                    40),
                new MeanDoseObjective("GTV", new UDose(72, UDose.UDoseUnit.Gy), new Priority(500))
            };
            planSetup.AddOptimizationObjectiveRange(objectives);

            // Add DVH estimates.
            var dvhFileParser = DVHFileParser.CreateFromFile("FilesForTesting/CumulativeDvhExampleFile.txt");
            var dvhFileParser2 = DVHFileParser.CreateFromFile("FilesForTesting/CumulativeDvhExampleFile3.txt");

            var dvhEstimateDetached1 = new UDvhEstimateDetached("plan1", dvhFileParser.GetStructure(), dvhFileParser.GetCoverage(), dvhFileParser.GetMinDose(),
                dvhFileParser.GetMedianDose(), dvhFileParser.GetMedianDose(), dvhFileParser.GetMaxDose(),
                dvhFileParser.GetSamplingCoverage(), dvhFileParser.GetStandardDeviationDose(),
                dvhFileParser.GetTotalVolume(), dvhFileParser.GetDVHCurve());

            var dvhEstimateDetached2 = new UDvhEstimateDetached("plan1", dvhFileParser2.GetStructure(),
                dvhFileParser2.GetCoverage(), dvhFileParser2.GetMinDose(),
                dvhFileParser2.GetMedianDose(), dvhFileParser2.GetMedianDose(), dvhFileParser2.GetMaxDose(),
                dvhFileParser2.GetSamplingCoverage(), dvhFileParser2.GetStandardDeviationDose(),
                dvhFileParser2.GetTotalVolume(), dvhFileParser2.GetDVHCurve());

            planSetup.AddDvhEstimate(dvhEstimateDetached1);
            planSetup.AddDvhEstimate(dvhEstimateDetached2);


            var result = planSetup.Detach();
            Assert.AreEqual(2, result.VmatArcs.Count());
            Assert.AreEqual(3, result.OptimizationObjectives.Count());
            Assert.AreEqual(2, result.DvhEstimates.Count());
        }
    }
}
