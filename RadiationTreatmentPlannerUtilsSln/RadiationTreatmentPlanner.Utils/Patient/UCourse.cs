﻿using System;
using System.Collections.Generic;
using System.Linq;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;

namespace RadiationTreatmentPlanner.Utils.Patient
{
    public class UCourse : IUCourse
    {
        public string Id { get; }
        public UPatient Patient { get; }
        private readonly List<UPlanSetup> _planSetups;
        public IEnumerable<UPlanSetup> PlanSetups => _planSetups.AsEnumerable();

        internal UCourse(string id, UPatient patient)
        {
            Id = id;
            Patient = patient;
            _planSetups = new List<UPlanSetup>();
        }

        public UPlanSetup AddPlanSetup(UPlanSetupDetached planSetupDetached)
        {
            if (_planSetups.Any(x => x.Id == planSetupDetached.Id))
                throw new ArgumentException(
                    $"Plan setup with planSetupId {planSetupDetached.Id} already exists and hence cannot be added.");
            var structureSet = Patient.StructureSets.FirstOrDefault(x => x.Id == planSetupDetached.StructureSet.Id);
            if (structureSet is null)
                throw new ArgumentException(
                    $"Cannot add plan setup, because linked structure set with id {planSetupDetached.StructureSet.Id} does not exist.");
            var planSetup = new UPlanSetup(planSetupDetached.Id, Patient, this, planSetupDetached.NumberOfFractions,
                planSetupDetached.TotalPrescriptionDose, structureSet);
            var optimizationObjectiveAddResult = planSetup.AddOptimizationObjectiveRange(planSetupDetached.OptimizationObjectives);
            if (!optimizationObjectiveAddResult.Success)
                throw new ArgumentException(
                    "Cannot add plan setup, because cannot copy optimization objectives, for the following reason:\n" +
                    $"{optimizationObjectiveAddResult.Info}");
            planSetup.AddVMATArcRange(planSetupDetached.VmatArcs);
            _planSetups.Add(planSetup);
            return planSetup;
        }

        public UPlanSetup AddPlanSetupWithId(string planSetupId, Option<uint> numberOfFractions, UDose totalPrescriptionDose,
            UStructureSet structureSet)
        {
            if (_planSetups.Any(x => x.Id == planSetupId))
                throw new ArgumentException(
                    $"Plan setup with planSetupId {planSetupId} already exists and hence cannot be added.");
            var planSetup = new UPlanSetup(planSetupId, Patient, this, numberOfFractions, totalPrescriptionDose,
                structureSet);
            _planSetups.Add(planSetup);
            return planSetup;
        }

        /// <summary>
        /// Creates a detached version of the course.
        /// </summary>
        /// <returns>Detached copy of course.</returns>
        public UCourseDetached Detach()
        {
            var course = new UCourseDetached(Id);
            foreach (var planSetup in PlanSetups) course.AddPlanSetup(planSetup.Detach());
            return course;
        }
    }
}
