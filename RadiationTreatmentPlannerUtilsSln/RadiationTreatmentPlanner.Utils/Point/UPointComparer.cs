﻿using System.Collections.Generic;

namespace RadiationTreatmentPlanner.Utils.Point
{
    public class UPointComparer : Comparer<UPoint>
    {
        public override int Compare(UPoint pointA, UPoint pointB)
        {
            return pointA.X.CompareTo(pointB.X) != 0 ? pointA.X.CompareTo(pointB.X) : pointA.Y.CompareTo(pointB.Y);
        }
    }
}
