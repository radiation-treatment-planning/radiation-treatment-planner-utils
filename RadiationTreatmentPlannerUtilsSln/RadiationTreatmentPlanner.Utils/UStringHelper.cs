﻿using System;
using System.Globalization;

namespace RadiationTreatmentPlanner.Utils
{
    public static class UStringHelper
    {
        public static double ConvertStringToDoubleOrDefault(string stringNumber, double defaultValue = double.NaN)
        {
            var provider = GetDoubleAndStringFormatProvider();
            double value;
            var success = double.TryParse(stringNumber, NumberStyles.Float, provider, out value);
            return !success ? defaultValue : value;
        }

        public static IFormatProvider GetDoubleAndStringFormatProvider() => new NumberFormatInfo
            {NumberDecimalSeparator = "."};

    }
}
