﻿using System;
using System.ComponentModel;
using System.Linq;

namespace RadiationTreatmentPlanner.Utils
{
    public static class EnumHelper
    {
        public static string ToDescription(this Enum value)
        {
            var fi = value.GetType().GetField(value.ToString());

            var attributes = fi.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];

            if (attributes != null && attributes.Any()) return attributes.First().Description;

            return value.ToString();
        }
    }
}
