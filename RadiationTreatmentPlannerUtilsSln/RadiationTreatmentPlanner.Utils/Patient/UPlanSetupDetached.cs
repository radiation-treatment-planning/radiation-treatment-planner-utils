﻿using System;
using System.Collections.Generic;
using System.Linq;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.Fields;
using RadiationTreatmentPlanner.Utils.OptimizationObjetives;

namespace RadiationTreatmentPlanner.Utils.Patient
{
    /// <summary>
    /// A PlanSetup that is detached from a patient. It can be used
    /// to build a new patient with PatientBuilder.
    /// </summary>
    public class UPlanSetupDetached : IUPlanSetup
    {
        public string Id { get; }
        public Option<uint> NumberOfFractions { get; private set; }
        public UDose TotalPrescriptionDose { get; private set; }
        public UStructureSetDetached StructureSet { get; }

        private readonly List<UDvhEstimateDetached> _dvhEstimates;
        public IEnumerable<UDvhEstimateDetached> DvhEstimates => _dvhEstimates;
        private readonly List<IObjective> _optimizationObjectives;
        public IEnumerable<IObjective> OptimizationObjectives => _optimizationObjectives;
        private readonly List<VMATArc> _vmatArcs;
        public IEnumerable<VMATArc> VmatArcs => _vmatArcs;

        public UPlanSetupDetached(string id, Option<uint> numberOfFractions, UDose totalPrescriptionDose, UStructureSetDetached structureSet)
        {
            Id = id;
            NumberOfFractions = numberOfFractions;
            TotalPrescriptionDose = totalPrescriptionDose;
            StructureSet = structureSet;
            _optimizationObjectives = new List<IObjective>();
            _vmatArcs = new List<VMATArc>();
            _dvhEstimates = new List<UDvhEstimateDetached>();
        }

        public void SetTotalDose(UDose totalPrescriptionDose) => TotalPrescriptionDose =
            totalPrescriptionDose ?? throw new ArgumentNullException(nameof(totalPrescriptionDose));
        public void SetNumberOfFractions(Option<uint> numberOfFractions) => NumberOfFractions = numberOfFractions;

        /// <summary>
        /// Add an optimization objective to plan setup.
        /// </summary>
        /// <param name="objective">An optimization objective</param>
        /// <returns></returns>
        public OptimizationObjectiveAddResult AddOptimizationObjective(IObjective objective)
        {
            if (objective == null) throw new ArgumentNullException(nameof(objective));
            if (StructureSet.Structures.Any(x => x.Id == objective.StructureId))
            {
                _optimizationObjectives.Add(objective);
                return new OptimizationObjectiveAddResult(true, "Successfully added one optimization objective.");
            }
            return new OptimizationObjectiveAddResult(false,
                $"Objective was not added, because there exists no structure with ID" +
                $" {objective.StructureId} in structure set with ID {StructureSet.Id}.");
        }

        /// <summary>
        /// Add multiple objectives to plan setup.
        /// </summary>
        /// <param name="objectives">Optimization objectives</param>
        /// <returns>OptimizationObjectiveAddResult</returns>
        public OptimizationObjectiveAddResult AddOptimizationObjectiveRange(IEnumerable<IObjective> objectives)
        {
            if (objectives == null) throw new ArgumentNullException(nameof(objectives));
            foreach (var objective in objectives)
            {
                if (StructureSet.Structures.All(x => x.Id != objective.StructureId))
                    return new OptimizationObjectiveAddResult(false,
                        $"No objectives were added, because there exists no structure with ID" +
                        $" {objective.StructureId} in structure set with ID {StructureSet.Id}.");
            }
            _optimizationObjectives.AddRange(objectives);
            return new OptimizationObjectiveAddResult(true,
                $"Successfully added {objectives.Count()} optimization objectives.");
        }

        /// <summary>
        /// Add VMAT arc.
        /// </summary>
        /// <param name="arc">VMAT arc.</param>
        public void AddVMATArc(VMATArc arc)
        {
            if (arc == null) throw new ArgumentNullException(nameof(arc));
            if (_vmatArcs.Any(x => x.Id == arc.Id))
                throw new ArgumentException($"There already exists an arc with ID {arc.Id}. Arc IDs must be unique.");
            _vmatArcs.Add(arc);
        }

        /// <summary>
        /// Add range of VMAT arcs.
        /// </summary>
        /// <param name="arcs">VMAT arcs.</param>
        public void AddVMATArcRange(IEnumerable<VMATArc> arcs)
        {
            if (arcs == null) throw new ArgumentNullException(nameof(arcs));
            foreach (var arc in arcs) AddVMATArc(arc);
        }

        public void AddDvhEstimate(UDvhEstimateDetached dvhEstimate)
        {
            if (dvhEstimate == null) throw new ArgumentNullException(nameof(dvhEstimate));
            if (StructureSet.Structures.All(x => x.Id != dvhEstimate.StructureId))
                throw new ArgumentException(
                    $"There exists no structure with ID {dvhEstimate.StructureId}" +
                    $" in structure set with ID {StructureSet.Id}.");
            if (_dvhEstimates.Any(x => x.StructureId == dvhEstimate.StructureId))
                throw new ArgumentException(
                    $"Cannot add DVH estimate, because there already " +
                    $"exists a DVH estimate for structure with ID {dvhEstimate.StructureId}.");
            _dvhEstimates.Add(dvhEstimate);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as UPlanSetupDetached);
        }

        public bool Equals(UPlanSetupDetached other)
        {
            if (other == null) return false;
            var otherObjectives = other.OptimizationObjectives.ToList();
            foreach (var objective in _optimizationObjectives)
                if (!otherObjectives.Contains(objective))
                    return false;

            var otherVmatArcs = other.VmatArcs.ToList();
            foreach (var arc in _vmatArcs)
                if (!otherVmatArcs.Contains(arc))
                    return false;

            return Id == other.Id
                   && NumberOfFractions.Equals(other.NumberOfFractions)
                   && TotalPrescriptionDose.Equals(other.TotalPrescriptionDose)
                   && StructureSet.Equals(other.StructureSet);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
