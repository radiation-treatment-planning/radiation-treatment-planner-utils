﻿using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.ROIType;

namespace RadiationTreatmentPlanner.Utils.Tests.ROIType
{
    [TestFixture]
    public class StructureIdAndRoiTypeDictionaryBuilderTests
    {
        [Test]
        public void Build_Test()
        {
            var result = StructureIdAndRoiTypeDictionaryBuilder.Initialize()
                .WithStructureIdAndRoiType("Bowel Small", RtRoiInterpretedTypePresets.SMALL_BOWEL())
                .WithStructureIdAndRoiType("PTV", RtRoiInterpretedTypePresets.PTV())
                .Build();

            Assert.IsTrue(result.ContainsROIType(RtRoiInterpretedTypePresets.SMALL_BOWEL()));
            Assert.IsTrue(result.ContainsROIType(RtRoiInterpretedTypePresets.PTV()));
            Assert.IsFalse(result.ContainsROIType(RtRoiInterpretedTypePresets.AVOIDANCE()));

            Assert.IsTrue(result.ContainsStructureId("Bowel Small"));
            Assert.IsTrue(result.ContainsStructureId("PTV"));
            Assert.IsFalse(result.ContainsStructureId("Avoidance"));

            string stringBowelSmall;
            var smallBowelIsContained = result.TryGetStructureIdWithROIType(RtRoiInterpretedTypePresets.SMALL_BOWEL(), out stringBowelSmall);

            Assert.AreEqual("Bowel Small", stringBowelSmall);
            Assert.IsTrue(smallBowelIsContained);

            string stringPtv;
            var ptvIsContained = result.TryGetStructureIdWithROIType(RtRoiInterpretedTypePresets.PTV(), out stringPtv);

            Assert.AreEqual("PTV", stringPtv);
            Assert.IsTrue(ptvIsContained);


            RtRoiInterpretedType typeBowellSmall;
            var bowelSmallTypeIsContained = result.TryGetROITypeOfStructureWithId("Bowel Small", out typeBowellSmall);

            Assert.AreEqual(RtRoiInterpretedTypePresets.SMALL_BOWEL(), typeBowellSmall);
            Assert.IsTrue(bowelSmallTypeIsContained);


            RtRoiInterpretedType typePtv;
            var ptvTypeIsContained = result.TryGetROITypeOfStructureWithId("PTV", out typePtv);

            Assert.AreEqual(RtRoiInterpretedTypePresets.PTV(), typePtv);
            Assert.IsTrue(ptvTypeIsContained);
        }

    }
}
