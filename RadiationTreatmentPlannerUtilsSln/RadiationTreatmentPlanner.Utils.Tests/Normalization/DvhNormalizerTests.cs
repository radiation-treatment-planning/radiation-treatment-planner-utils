﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.Normalization;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.Tests.Normalization
{
    [TestFixture]
    public class DvhNormalizerTests
    {
        [Test]
        public void NormalizeSoThatXPercentOfPrescriptionDoseCoversYPercentOfTargetVolume_Test()
        {
            var dvhCurve = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(40, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(5, UDose.UDoseUnit.Gy), new UVolume(30, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(15, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(20, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL, new UVolume(40, UVolume.VolumeUnit.ccm));

            var dvhCurve2 = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(80, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(60, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(20, UDose.UDoseUnit.Gy), new UVolume(40, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(30, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(40, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL, new UVolume(80, UVolume.VolumeUnit.ccm));

            var dvhEstimate = new UDvhEstimateDetached("P1", "S1", 1, dvhCurve.GetMinimumDose(), dvhCurve.GetMeanDose(),
                dvhCurve.GetMedianDose(), dvhCurve.GetMaximumDose(), 1, dvhCurve.GetStandardDeviationDose(),
                dvhCurve.TotalVolume(), dvhCurve);

            var dvhEstimate2 = new UDvhEstimateDetached("P1", "S2", 1, dvhCurve2.GetMinimumDose(), dvhCurve2.GetMeanDose(),
                dvhCurve2.GetMedianDose(), dvhCurve2.GetMaximumDose(), 1, dvhCurve2.GetStandardDeviationDose(),
                dvhCurve2.TotalVolume(), dvhCurve2);

            var totalPrescriptionDose = new UDose(15, UDose.UDoseUnit.Gy);

            var expectedDvhCurve = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(40, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(7.5, UDose.UDoseUnit.Gy), new UVolume(30, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(15, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(22.5, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(30, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL, new UVolume(40, UVolume.VolumeUnit.ccm));

            var expectedDvhCurve2 = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(80, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(15, UDose.UDoseUnit.Gy), new UVolume(60, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(30, UDose.UDoseUnit.Gy), new UVolume(40, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(45, UDose.UDoseUnit.Gy), new UVolume(20, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(60, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL, new UVolume(80, UVolume.VolumeUnit.ccm));

            var expectedDvhEstimate = new UDvhEstimateDetached("P1", "S1", 1, expectedDvhCurve.GetMinimumDose(),
                expectedDvhCurve.GetMeanDose(),
                expectedDvhCurve.GetMedianDose(), expectedDvhCurve.GetMaximumDose(), 1,
                expectedDvhCurve.GetStandardDeviationDose(),
                expectedDvhCurve.TotalVolume(), expectedDvhCurve);

            var expectedDvhEstimate2 = new UDvhEstimateDetached("P1", "S2", 1, expectedDvhCurve2.GetMinimumDose(),
                expectedDvhCurve2.GetMeanDose(),
                expectedDvhCurve2.GetMedianDose(), expectedDvhCurve2.GetMaximumDose(), 1,
                expectedDvhCurve2.GetStandardDeviationDose(),
                expectedDvhCurve2.TotalVolume(), expectedDvhCurve2);

            var normalizer = new DvhNormalizer();
            var result = normalizer.NormalizeSoThatXPercentOfPrescriptionDoseCoversYPercentOfTargetVolume("S1",
                new List<UDvhEstimateDetached> { dvhEstimate, dvhEstimate2 }, 100, 50, totalPrescriptionDose);
            Assert.AreEqual(1.5, result.NormalizationScore);
            Assert.AreEqual(expectedDvhEstimate, result.NormalizedDvhEstimates.First());
            Assert.AreEqual(expectedDvhEstimate2, result.NormalizedDvhEstimates.Last());
        }
    }
}
