﻿using System;

namespace RadiationTreatmentPlanner.Utils
{
    public class UProbability
    {
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public double Value { get; }
        public UProbability(double value)
        {
            if (value < 0 || value > 1)
                throw new ArgumentException("Probabilities cannot be lower than zero or larger than one.");

            Value = value;
        }

        public static bool operator ==(UProbability probability1, UProbability probability2)
        {
            return probability1.Value == probability2.Value;
        }

        public static bool operator !=(UProbability probability1, UProbability probability2)
        {
            return !(probability1 == probability2);
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as UProbability);
        }

        public bool Equals(UProbability probability)
        {
            if (probability is null)
                return false;

            if (Object.ReferenceEquals(this, probability))
                return true;

            if (this.GetType() != probability.GetType())
                return false;

            return (Math.Abs(Value - probability.Value) < 1E-4);
        }
    }
}
