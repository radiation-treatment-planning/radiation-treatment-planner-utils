﻿using System.ComponentModel;

namespace RadiationTreatmentPlanner.Utils.DVH
{
    public enum DVHVersion101
    {
        [Description("Version DVH exporter")] VERSION = 0,
        [Description("Patient PIZ")] PIZ = 1,
        [Description("Plan")] PLAN = 2,
        [Description("Structure")] STRUCTURE = 3,
        [Description("TotalVolume")] TOTAL_VOLUME = 4,
        [Description("Coverage")] COVERAGE = 5,
        [Description("Sampling Coverage")] SAMPLING_COVERAGE = 6,
        [Description("Max dose")] MAX_DOSE = 7,
        [Description("Min dose")] MIN_DOSE = 8,
        [Description("Median dose")] MEDIAN_DOSE = 9,

        [Description("Standard deviation dose")]
        STANDARD_DEVIATION = 10,
        [Description("Prescription dose")] PRESCRIPTION_DOSE = 11,
        [Description("Number of fractions")] FRACTION_NUMBER = 12,
        [Description("Dose [")] DVHCURVE = 13
    }
}
