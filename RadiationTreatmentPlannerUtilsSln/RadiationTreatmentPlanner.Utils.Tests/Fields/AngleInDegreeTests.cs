﻿using System;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Fields;

namespace RadiationTreatmentPlanner.Utils.Tests.Fields
{
    [TestFixture]
    public class AngleInDegreeTests
    {
        [TestCase(0)]
        [TestCase(1.3)]
        [TestCase(360)]
        public void Constructor_Tests(double value)
        {
            var result = new AngleInDegree(value);
            Assert.AreEqual(value, result.Value);
        }

        [TestCase(-0.1)]
        [TestCase(360.01)]
        public void Constructor_ThrowArgumentOutOfRangeException_IfArgumentIsLowerZeroOrLarger360_Tests(double value)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new AngleInDegree(value));
        }

        [TestCase(0)]
        [TestCase(1.3)]
        [TestCase(360)]
        public void Equals_Tests(double value)
        {
            var result = new AngleInDegree(value);
            var result2 = new AngleInDegree(value);
            Assert.AreEqual(result, result2);
        }

        [Test]
        public void Equals_FloatingPointAccuracyOf_0_001_Tests()
        {
            var d1 = new AngleInDegree(3.14159);
            var d2 = new AngleInDegree(3.1414);
            var d3 = new AngleInDegree(3.1434);

            Assert.AreEqual(d1, d2);
            Assert.AreNotEqual(d1, d3);
        }
    }
}
