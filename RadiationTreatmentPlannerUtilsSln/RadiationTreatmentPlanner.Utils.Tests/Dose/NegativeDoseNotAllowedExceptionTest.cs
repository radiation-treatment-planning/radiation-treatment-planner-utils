﻿using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;

namespace RadiationTreatmentPlanner.Utils.Tests.Dose
{
    [TestFixture]
    public class NegativeDoseNotAllowedExceptionTest
    {
        [Test]
        public void NegativeDoseNotAllowedException_OneInputArgument_Test()
        {
            var number = -1.0;
            Assert.Throws<NegativeDoseException>(() => ThrowNegativeDoseNotAllowedException(number));
        }

        private void ThrowNegativeDoseNotAllowedException(double number)
        {
            if (number < 0)
                throw new NegativeDoseException(number);
        }
    }
}
