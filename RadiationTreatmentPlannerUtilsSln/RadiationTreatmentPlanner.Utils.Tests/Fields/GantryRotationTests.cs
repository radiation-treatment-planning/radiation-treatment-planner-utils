﻿using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Fields;

namespace RadiationTreatmentPlanner.Utils.Tests.Fields
{
    [TestFixture]
    public class GantryRotationTests
    {
        [TestCase(10, 50, RotationDirection.Clockwise)]
        [TestCase(10, 50, RotationDirection.AntiClockwise)]
        [TestCase(0, 50, RotationDirection.AntiClockwise)]
        [TestCase(0, 360, RotationDirection.Clockwise)]
        public void Constructor_Tests(double startValue, double finishValue, RotationDirection rotationDirection)
        {
            var start = new AngleInDegree(startValue);
            var finish = new AngleInDegree(finishValue);
            var result = new GantryRotation(start, finish, rotationDirection);

            Assert.AreEqual(start, result.StartAngle);
            Assert.AreEqual(finish, result.FinishAngle);
            Assert.AreEqual(rotationDirection, result.RotationDirection);
        }

        [Test]
        public void Equals_Tests()
        {
            var g1 = new GantryRotation(new AngleInDegree(10), new AngleInDegree(100), RotationDirection.Clockwise);
            var g2 = new GantryRotation(new AngleInDegree(10), new AngleInDegree(100), RotationDirection.AntiClockwise);
            var g3 = new GantryRotation(new AngleInDegree(10), new AngleInDegree(101), RotationDirection.Clockwise);
            var g4 = new GantryRotation(new AngleInDegree(5), new AngleInDegree(100), RotationDirection.Clockwise);
            var g5 = new GantryRotation(new AngleInDegree(10), new AngleInDegree(100), RotationDirection.Clockwise);

            Assert.AreNotEqual(g1, g2);
            Assert.AreNotEqual(g1, g3);
            Assert.AreNotEqual(g1, g4);
            Assert.AreEqual(g1, g5);
        }
    }
}
