﻿using System;
using System.Collections.Generic;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Fields;
using RadiationTreatmentPlanner.Utils.OptimizationObjetives;

namespace RadiationTreatmentPlanner.Utils.Patient
{
    public interface IUPlanSetup
    {
        string Id { get; }
        Option<uint> NumberOfFractions { get; }
        UDose TotalPrescriptionDose { get; }
        IEnumerable<IObjective> OptimizationObjectives { get; }
        IEnumerable<VMATArc> VmatArcs { get; }


        void SetTotalDose(UDose totalPrescriptionDose);
        void SetNumberOfFractions(Option<uint> numberOfFractions);
        OptimizationObjectiveAddResult AddOptimizationObjective(IObjective objective);
        OptimizationObjectiveAddResult AddOptimizationObjectiveRange(IEnumerable<IObjective> objectives);
        void AddVMATArc(VMATArc arc);
        void AddVMATArcRange(IEnumerable<VMATArc> arcs);
    }

    public class OptimizationObjectiveAddResult
    {
        public bool Success { get; }
        public string Info { get; }

        public OptimizationObjectiveAddResult(bool success, string info)
        {
            Success = success;
            Info = info ?? throw new ArgumentNullException(nameof(info));
        }
    }
}
