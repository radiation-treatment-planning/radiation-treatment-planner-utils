﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Numerics.Interpolation;
using MathNet.Numerics.Statistics;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.DVH
{
    public class DVHCurve
    {
        public enum Type
        {
            UNDEFINED = 0,
            CUMULATIVE = 1,
            DIFFERENTIAL = 2
        }

        public enum DoseType
        {
            UNDEFINED = 0,
            PHYSICAL = 1,
            EQD2 = 2,
            EQD0 = 3
        }

        public DVHCurve(List<Tuple<UDose, UVolume>> curvePoints, Type dvhType, DoseType typeOfDose, UVolume totalAbsoluteVolume = null)
        {
            _curvePoints = curvePoints ?? throw new ArgumentNullException(nameof(curvePoints));
            DVHType = dvhType;
            TypeOfDose = typeOfDose;
            _totalVolume = totalAbsoluteVolume;
        }

        public IEnumerable<Tuple<UDose, UVolume>> CurvePoints => _curvePoints;
        protected List<Tuple<UDose, UVolume>> _curvePoints;
        public Type DVHType { get; }
        public DoseType TypeOfDose { get; }

        public int Count()
        {
            return _curvePoints.Count;
        }

        public IEnumerable<Tuple<UDose, UVolume>> GetPoints()
        {
            return _curvePoints;
        }

        public UVolume _totalVolume;

        public UVolume TotalVolume()
        {
            if (_totalVolume != null) return _totalVolume;
            if (!_curvePoints.Any()) return _totalVolume = new UVolume(double.NaN, UVolume.VolumeUnit.Undefined);

            return (_totalVolume = DVHType == Type.CUMULATIVE
                ? new UVolume(_curvePoints.First().Item2.Value, GetVolumeUnit())
                : new UVolume(_curvePoints.Sum(v => v.Item2.Value), GetVolumeUnit()));
        }

        public UVolume.VolumeUnit GetVolumeUnit()
        {
            return _curvePoints.Any() ? _curvePoints.First().Item2.Unit : UVolume.VolumeUnit.Undefined;
        }

        public UDose.UDoseUnit GetDoseUnit()
        {
            return _curvePoints.Any() ? _curvePoints.First().Item1.Unit : UDose.UDoseUnit.Unknown;
        }

        public UDose GetMaximumDose()
        {
            var differentialDvh = this.ToDifferential();
            var maxDoseValue = differentialDvh._curvePoints
                .Where(x => x.Item2.Value > 0.0)
                .Max(y => y.Item1.Value);
            return new UDose(maxDoseValue, GetDoseUnit());
        }

        public UDose GetMinimumDose()
        {
            var differentialDvh = this.ToDifferential();
            var minDoseValue = differentialDvh._curvePoints
                .Where(x => x.Item2.Value > 0.0)
                .Min(y => y.Item1.Value);
            return new UDose(minDoseValue, GetDoseUnit());
        }

        public UDose GetMeanDose()
        {
            var dDvh = this.ToDifferential();
            var sum = dDvh._curvePoints.Sum(x => x.Item1.Value * x.Item2.Value);
            var sumOfVolumes = dDvh._curvePoints.Sum(x => x.Item2.Value);
            var mean = sum / sumOfVolumes;
            return new UDose(mean, GetDoseUnit());
        }

        public UDose GetMedianDose()
        {
            var percentDvh = this.ToVolumeInPercent();
            return percentDvh.GetDoseAtVolume(new UVolume(50, UVolume.VolumeUnit.percent));
        }

        public UDose GetStandardDeviationDose()
        {
            var dDvh = this.DVHType != Type.DIFFERENTIAL ? this.ToDifferential() : this;
            return new UDose(dDvh._curvePoints.Select(x => x.Item1.Value).StandardDeviation(), GetDoseUnit());
        }

        public UVolume GetMaximumVolume() => new UVolume(_curvePoints.Max(x => x.Item2.Value), GetVolumeUnit());
        public UVolume GetMinimumVolume() => new UVolume(_curvePoints.Min(x => x.Item2.Value), GetVolumeUnit());

        public UDose GetDoseAtVolume(UVolume volume)
        {
            // Check similarity of volume units.
            if (volume.Unit != GetVolumeUnit())
                throw new ArgumentException(
                    $"Argument and DVHCurve volume units must match, but were {volume.Unit} and {GetVolumeUnit()}");

            // Check DvhType.
            if (DVHType != Type.CUMULATIVE)
                throw new NotImplementedException($"Only implemented for cumulative DVHs, but is {DVHType}");

            var maximumVolume = GetMaximumVolume();

            // Lower or equal minimum volume case.
            if (volume.Value <= GetMinimumVolume().Value)
                return new UDose(_curvePoints.Max(x => x.Item1.Value), GetDoseUnit());

            // Larger than maximum volume case.
            if (volume.Value == maximumVolume.Value) return GetMinimumDose();

            if (volume.Value > maximumVolume.Value) return new UDose(double.NaN, UDose.UDoseUnit.Unknown);

            // Calculate closest point and use it, if it is close enough.
            var volumeWithMinimalDifference = _curvePoints.Min(x => Math.Abs(x.Item2.Value - volume.Value));
            var closestPoint =
                _curvePoints.First(x => Math.Abs(x.Item2.Value - volume.Value) == volumeWithMinimalDifference);
            if (volumeWithMinimalDifference < 0.001) return closestPoint.Item1;

            // Interpolate.
            var interpolation =
                LinearSpline.Interpolate(_curvePoints.Select(x => x.Item2.Value).ToArray(),
                    _curvePoints.Select(x => x.Item1.Value).ToArray());
            var interpolatedDoseValue = interpolation.Interpolate(volume.Value);
            return new UDose(interpolatedDoseValue, GetDoseUnit());
        }

        public UVolume GetVolumeAtDose(UDose dose)
        {
            // Check similarity of volume units.
            if (dose.Unit != GetDoseUnit())
                throw new ArgumentException(
                    $"Argument and DVHCurve dose units must match, but were {dose.Unit} and {GetVolumeUnit()}");

            // Check DvhType.
            if (DVHType != Type.CUMULATIVE)
                throw new NotImplementedException($"Only implemented for cumulative DVHs, but is {DVHType}");

            // Larger or equal max dose.
            var maxDose = GetMaximumDose();
            if (dose.Value > maxDose.Value) return new UVolume(0, GetVolumeUnit());

            // Dose is lower than minimum dose case.
            if (dose.Value < GetMinimumDose().Value) return GetMaximumVolume();

            // If hits exact point.
            var possibleExactPoint = _curvePoints.FirstOrDefault(x => x.Item1 == dose);
            if (possibleExactPoint != null) return possibleExactPoint.Item2;

            // Interpolate.
            var interpolation =
                LinearSpline.InterpolateSorted(_curvePoints.Select(x => x.Item1.Value).ToArray(),
                    _curvePoints.Select(x => x.Item2.Value).ToArray());
            var interpolatedVolumeValue = interpolation.Interpolate(dose.Value);
            return new UVolume(interpolatedVolumeValue, GetVolumeUnit());
        }

        public override bool Equals(object obj) => obj is DVHCurve curve && Equals(curve);

        private bool Equals(DVHCurve dvhCurve)
        {
            var points = dvhCurve.GetPoints().ToArray();
            for (var i = 0; i < points.Length; i++)
                if (!points[i].Equals(_curvePoints[i]))
                    return false;

            if (dvhCurve.TypeOfDose != TypeOfDose) return false;
            return dvhCurve.DVHType == DVHType;
        }
    }
}
