﻿namespace RadiationTreatmentPlanner.Utils.Fields
{
    public enum RotationDirection
    {
        Clockwise = 1,
        AntiClockwise = -1
    }
}