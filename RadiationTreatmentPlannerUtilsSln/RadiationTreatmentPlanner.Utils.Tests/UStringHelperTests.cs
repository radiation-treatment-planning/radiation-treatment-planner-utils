﻿using System;
using NUnit.Framework;

namespace RadiationTreatmentPlanner.Utils.Tests
{
    [TestFixture]
    public class UStringHelperTests
    {
        [TestCase("12.345", 12.345d)]
        [TestCase("0.0", 0.0d)]
        [TestCase("1", 1.0d)]
        [TestCase("-1.5", -1.5d)]
        public void ConvertStringToDouble_Test(string input, double expectedValue)
        {
            Assert.AreEqual(expectedValue, UStringHelper.ConvertStringToDoubleOrDefault(input));
        }

        [Test]
        public void Empty_String_Input_Returns_NAN_Test()
        {
            var value = UStringHelper.ConvertStringToDoubleOrDefault("");
            Assert.AreEqual(Double.NaN, value);
        }

        [Test]
        public void Null_String_Input_Returns_NAN_Test()
        {
            var value = UStringHelper.ConvertStringToDoubleOrDefault(null);
            Assert.AreEqual(Double.NaN, value);
        }
    }
}
