﻿using System;

namespace RadiationTreatmentPlanner.Utils.Volume
{
    [Serializable]
    public class NegativeVolumeException : Exception
    {
        public NegativeVolumeException()
        {
        }

        public NegativeVolumeException(double volumeValue) : base(
            $"Negative volume not allowed. Received: {volumeValue}")
        {
        }
    }
}
