﻿using System;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.DVH
{
    /// <summary>
    /// Dose Volume Histogram estimate, detached from its UPlanSetup and UStructure.
    /// </summary>
    public class UDvhEstimateDetached
    {
        public string StructureId { get; }
        public string PlanSetupId { get; }
        public double Coverage { get; }
        public UDose MinDose { get; }
        public UDose MeanDose { get; }
        public UDose MedianDose { get; }
        public UDose MaxDose { get; }
        public double SamplingCoverage { get; }
        public UDose StandardDeviationDose { get; }
        public UVolume Volume { get; }
        public DVHCurve DvhCurve { get; }

        public UDvhEstimateDetached(string planSetupId, string structureId, double coverage, UDose minDose, UDose meanDose, UDose medianDose, UDose maxDose,
            double samplingCoverage, UDose standardDeviationDose, UVolume volume, DVHCurve dvhCurve)
        {
            PlanSetupId = planSetupId;
            StructureId = structureId;
            Coverage = coverage;
            MinDose = minDose;
            MeanDose = meanDose;
            MedianDose = medianDose;
            MaxDose = maxDose;
            SamplingCoverage = samplingCoverage;
            StandardDeviationDose = standardDeviationDose;
            Volume = volume;
            DvhCurve = dvhCurve;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as UDvhEstimateDetached);
        }

        public bool Equals(UDvhEstimateDetached other)
        {
            return other != null
                   && Equals(PlanSetupId, other.PlanSetupId)
                   && Equals(StructureId, other.StructureId)
                   && Equals(Coverage, other.Coverage)
                   && Equals(MinDose, other.MinDose)
                   && Equals(MeanDose, other.MeanDose)
                   && Equals(MedianDose, other.MedianDose)
                   && Equals(MaxDose, other.MaxDose)
                   && Equals(SamplingCoverage, other.SamplingCoverage)
                   && Equals(Volume, other.Volume)
                   && Equals(DvhCurve, other.DvhCurve);
        }

        public override int GetHashCode()
        {
            var hashCode = new HashCode();
            hashCode.Add(StructureId);
            hashCode.Add(PlanSetupId);
            hashCode.Add(Coverage);
            hashCode.Add(MinDose);
            hashCode.Add(MeanDose);
            hashCode.Add(MedianDose);
            hashCode.Add(MaxDose);
            hashCode.Add(SamplingCoverage);
            hashCode.Add(StandardDeviationDose);
            hashCode.Add(Volume);
            hashCode.Add(DvhCurve);
            return hashCode.ToHashCode();
        }
    }
}
