﻿using System;
using System.ComponentModel;

namespace RadiationTreatmentPlanner.Utils.Volume
{
    public class UVolume
    {
        public enum VolumeUnit
        {
            [Description("cm³")] ccm = 0,
            [Description("mm³")] cmm = 1,
            [Description("%")] percent = 2,
            [Description("Undefined")] Undefined
        }

        public UVolume(double value, VolumeUnit unit)
        {
            Value = value;
            Unit = unit;

            CheckInputs();
        }

        public double Value { get; }
        public VolumeUnit Unit { get; }

        private void CheckInputs()
        {
            if (Value < 0) throw new NegativeVolumeException(Value);
        }

        public static implicit operator UVolume(string input) => input.ToUVolume();

        public override string ToString() =>
            $"{Value.ToString(UStringHelper.GetDoubleAndStringFormatProvider())}{Unit.ToDescription()}";

        public string ToString(uint numberOfFloatingPointDigits)
        {
            var volumeValue = Math.Round(Value, (int)numberOfFloatingPointDigits);
            return $"{volumeValue.ToString(UStringHelper.GetDoubleAndStringFormatProvider())}{Unit.ToDescription()}";
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as UVolume);
        }

        private bool Equals(UVolume other)
        {
            return other != null &&
                   Math.Abs(Value - other.Value) < 0.0001 &&
                   Unit == other.Unit;
        }
    }
}
