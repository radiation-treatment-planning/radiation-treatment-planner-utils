﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.Tests.DVH
{
    [TestFixture]
    public class DVHCurveTests
    {
        private List<Tuple<UDose, UVolume>> _doseVolumeTuples;
        private List<Tuple<UDose, UVolume>> _doseVolumeTuples2;

        [SetUp]
        public void SetUp()
        {
            _doseVolumeTuples = new List<Tuple<UDose, UVolume>>();
            _doseVolumeTuples.Add(Tuple.Create(new UDose(0.0, UDose.UDoseUnit.Gy), new UVolume(2.3, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples.Add(Tuple.Create(new UDose(1.0, UDose.UDoseUnit.Gy), new UVolume(2.0, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples.Add(Tuple.Create(new UDose(2.0, UDose.UDoseUnit.Gy), new UVolume(1.0, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples.Add(Tuple.Create(new UDose(3.0, UDose.UDoseUnit.Gy), new UVolume(0.0, UVolume.VolumeUnit.ccm)));


            _doseVolumeTuples2 = new List<Tuple<UDose, UVolume>>();
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(1, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(3, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(5, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(7, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(9, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(11, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(13, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(15, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(17, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(19, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(21, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(23, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(25, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(27, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(29, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(31, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(33, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(35, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(37, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(39, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(41, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(43, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(45, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(47, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(49, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(51, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(53, UDose.UDoseUnit.Gy), new UVolume(142, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(55, UDose.UDoseUnit.Gy), new UVolume(141, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(57, UDose.UDoseUnit.Gy), new UVolume(139, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(59, UDose.UDoseUnit.Gy), new UVolume(133, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(61, UDose.UDoseUnit.Gy), new UVolume(123, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(63, UDose.UDoseUnit.Gy), new UVolume(100, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(65, UDose.UDoseUnit.Gy), new UVolume(47, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(67, UDose.UDoseUnit.Gy), new UVolume(12, UVolume.VolumeUnit.ccm)));
            _doseVolumeTuples2.Add(Tuple.Create(new UDose(70, UDose.UDoseUnit.Gy), new UVolume(0.0, UVolume.VolumeUnit.ccm)));
        }

        [Test]
        public void Count_Test()
        {
            var dvhCurve = new DVHCurve(_doseVolumeTuples, DVHCurve.Type.UNDEFINED, DVHCurve.DoseType.PHYSICAL);
            Assert.AreEqual(4, dvhCurve.Count());
        }

        [Test]
        public void GetPoints()
        {
            var dvhCurve = new DVHCurve(_doseVolumeTuples, DVHCurve.Type.UNDEFINED, DVHCurve.DoseType.PHYSICAL);
            var points = dvhCurve.GetPoints().ToArray();

            for (var i = 0; i < points.Length; i++)
            {
                var point = points[i];
                var dose = point.Item1;
                var volume = point.Item2;

                Assert.AreEqual(_doseVolumeTuples[i].Item1.Value, dose.Value);
                Assert.AreEqual(_doseVolumeTuples[i].Item1.Unit, dose.Unit);
                Assert.AreEqual(_doseVolumeTuples[i].Item2.Value, volume.Value);
                Assert.AreEqual(_doseVolumeTuples[i].Item2.Unit, volume.Unit);
            }
        }

        [Test]
        public void TotalVolume_CumulativeInput_Test()
        {
            var dvhCurve = new DVHCurve(_doseVolumeTuples, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
            Assert.AreEqual(2.3, dvhCurve.TotalVolume().Value);
            Assert.AreEqual(UVolume.VolumeUnit.ccm, dvhCurve.TotalVolume().Unit);
        }

        [Test]
        public void TotalVolume_DifferentialInput_Test()
        {
            var cDvhCurve = new DVHCurve(_doseVolumeTuples, DVHCurve.Type.UNDEFINED, DVHCurve.DoseType.PHYSICAL);
            var dDvhCurve = cDvhCurve.ToDifferential();

            Assert.AreEqual(2.3, dDvhCurve.TotalVolume().Value);
            Assert.AreEqual(UVolume.VolumeUnit.ccm, dDvhCurve.TotalVolume().Unit);
            Assert.AreEqual(DVHCurve.Type.DIFFERENTIAL, dDvhCurve.DVHType);
        }

        [Test]
        public void GetMaximumDose_Test()
        {
            var curve = new DVHCurve(_doseVolumeTuples, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
            Assert.AreEqual(new UDose(2, UDose.UDoseUnit.Gy), curve.GetMaximumDose());
        }

        [Test]
        public void GetMinimumDose_Test()
        {
            var curve = new DVHCurve(_doseVolumeTuples, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
            Assert.AreEqual(new UDose(0, UDose.UDoseUnit.Gy), curve.GetMinimumDose());
        }

        [Test]
        public void GetMeanDose_Test()
        {
            var curve = new DVHCurve(_doseVolumeTuples2, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
            Assert.AreEqual(new UDose(62.7887323943662, UDose.UDoseUnit.Gy), curve.GetMeanDose());
        }

        [Test]
        public void GetMedianDose_Test()
        {
            var curve = new DVHCurve(_doseVolumeTuples2, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL,
                new UVolume(142, UVolume.VolumeUnit.ccm));
            Assert.AreEqual(new UDose(64.0943396226415, UDose.UDoseUnit.Gy), curve.GetMedianDose());
        }

        [Test]
        public void GetStandardDeviationDose_Test()
        {
            var curve = new DVHCurve(_doseVolumeTuples, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
            var result = curve.GetStandardDeviationDose();
            Assert.AreEqual(UDose.UDoseUnit.Gy, result.Unit);
            Assert.AreEqual(1.29, result.Value, 0.001);
        }

        [Test]
        public void GetMaximumVolume_Test()
        {
            var curve = new DVHCurve(_doseVolumeTuples, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
            Assert.AreEqual(new UVolume(2.3, UVolume.VolumeUnit.ccm), curve.GetMaximumVolume());
        }

        [Test]
        public void GetMinimumVolume_Test()
        {
            var curve = new DVHCurve(_doseVolumeTuples, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
            Assert.AreEqual(new UVolume(0, UVolume.VolumeUnit.ccm), curve.GetMinimumVolume());
        }

        [Test]
        public void GetDoseAtVolume_ThrowExceptionIfCurveIsNotCumulative_Test()
        {
            var curve = new DVHCurve(_doseVolumeTuples, DVHCurve.Type.DIFFERENTIAL, DVHCurve.DoseType.PHYSICAL);
            Assert.Throws<NotImplementedException>(() => curve.GetDoseAtVolume(new UVolume(1.5, UVolume.VolumeUnit.ccm)));
        }

        [Test]
        public void GetDoseAtVolume_ThrowExceptionIfVolumeUnitsAreNotEqual_Test()
        {
            var curve = new DVHCurve(_doseVolumeTuples, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
            Assert.Throws<ArgumentException>(() => curve.GetDoseAtVolume(new UVolume(1.5, UVolume.VolumeUnit.cmm)));
        }

        [TestCase(1.5, 1.5)]
        [TestCase(2.0, 1.0)]
        [TestCase(0.0, 3.0)]
        [TestCase(0.5, 2.5)]
        public void GetDoseAtVolume_Test(double volumeValue, double expectedResultDoseValue)
        {
            var curve = new DVHCurve(_doseVolumeTuples, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
            var result = curve.GetDoseAtVolume(new UVolume(volumeValue, UVolume.VolumeUnit.ccm));
            Assert.AreEqual(new UDose(expectedResultDoseValue, UDose.UDoseUnit.Gy), result);
        }

        [Test]
        public void GetDoseAtVolume_ReturnNANandUnknown_IfVolumeIsTooHigh_Test()
        {
            var curve = new DVHCurve(_doseVolumeTuples, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
            var result = curve.GetDoseAtVolume(new UVolume(3.0, UVolume.VolumeUnit.ccm));

            Assert.IsNaN(result.Value);
            Assert.AreEqual(UDose.UDoseUnit.Unknown, result.Unit);
        }

        [Test]
        public void GetDoseAtVolume_PercentAsVolumeUnit_Test()
        {
            var dvhCurve = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(100, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(5, UDose.UDoseUnit.Gy), new UVolume(50, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(15, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.percent)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            var result = dvhCurve.GetDoseAtVolume(new UVolume(75, UVolume.VolumeUnit.percent));
            var expectedResult = new UDose(2.5, UDose.UDoseUnit.Gy);
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void GetDoseAtVolume_ReturnInvalidDoseForTooHighVolume_Test()
        {
            var curve = new DVHCurve(_doseVolumeTuples, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
            var result = curve.GetDoseAtVolume(new UVolume(5.0, UVolume.VolumeUnit.ccm));
            var expectedResult = new UDose(double.NaN, UDose.UDoseUnit.Unknown);
            Assert.AreEqual(expectedResult.Value, result.Value);
            Assert.AreEqual(expectedResult.Unit, result.Unit);
        }

        [Test]
        public void GetVolumeAtDose_ThrowExceptionIfCurveIsNotCumulative_Test()
        {
            var curve = new DVHCurve(_doseVolumeTuples, DVHCurve.Type.DIFFERENTIAL, DVHCurve.DoseType.PHYSICAL);
            Assert.Throws<NotImplementedException>(() => curve.GetVolumeAtDose(new UDose(1.5, UDose.UDoseUnit.Gy)));
        }

        [Test]
        public void GetVolumeAtDose_ThrowExceptionIfDoseUnitsAreNotEqual_Test()
        {
            var curve = new DVHCurve(_doseVolumeTuples, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
            Assert.Throws<ArgumentException>(() => curve.GetVolumeAtDose(new UDose(1.5, UDose.UDoseUnit.cGy)));
        }

        [TestCase(0.0, 2.3)]
        [TestCase(7.0, 0)]
        [TestCase(1.5, 1.5)]
        [TestCase(2.0, 1.0)]
        [TestCase(0.5, 2.15)]
        public void GetVolumeAtDose_Test(double doseValue, double expectedVolumeValue)
        {
            var dose = new UDose(doseValue, UDose.UDoseUnit.Gy);
            var expectedVolume = new UVolume(expectedVolumeValue, UVolume.VolumeUnit.ccm);

            var curve = new DVHCurve(_doseVolumeTuples, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
            Assert.AreEqual(expectedVolume, curve.GetVolumeAtDose(dose));
        }

        [Test]
        public void Equals_Test()
        {
            var firstCurve = new DVHCurve(_doseVolumeTuples, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
            var secondCurve = new DVHCurve(_doseVolumeTuples, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            Assert.IsTrue(firstCurve.Equals(secondCurve));
        }

        [Test]
        public void TotalVolume_KeepAbsoluteTotalVolume_AlsoAfterConversionToVolumeInPercent_Test()
        {
            var dvhCurve = new DVHCurve(_doseVolumeTuples, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
            dvhCurve.ToVolumeInPercent();

            Assert.AreEqual(new UVolume(2.3, UVolume.VolumeUnit.ccm), dvhCurve.TotalVolume());
        }
    }
}
