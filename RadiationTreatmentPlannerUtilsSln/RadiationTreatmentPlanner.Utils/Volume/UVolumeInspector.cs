﻿namespace RadiationTreatmentPlanner.Utils.Volume
{
    public static class UVolumeInspector
    {
        public static bool IsAbsolute(this UVolume volume)
        {
            if (volume.Unit == UVolume.VolumeUnit.ccm) return true;
            if (volume.Unit == UVolume.VolumeUnit.cmm) return true;
            return false;
        }
        public static bool IsRelative(this UVolume volume)
        {
            if (volume.Unit == UVolume.VolumeUnit.percent) return true;
            return false;
        }
    }
}
