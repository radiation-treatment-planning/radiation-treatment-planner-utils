﻿using System.Collections.Generic;
using System.Linq;

namespace RadiationTreatmentPlanner.Utils.Point
{
    public class UPointArraySorter
    {
        public UPointComparer Comparer { get; }

        public UPointArraySorter(UPointComparer comparer)
        {
            Comparer = comparer;
        }

        public UPoint[] Sort(UPoint[] arrayToSort)
        {
            var list = arrayToSort.ToList();
            list.Sort(Comparer);
            return list.ToArray();
        }

        public List<UPoint> Sort(List<UPoint> listToSort)
        {
            listToSort.Sort(Comparer);
            return listToSort;
        }
    }
}
