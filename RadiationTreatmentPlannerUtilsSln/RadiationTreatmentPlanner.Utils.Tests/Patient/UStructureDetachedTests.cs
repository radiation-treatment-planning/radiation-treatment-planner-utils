﻿using System;
using System.Linq;
using System.Windows.Media.Media3D;
using NUnit.Framework;
using Optional;
using RadiationTreatmentPlanner.Utils.Patient;

namespace RadiationTreatmentPlanner.Utils.Tests.Patient
{
    [TestFixture]
    public class UStructureDetachedTests
    {
        [Test]
        public void Constructor_Test()
        {
            var structureSet = new UStructureSetDetached("ss");
            var structure = structureSet.AddStructureWithId("s1", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var resultStructure = structureSet.Structures.FirstOrDefault(x => x.Id == "s1");
            Assert.NotNull(structure);
            Assert.NotNull(resultStructure);
            Assert.AreEqual("s1", resultStructure.Id);
            Assert.AreEqual(Option.None<(double, double, double)>(), resultStructure.CenterPoint);
            Assert.AreEqual(Option.None<string>(), resultStructure.DicomType);
            Assert.AreEqual(Option.None<double>(), resultStructure.Volume);
            Assert.AreEqual(Option.None<bool>(), resultStructure.HasSegment);
            Assert.AreEqual(Option.None<MeshGeometry3D>(), resultStructure.MeshGeometry3D);
        }

        [Test]
        public void AddStructureWithId_ThrowArgumentExceptionIfVolumeIsLowerZero_Test()
        {
            var structureSetDetached = new UStructureSetDetached("ss");
            Assert.Throws<ArgumentException>(() => structureSetDetached.AddStructureWithId("s1", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.Some<double>(-1), Option.None<bool>(), Option.None<MeshGeometry3D>()));
        }

        [Test]
        public void Equals_Test()
        {
            var structure1 = new UStructureDetached("S", Option.Some((1.0, 2.0, 3.0)), Option.Some("whatever"),
                Option.Some(10.4), Option.Some(false), Option.None<MeshGeometry3D>());
            var structure2 = new UStructureDetached("S", Option.Some((1.0, 2.0, 3.0)), Option.Some("whatever"),
                Option.Some(10.4), Option.Some(false), Option.None<MeshGeometry3D>());

            Assert.AreEqual(structure1, structure2);
        }
    }
}
