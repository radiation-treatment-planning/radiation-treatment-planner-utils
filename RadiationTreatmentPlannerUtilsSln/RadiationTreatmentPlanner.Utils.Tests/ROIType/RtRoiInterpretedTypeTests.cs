﻿using System;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.ROIType;

namespace RadiationTreatmentPlanner.Utils.Tests.ROIType
{
    [TestFixture]
    public class RtRoiInterpretedTypeTests
    {
        [Test]
        public void Constructor_ThrowArgumentNullExceptionFor_Null_AsCodeParameter_Test()
        {
            Assert.Throws<ArgumentNullException>(() =>
                new RtRoiInterpretedType(null, "Placeholder description"));
        }

        [Test]
        public void Constructor_ThrowArgumentNullExceptionFor_Null_AsDescriptionParameter_Test()
        {
            Assert.Throws<ArgumentNullException>(() =>
                new RtRoiInterpretedType("PLACEHOLDER", null));
        }

        [Test]
        public void Constructor_Test()
        {
            var result = new RtRoiInterpretedType("CODE", "Description");

            Assert.AreEqual("CODE", result.Code);
            Assert.AreEqual("Description", result.Description);
        }

        [TestCase("PTV", "Planning risk volume")]
        [TestCase("PTV", "")]
        [TestCase("", "Planning risk volume")]
        [TestCase("123", "456")]
        public void Equals_ReturnTrueIfEquals_Test(string code, string description)
        {
            var type1 = new RtRoiInterpretedType(code, description);
            var type2 = new RtRoiInterpretedType(code, description);
            Assert.AreEqual(type1, type2);
        }

        [Test]
        public void Equals_ReturnFalseIfNotEquals_Test()
        {
            var type1 = new RtRoiInterpretedType("PTV", "PTV description");

            Assert.AreNotEqual(type1, new RtRoiInterpretedType("PTV", "Other description"));
            Assert.AreNotEqual(type1, new RtRoiInterpretedType("Other code", "PTV description"));
            Assert.AreNotEqual(type1, new RtRoiInterpretedType("Other code", "Other description"));
        }
    }
}
