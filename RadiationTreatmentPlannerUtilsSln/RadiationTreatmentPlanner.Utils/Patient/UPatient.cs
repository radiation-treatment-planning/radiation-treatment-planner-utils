﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RadiationTreatmentPlanner.Utils.Patient
{
    public class UPatient
    {
        public string Id { get; internal set; }
        public string Name { get; internal set; }
        private readonly List<UCourse> _courses;
        private readonly List<UStructureSet> _structureSets;
        public IEnumerable<UCourse> Courses => _courses.AsEnumerable();
        public IEnumerable<UStructureSet> StructureSets => _structureSets.AsEnumerable();

        public UPatient(string id, string name)
        {
            Id = id;
            Name = name; 
            _courses = new List<UCourse>();
            _structureSets = new List<UStructureSet>();
        }

        public UCourse AddCourseWithId(string courseId)
        {
            if (_courses.Any(x => x.Id == courseId))
                throw new ArgumentException(
                    $"Course with id {courseId} already exists and hence cannot be added.");
            var course = new UCourse(courseId, this);
            _courses.Add(course);
            return course;
        }

        public UCourse AddCourse(UCourseDetached course)
        {
            if (_courses.Any(x => x.Id == course.Id))
                throw new ArgumentException(
                    $"Course with id {course.Id} already exists and hence cannot be added.");
            var newCourse = new UCourse(course.Id, this);
            _courses.Add(newCourse);
            foreach (var planSetupDetached in course.PlanSetups)
            {
                if (StructureSets.All(x => x.Id != planSetupDetached.StructureSet.Id)) AddStructureSet(planSetupDetached.StructureSet);
                newCourse?.AddPlanSetup(planSetupDetached);
            }

            return newCourse;
        }

        public UStructureSet AddStructureSet(UStructureSetDetached structureSetDetached)
        {
            if (_structureSets.Any(x => x.Id == structureSetDetached.Id))
                throw new ArgumentException(
                    $"StructureId set with id {structureSetDetached.Id} already exists and hence cannot be added.");
            var structureSet = new UStructureSet(structureSetDetached.Id, this);
            foreach (var structure in structureSetDetached.Structures) structureSet.AddStructure(structure);
            _structureSets.Add(structureSet);
            return structureSet;
        }

        public UStructureSet AddStructureSetWithId(string structureSetId)
        {
            if (_structureSets.Any(x => x.Id == structureSetId))
                throw new ArgumentException(
                    $"StructureId set with id {structureSetId} already exists and hence cannot be added.");
            var structureSet = new UStructureSet(structureSetId, this);
            _structureSets.Add(structureSet);
            return structureSet;
        }
    }
}
