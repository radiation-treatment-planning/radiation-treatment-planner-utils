﻿namespace RadiationTreatmentPlanner.Utils.Dose
{
    public class DoseUnitExtractionResult
    {
        public bool Success { get; }
        public UDose.UDoseUnit DoseUnit { get; }

        public DoseUnitExtractionResult(bool success, UDose.UDoseUnit doseUnit)
        {
            Success = success;
            DoseUnit = doseUnit;
        }
    }
}
