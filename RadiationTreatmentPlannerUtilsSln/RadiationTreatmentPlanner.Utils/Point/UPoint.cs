﻿using System;

namespace RadiationTreatmentPlanner.Utils.Point
{
    public struct UPoint
    {
        public bool Equals(UPoint other)
        {
            return X.Equals(other.X) && Y.Equals(other.Y);
        }

        public override bool Equals(object obj)
        {
            return obj is UPoint other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X.GetHashCode() * 397) ^ Y.GetHashCode();
            }
        }

        public double X { get; }
        public double Y { get; }

        public UPoint(double x, double y)
        {
            X = x;
            Y = y;
        }

        public static bool operator ==(UPoint a, UPoint b)
        {
            return Math.Abs(a.X - b.X) < 1E-5 && Math.Abs(a.Y - b.Y) < 1E-5;
        }

        public static bool operator !=(UPoint a, UPoint b)
        {
            return !(a == b);
        }

        public static UPoint operator *(UPoint a, double b)
        {
            return new UPoint(a.X * b, a.Y * b);
        }

        public static double operator *(UPoint a, UPoint b)
        {
            return a.X * b.Y - a.Y * b.X;
        }

        public static UPoint operator +(UPoint a, UPoint b)
        {
            return new UPoint(a.X + b.X, a.Y + b.Y);
        }

        public static UPoint operator -(UPoint a, UPoint b)
        {
            return new UPoint(a.X - b.X, a.Y - b.Y);
        }

        public static bool IsDominating(UPoint a, UPoint b)
        {
            return (a.X <= b.X && a.Y <= b.Y) && (a.X < b.X || a.Y < b.Y);
        }
    }
}
