﻿using System;
using System.Windows.Media.Media3D;
using Optional;

namespace RadiationTreatmentPlanner.Utils.Patient
{
    public class UStructure : IUStructure
    {
        public string Id { get; }
        public UStructureSet StructureSet { get; }
        public Option<(double, double, double)> CenterPoint { get; }
        public Option<string> DicomType { get; }
        public Option<double> Volume { get; }
        public Option<bool> HasSegment { get; }
        public Option<MeshGeometry3D> MeshGeometry3D { get; }

        internal UStructure(string id, UStructureSet structureSet, Option<(double, double, double)> centerPoint, Option<string> dicomType, Option<double> volume, Option<bool> hasSegment, Option<MeshGeometry3D> meshGeometry3D)
        {

            if (volume.ValueOr(0) < 0)
                throw new ArgumentException($"Volume cannot be lower than zero, but was {volume.ValueOr(0)}");
            Id = id;
            StructureSet = structureSet;
            CenterPoint = centerPoint;
            DicomType = dicomType;
            Volume = volume;
            HasSegment = hasSegment;
            MeshGeometry3D = meshGeometry3D;
        }

        /// <summary>
        /// Creates a detached version of the structure.
        /// </summary>
        /// <returns>Detached copy of the structure.</returns>
        public UStructureDetached Detach() => new UStructureDetached(Id, CenterPoint, DicomType, Volume, HasSegment, MeshGeometry3D);
    }
}
