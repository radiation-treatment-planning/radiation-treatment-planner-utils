﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Media3D;
using Optional;

namespace RadiationTreatmentPlanner.Utils.Patient
{
    public class UStructureSet : IUStructureSet
    {
        public string Id { get; }
        public UPatient Patient { get; }
        private readonly List<UStructure> _structures;
        public IEnumerable<UStructure> Structures => _structures.AsEnumerable();

        internal UStructureSet(string id, UPatient patient)
        {
            Id = id;
            Patient = patient;
            _structures = new List<UStructure>();
        }

        public UStructure AddStructure(UStructureDetached structureDetached)
        {
            ThrowErrorIfStructureWithIdAlreadyExists(structureDetached.Id);
            var structure = new UStructure(structureDetached.Id, this, structureDetached.CenterPoint,
                structureDetached.DicomType, structureDetached.Volume, structureDetached.HasSegment,
                structureDetached.MeshGeometry3D);
            _structures.Add(structure);
            return structure;
        }

        public UStructure AddStructureWithId(string id, Option<(double, double, double)> centerPoint,
            Option<string> dicomType, Option<double> volume, Option<bool> hasSegment,
            Option<MeshGeometry3D> meshGeometry3D)
        {
            ThrowErrorIfStructureWithIdAlreadyExists(id);
            var structure = new UStructure(id, this, centerPoint, dicomType, volume, hasSegment, meshGeometry3D);
            _structures.Add(structure);
            return structure;
        }
        private void ThrowErrorIfStructureWithIdAlreadyExists(string id)
        {
            if (_structures.Any(x => x.Id == id))
                throw new ArgumentException(
                    $"A structure with ID {id} already exists and hence no new one with the same ID can be added.");
        }

        /// <summary>
        /// Creates a detached version of the structure set.
        /// </summary>
        /// <returns>Detached copy of structure set.</returns>
        public UStructureSetDetached Detach()
        {
            var detachedStructureSet = new UStructureSetDetached(Id);
            foreach (var structure in Structures) detachedStructureSet.AddStructure(structure.Detach());
            return detachedStructureSet;
        }
    }
}
