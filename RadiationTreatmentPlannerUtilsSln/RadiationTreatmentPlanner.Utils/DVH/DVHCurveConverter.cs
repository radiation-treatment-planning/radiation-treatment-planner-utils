﻿using System;
using System.Collections.Generic;
using System.Linq;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.DVH
{
    public static class DVHCurveConverter
    {
        /// <summary>
        ///     Converts cumulative to differential dose volume pair list. For the final difference a 0 is assumed.
        ///     In case that a differential list is fed into the method, the list is returned
        ///     without any changes.
        /// </summary>
        /// <param name="dvhCurve">
        ///     A list of UDose, UVolume tuples.
        ///     An ascending sorting of the dose values is assumed.
        /// </param>
        /// <returns>
        ///     A DVHCurve.
        /// </returns>
        public static DVHCurve ToDifferential(this DVHCurve dvhCurve)
        {
            if (!dvhCurve.GetPoints().Any())
                return new DVHCurve(new List<Tuple<UDose, UVolume>>(), DVHCurve.Type.DIFFERENTIAL, dvhCurve.TypeOfDose);

            if (dvhCurve.DVHType == DVHCurve.Type.DIFFERENTIAL)
                return dvhCurve;

            var newDvhArray = new Tuple<UDose, UVolume>[dvhCurve.Count()];

            var dvhPoints = dvhCurve.GetPoints().ToArray();

            for (var i = 0; i < dvhPoints.Length; i++)
            {
                var dose = dvhPoints.ElementAt(i).Item1;
                var volumeUnit = dvhPoints.ElementAt(i).Item2.Unit;
                var volumeValue = 0.0;
                if (i < dvhPoints.Length - 1)
                    volumeValue = dvhPoints.ElementAt(i).Item2.Value -
                                  dvhPoints.ElementAt(i + 1).Item2.Value;
                volumeValue = Math.Max(volumeValue, 0.0);
                var newDoseValuePair = Tuple.Create(dose, new UVolume(volumeValue, volumeUnit));
                newDvhArray[i] = newDoseValuePair;
            }

            return new DVHCurve(newDvhArray.ToList(), DVHCurve.Type.DIFFERENTIAL, dvhCurve.TypeOfDose);
        }

        public static DVHCurve ToVolumeInPercent(this DVHCurve dvhCurve)
        {
            var volumeUnit = dvhCurve.GetVolumeUnit();
            if (volumeUnit == UVolume.VolumeUnit.percent || volumeUnit == UVolume.VolumeUnit.Undefined) return dvhCurve;

            var totalVolume = dvhCurve.TotalVolume();
            if (!totalVolume.IsAbsolute()) return dvhCurve;

            var points = dvhCurve.GetPoints().ToArray();
            var convertedPoints = new Tuple<UDose, UVolume>[points.Length];

            for (var i = 0; i < points.Length; i++)
            {
                var point = points[i];
                var volume = point.Item2;
                var convertedVolume = new UVolume(volume.Value / totalVolume.Value * 100.0, UVolume.VolumeUnit.percent);
                var convertedPoint = Tuple.Create(point.Item1, convertedVolume);
                convertedPoints[i] = convertedPoint;
            }

            return new DVHCurve(convertedPoints.ToList(), dvhCurve.DVHType, dvhCurve.TypeOfDose, totalVolume);
        }

        /// <summary>
        /// Convert DVHCurve to equivalent dose in 0Gy fractions with the linear-quadratic model
        /// which describes the cell survival curve after exposure to radiation. Ignores
        /// <param name="numberOfFractions">numberOfFractions</param> if dose is in EQD0.
        /// Throws exception, if dose is not physical dose, EQD0 or already EQD2.
        /// </summary>
        /// <param name="dvhCurve">Dose-Volume histogram curve.</param>
        /// <param name="alphaOverBeta">Alpha/Beta ratio of the LQ model (> 0).</param>
        /// <param name="numberOfFractions">Number of fractions (> 0).</param>
        /// <returns>DVHCurve converted to EQD2.</returns>
        public static DVHCurve ToEqd2(this DVHCurve dvhCurve, double alphaOverBeta, uint numberOfFractions)
        {
            if (alphaOverBeta <= 0)
                throw new ArgumentOutOfRangeException(
                    $"{nameof(alphaOverBeta)} must be larger zero, but was {alphaOverBeta}.");
            if (numberOfFractions == 0)
                throw new ArgumentOutOfRangeException(
                    $"{nameof(numberOfFractions)} must be larger zero, but was {numberOfFractions}.");

            if (dvhCurve.TypeOfDose == DVHCurve.DoseType.EQD2) return dvhCurve;
            List<Tuple<UDose, UVolume>> convertedPoints;
            switch (dvhCurve.TypeOfDose)
            {
                case DVHCurve.DoseType.EQD0:
                    convertedPoints = dvhCurve.CurvePoints.Select(x =>
                        Tuple.Create(x.Item1.Eqd0ToEqd2(alphaOverBeta), x.Item2)).ToList();
                    break;
                case DVHCurve.DoseType.PHYSICAL:
                    convertedPoints = dvhCurve.CurvePoints.Select(x =>
                        Tuple.Create(x.Item1.PhysicalToEqd2(alphaOverBeta, numberOfFractions), x.Item2)).ToList();
                    break;
                default:
                    throw new NotImplementedException(
                        $"Conversion from {dvhCurve.TypeOfDose} to {DVHCurve.DoseType.EQD2} is not implemented.");
            }
            var totalVolume = dvhCurve.TotalVolume();
            return totalVolume.IsAbsolute()
                ? new DVHCurve(convertedPoints, dvhCurve.DVHType, DVHCurve.DoseType.EQD2, totalVolume)
                : new DVHCurve(convertedPoints, dvhCurve.DVHType, DVHCurve.DoseType.EQD2);
        }

        /// <summary>
        /// Convert DVHCurve from physical to equivalent dose in 0Gy fractions with
        /// the linear-quadratic model which describers the cell survival curve after
        /// exposure to radiation. Throws exception, if dose is not physical dose or already EQD0.
        /// </summary>
        /// <param name="dvhCurve">Dose-Volume histogram curve with physical dose.</param>
        /// <param name="alphaOverBeta">Alpha/Beta ratio of the LQ model (> 0).</param>
        /// <param name="numberOfFractions">Number of fractions (> 0).</param>
        /// <returns>DVHCurve converted to EQD0.</returns>
        public static DVHCurve ToEqd0(this DVHCurve dvhCurve, double alphaOverBeta, uint numberOfFractions)
        {
            if (alphaOverBeta <= 0)
                throw new ArgumentOutOfRangeException(
                    $"{nameof(alphaOverBeta)} must be larger zero, but was {alphaOverBeta}.");
            if (numberOfFractions == 0)
                throw new ArgumentOutOfRangeException(
                    $"{nameof(numberOfFractions)} must be larger zero, but was {numberOfFractions}.");

            if (dvhCurve.TypeOfDose == DVHCurve.DoseType.EQD0) return dvhCurve;
            List<Tuple<UDose, UVolume>> convertedPoints;
            switch (dvhCurve.TypeOfDose)
            {
                case DVHCurve.DoseType.EQD2:
                    convertedPoints = dvhCurve.CurvePoints.Select(x =>
                        Tuple.Create(x.Item1.Eqd2ToEqd0(alphaOverBeta), x.Item2)).ToList();
                    break;
                case DVHCurve.DoseType.PHYSICAL:
                    convertedPoints = dvhCurve.CurvePoints.Select(x =>
                        Tuple.Create(x.Item1.PhysicalToEqd0(alphaOverBeta, numberOfFractions), x.Item2)).ToList();
                    break;
                default:
                    throw new NotImplementedException(
                        $"Conversion from {dvhCurve.TypeOfDose} to {DVHCurve.DoseType.EQD0} is not implemented.");
            }
            var totalVolume = dvhCurve.TotalVolume();
            return totalVolume.IsAbsolute()
                ? new DVHCurve(convertedPoints, dvhCurve.DVHType, DVHCurve.DoseType.EQD0, totalVolume)
                : new DVHCurve(convertedPoints, dvhCurve.DVHType, DVHCurve.DoseType.EQD0);
        }
    }
}
