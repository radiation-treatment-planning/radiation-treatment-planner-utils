﻿namespace RadiationTreatmentPlanner.Utils.Patient
{
    public interface IUCourse
    {
        string Id { get; }
    }
}
