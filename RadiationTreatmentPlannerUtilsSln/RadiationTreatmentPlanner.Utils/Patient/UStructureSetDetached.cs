﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Media3D;
using Optional;

namespace RadiationTreatmentPlanner.Utils.Patient
{
    public class UStructureSetDetached : IUStructureSet
    {
        public string Id { get; }
        private readonly List<UStructureDetached> _structures;
        public IEnumerable<UStructureDetached> Structures => _structures.AsEnumerable();

        public UStructureSetDetached(string id)
        {
            Id = id;
            _structures = new List<UStructureDetached>();
        }

        public void AddStructure(UStructureDetached structure)
        {
            ThrowErrorIfStructureWithIdAlreadyExists(structure.Id);
            _structures.Add(structure);
        }

        public UStructureDetached AddStructureWithId(string id, Option<(double, double, double)> centerPoint,
            Option<string> dicomType, Option<double> volume, Option<bool> hasSegment,
            Option<MeshGeometry3D> meshGeometry3D)
        {
            ThrowErrorIfStructureWithIdAlreadyExists(id);
            if (volume.ValueOr(0) < 0)
                throw new ArgumentException($"Volume cannot be lower than zero, but was {volume.ValueOr(0)}");
            var structure = new UStructureDetached(id, centerPoint, dicomType, volume, hasSegment, meshGeometry3D);
            _structures.Add(structure);
            return structure;
        }

        private void ThrowErrorIfStructureWithIdAlreadyExists(string id)
        {
            if (_structures.Any(x => x.Id == id))
                throw new ArgumentException(
                    $"A structure with ID {id} already exists and hence no new one with the same ID can be added.");
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as UStructureSetDetached);
        }

        public bool Equals(UStructureSetDetached other)
        {
            if (other == null) return false;
            var otherStructures = other.Structures.ToList();
            if (_structures.Count != otherStructures.Count) return false;
            foreach (var structure in _structures)
                if (!otherStructures.Contains(structure))
                    return false;
            return Id == other.Id;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
