﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Media3D;
using NUnit.Framework;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Fields;
using RadiationTreatmentPlanner.Utils.OptimizationObjetives;
using RadiationTreatmentPlanner.Utils.Patient;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RadiationTreatmentPlanner.Utils.Tests.Patient
{
    [TestFixture]
    public class UCourseTests
    {
        [Test]
        public void AddPlanSetup_UPlanSetupDetached_As_Input_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            var structureGtvDetached = new UStructureDetached("GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            var structureOarDetached = new UStructureDetached("OAR", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>());
            structureSetDetached.AddStructure(structureGtvDetached);
            structureSetDetached.AddStructure(structureOarDetached);

            var course = patient.AddCourseWithId("C1");
            patient.AddStructureSet(structureSetDetached);
            var structureSet = patient.StructureSets.FirstOrDefault();

            var planSetupDetached1 = new UPlanSetupDetached("plan1", Option.Some<uint>(12), new UDose(60, UDose.UDoseUnit.Gy),
                structureSetDetached);
            var objectivesForPlanSetup1 = new IObjective[]
            {
                new PointObjective("GTV", ObjectiveOperator.LOWER, new Priority(500), new UDose(66, UDose.UDoseUnit.Gy),
                    new UVolume(98, UVolume.VolumeUnit.percent)),
                new EUDObjective("OAR", ObjectiveOperator.LOWER, new Priority(500), new UDose(40, UDose.UDoseUnit.Gy),
                    40),
                new MeanDoseObjective("GTV", new UDose(72, UDose.UDoseUnit.Gy), new Priority(500))
            };
            planSetupDetached1.AddOptimizationObjectiveRange(objectivesForPlanSetup1);

            var planSetupDetached2 = new UPlanSetupDetached("plan2", Option.Some<uint>(11), new UDose(62, UDose.UDoseUnit.Gy),
                structureSetDetached);

            var arcId = "ARC1";
            var gantryRotation = new GantryRotation(new AngleInDegree(10), new AngleInDegree(170), RotationDirection.Clockwise);
            var collimatorRotation = new AngleInDegree(5);
            var collimatorRotation2 = new AngleInDegree(355);
            var couchRotation = new AngleInDegree(0);
            var arc1 = new VMATArc(arcId, gantryRotation, collimatorRotation, couchRotation);
            var arc2 = new VMATArc(arcId, gantryRotation, collimatorRotation2, couchRotation);
            var arcs = new List<VMATArc> { arc1, arc2 };

            course.AddPlanSetup(planSetupDetached1);
            course.AddPlanSetup(planSetupDetached2);

            var resultCourse = patient.Courses.FirstOrDefault();
            var resultPlan1 = resultCourse?.PlanSetups.FirstOrDefault(x => x.Id == planSetupDetached1.Id);
            var resultPlan2 = resultCourse?.PlanSetups.FirstOrDefault(x => x.Id == planSetupDetached2.Id);

            Assert.AreEqual(2, resultCourse?.PlanSetups.Count());
            Assert.NotNull(resultCourse?.PlanSetups.FirstOrDefault(x => x.Id == planSetupDetached1.Id));
            Assert.NotNull(resultCourse?.PlanSetups.FirstOrDefault(x => x.Id == planSetupDetached2.Id));
            Assert.AreEqual(resultCourse, resultPlan1?.Course);
            Assert.AreEqual(resultCourse, resultPlan2?.Course);
            Assert.AreEqual(patient, resultPlan1?.Patient);
            Assert.AreEqual(patient, resultPlan2?.Patient);
            Assert.IsEmpty(resultPlan1.DvhEstimates);
            Assert.IsEmpty(resultPlan2.DvhEstimates);
            Assert.AreEqual(Option.Some<uint>(12), resultPlan1?.NumberOfFractions);
            Assert.AreEqual(Option.Some<uint>(11), resultPlan2?.NumberOfFractions);
            Assert.AreEqual(structureSet, resultPlan1?.StructureSet);
            Assert.AreEqual(structureSet, resultPlan2?.StructureSet);
            Assert.AreEqual(UDose.UDoseUnit.Gy, resultPlan1?.TotalPrescriptionDose.Unit);
            Assert.AreEqual(UDose.UDoseUnit.Gy, resultPlan2?.TotalPrescriptionDose.Unit);
            Assert.AreEqual(60, resultPlan1?.TotalPrescriptionDose.Value);
            Assert.AreEqual(62, resultPlan2?.TotalPrescriptionDose.Value);

            var objectivesResult = resultPlan1.OptimizationObjectives.ToList();
            Assert.AreEqual(3, objectivesResult.Count());
            Assert.Contains(objectivesForPlanSetup1[0], objectivesResult);
            Assert.Contains(objectivesForPlanSetup1[1], objectivesResult);
            Assert.Contains(objectivesForPlanSetup1[2], objectivesResult);
        }

        [Test]
        public void AddPlanSetupWithId_Test()
        {

            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            patient.AddCourseWithId("C1");
            patient.AddStructureSet(structureSetDetached);
            var structureSet = patient.StructureSets.FirstOrDefault();

            var resultCourse = patient.Courses.FirstOrDefault();
            resultCourse?.AddPlanSetupWithId("plan1", Option.Some<uint>(12), new UDose(60, UDose.UDoseUnit.Gy),
                structureSet);
            resultCourse?.AddPlanSetupWithId("plan2", Option.Some<uint>(11), new UDose(62, UDose.UDoseUnit.Gy),
                structureSet);


            var resultPlan1 = resultCourse?.PlanSetups.FirstOrDefault(x => x.Id == "plan1");
            var resultPlan2 = resultCourse?.PlanSetups.FirstOrDefault(x => x.Id == "plan2");

            Assert.AreEqual(2, resultCourse?.PlanSetups.Count());
            Assert.NotNull(resultCourse?.PlanSetups.FirstOrDefault(x => x.Id == "plan1"));
            Assert.NotNull(resultCourse?.PlanSetups.FirstOrDefault(x => x.Id == "plan2"));
            Assert.AreEqual(resultCourse, resultPlan1?.Course);
            Assert.AreEqual(resultCourse, resultPlan2?.Course);
            Assert.AreEqual(patient, resultPlan1?.Patient);
            Assert.AreEqual(patient, resultPlan2?.Patient);
            Assert.IsEmpty(resultPlan1.DvhEstimates);
            Assert.IsEmpty(resultPlan2.DvhEstimates);
            Assert.AreEqual(Option.Some<uint>(12), resultPlan1?.NumberOfFractions);
            Assert.AreEqual(Option.Some<uint>(11), resultPlan2?.NumberOfFractions);
            Assert.AreEqual(structureSet, resultPlan1?.StructureSet);
            Assert.AreEqual(structureSet, resultPlan2?.StructureSet);
            Assert.AreEqual(UDose.UDoseUnit.Gy, resultPlan1?.TotalPrescriptionDose.Unit);
            Assert.AreEqual(UDose.UDoseUnit.Gy, resultPlan2?.TotalPrescriptionDose.Unit);
            Assert.AreEqual(60, resultPlan1?.TotalPrescriptionDose.Value);
            Assert.AreEqual(62, resultPlan2?.TotalPrescriptionDose.Value);
        }

        [Test]
        public void AddPlanSetup_ThrowArgumentErrorIfStructureSetDoesNotAlreadyExist_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            var course = patient.AddCourseWithId("C1");
            var planSetup = new UPlanSetupDetached("plan", Option.Some<uint>(12), new UDose(60, UDose.UDoseUnit.Gy),
                structureSetDetached);
            Assert.Throws<ArgumentException>(() =>
                course.AddPlanSetup(planSetup));
        }

        [Test]
        public void AddPlanSetup_ThrowArgumentErrorIfPlanSetupAlreadyExists_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            var course = patient.AddCourseWithId("C1");
            var structureSet = patient.AddStructureSet(structureSetDetached);
            var planSetup = new UPlanSetupDetached("plan", Option.Some<uint>(12), new UDose(60, UDose.UDoseUnit.Gy),
                structureSetDetached);
            course.AddPlanSetup(planSetup);
            Assert.Throws<ArgumentException>(() =>
                course.AddPlanSetup(planSetup));
        }

        [Test]
        public void AddPlanSetupWithId_ThrowArgumentErrorIfPlanSetupAlreadyExists_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("ss");
            var course = patient.AddCourseWithId("C1");
            var structureSet = patient.AddStructureSet(structureSetDetached);
            course.AddPlanSetupWithId("plan", Option.Some<uint>(12), new UDose(60, UDose.UDoseUnit.Gy), structureSet);
            Assert.Throws<ArgumentException>(() =>
                course.AddPlanSetupWithId("plan", Option.Some<uint>(12), new UDose(60, UDose.UDoseUnit.Gy),
                    structureSet));
        }

        [Test]
        public void Detach_Test()
        {
            var patient = new UPatient("id", "name");
            var structureSet = patient.AddStructureSetWithId("ss");
            structureSet.AddStructure(new UStructureDetached("s1", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>()));
            structureSet.AddStructure(new UStructureDetached("s2", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.None<double>(), Option.None<bool>(), Option.None<MeshGeometry3D>()));
            var course = patient.AddCourseWithId("C1");
            var plan1 = course.AddPlanSetupWithId("plan1", Option.Some<uint>(12), new UDose(60, UDose.UDoseUnit.Gy), structureSet);
            var plan2 = course.AddPlanSetupWithId("plan2", Option.Some<uint>(11), new UDose(62, UDose.UDoseUnit.Gy), structureSet);
            var courseDetached = course.Detach();

            var planDetached1 = courseDetached.PlanSetups.FirstOrDefault(x => x.Id == plan1.Id);
            var planDetached2 = courseDetached.PlanSetups.FirstOrDefault(x => x.Id == plan2.Id);

            Assert.AreEqual(course.Id, courseDetached.Id);
            Assert.NotNull(planDetached1);
            Assert.AreEqual(plan1.StructureSet.Id, planDetached1.StructureSet.Id);
            Assert.NotNull(planDetached1.StructureSet.Structures.FirstOrDefault(x => x.Id == "s1"));
            Assert.NotNull(planDetached1.StructureSet.Structures.FirstOrDefault(x => x.Id == "s2"));

            Assert.AreEqual(course.Id, courseDetached.Id);
            Assert.NotNull(planDetached2);
            Assert.AreEqual(plan2.StructureSet.Id, planDetached2.StructureSet.Id);
            Assert.NotNull(planDetached2.StructureSet.Structures.FirstOrDefault(x => x.Id == "s1"));
            Assert.NotNull(planDetached1.StructureSet.Structures.FirstOrDefault(x => x.Id == "s2"));



        }
    }
}