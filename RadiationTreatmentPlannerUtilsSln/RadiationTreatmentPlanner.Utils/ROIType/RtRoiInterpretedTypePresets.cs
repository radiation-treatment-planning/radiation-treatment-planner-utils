﻿namespace RadiationTreatmentPlanner.Utils.ROIType
{
    public static class RtRoiInterpretedTypePresets
    {
        public static RtRoiInterpretedType EXTERNAL() =>
            new RtRoiInterpretedType("EXTERNAL", "external patient contour");

        public static RtRoiInterpretedType PTV() =>
            new RtRoiInterpretedType("PTV", "Planning Target Volume (as defined in [ICRU Report 50])");

        public static RtRoiInterpretedType CTV() =>
            new RtRoiInterpretedType("CTV", "Clinical Target Volume (as defined in [ICRU Report 50])");

        public static RtRoiInterpretedType GTV() =>
            new RtRoiInterpretedType("GTV", "Gross Tumor Volume (as defined in [ICRU Report 50])");

        public static RtRoiInterpretedType TREATED_VOLUME() =>
            new RtRoiInterpretedType("TREATED_VOLUME", "Treated Volume (as defined in [ICRU Report 50])");

        public static RtRoiInterpretedType IRRAD_VOLUME() =>
            new RtRoiInterpretedType("IRRAD_VOLUME", "Irradiated Volume (as defined in [ICRU Report 50])");

        public static RtRoiInterpretedType BOLUS() =>
            new RtRoiInterpretedType("BOLUS", "patient bolus to be used for external beam therapy");

        public static RtRoiInterpretedType AVOIDANCE() =>
            new RtRoiInterpretedType("AVOIDANCE", "region in which dose is to be minimized");

        public static RtRoiInterpretedType ORGAN() =>
            new RtRoiInterpretedType("ORGAN", "patient organ");

        public static RtRoiInterpretedType MARKER() =>
            new RtRoiInterpretedType("MARKER", "patient marker or marker on a localizer");

        public static RtRoiInterpretedType REGISTRATION() =>
            new RtRoiInterpretedType("REGISTRATION", "registration ROI");

        public static RtRoiInterpretedType ISOCENTER() =>
            new RtRoiInterpretedType("ISOCENTER", "treatment isocenter to be used for external beam therapy");

        public static RtRoiInterpretedType CONTRAST_AGENT() =>
            new RtRoiInterpretedType("CONTRAST_AGENT", "volume into which a contrast agent has been injected");

        public static RtRoiInterpretedType CAVITY() =>
            new RtRoiInterpretedType("CAVITY", "patient anatomical cavity");

        public static RtRoiInterpretedType BRACHY_CHANNEL() =>
            new RtRoiInterpretedType("BRACHY_CHANNEL", "brachytherapy channel");

        public static RtRoiInterpretedType BRACHY_ACCESSORY() =>
            new RtRoiInterpretedType("BRACHY_ACCESSORY", "brachytherapy accessory device");

        public static RtRoiInterpretedType BRACHY_SRC_APP() =>
            new RtRoiInterpretedType("BRACHY_SRC_APP", "brachytherapy source applicator");

        public static RtRoiInterpretedType BRACHY_CHNL_SHLD() =>
            new RtRoiInterpretedType("BRACHY_CHNL_SHLD", "brachytherapy channel shield");

        public static RtRoiInterpretedType SUPPORT() =>
            new RtRoiInterpretedType("SUPPORT", "external patient support device");

        public static RtRoiInterpretedType FIXATION() =>
            new RtRoiInterpretedType("FIXATION", "external patient fixation or immobilization device");

        public static RtRoiInterpretedType DOSE_REGION() =>
            new RtRoiInterpretedType("DOSE_REGION", "ROI to be used as a dose reference");

        public static RtRoiInterpretedType CONTROL() =>
            new RtRoiInterpretedType("CONTROL", "ROI to be used in control of dose optimization and calculation");

        public static RtRoiInterpretedType DOSE_MEASUREMENT() =>
            new RtRoiInterpretedType("DOSE_MEASUREMENT", "ROI representing a dose measurement device, such as a chamber or TLD");

        public static RtRoiInterpretedType STOMACH() =>
            new RtRoiInterpretedType("STOMACH", "Stomach");

        public static RtRoiInterpretedType PANCREAS() =>
            new RtRoiInterpretedType("PANCREAS", "Pancreas");

        public static RtRoiInterpretedType SMALL_BOWEL() =>
            new RtRoiInterpretedType("SMALL_BOWEL", "Small bowel (small intestine)");

        public static RtRoiInterpretedType SPINAL_CORD() =>
            new RtRoiInterpretedType("SPINAL_CORD", "Spinal cord");

        public static RtRoiInterpretedType LIVER() =>
            new RtRoiInterpretedType("LIVER", "Liver");

        public static RtRoiInterpretedType LEFT_KIDNEY() =>
            new RtRoiInterpretedType("LEFT_KIDNEY", "Left kidney");

        public static RtRoiInterpretedType RIGHT_KIDNEY() =>
            new RtRoiInterpretedType("RIGHT_KIDNEY", "Right kidney");

        public static RtRoiInterpretedType PRV() =>
            new RtRoiInterpretedType("PRV", "Planning risk volume. DOI:10.1007/s00066-016-1057-x");

        public static RtRoiInterpretedType PTV_DOM() =>
            new RtRoiInterpretedType("PTV_DOM", "Dominant Planning risk volume. DOI:10.1007/s00066-016-1057-x");

        public static RtRoiInterpretedType PTV_SIP_STOMACH() =>
            new RtRoiInterpretedType("PTV_SIP_STOMACH",
                "Simultaneous integrated protection of stomach. DOI:10.1007/s00066-016-1057-x");

        public static RtRoiInterpretedType PTV_SIP_SMALL_BOWEL() =>
            new RtRoiInterpretedType("PTV_SIP_SMALL_BOWEL",
                "Simultaneous integrated protection of small bowel. DOI:10.1007/s00066-016-1057-x");

        public static RtRoiInterpretedType ITV() =>
            new RtRoiInterpretedType("ITV", "Internal target volume");
    }
}
