﻿using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.ROIType;

namespace RadiationTreatmentPlanner.Utils.Tests.ROIType
{
    [TestFixture]
    public class RtpRoiInterpretedTypePresetsTests
    {
        [Test]
        public void EXTERNAL_Test()
        {
            var result = RtRoiInterpretedTypePresets.EXTERNAL();

            Assert.AreEqual("EXTERNAL", result.Code);
            Assert.AreEqual("external patient contour", result.Description);
        }

        [Test]
        public void PTV_Test()
        {
            var result = RtRoiInterpretedTypePresets.PTV();

            Assert.AreEqual("PTV", result.Code);
            Assert.AreEqual("Planning Target Volume (as defined in [ICRU Report 50])", result.Description);
        }

        [Test]
        public void CTV_Test()
        {
            var result = RtRoiInterpretedTypePresets.CTV();

            Assert.AreEqual("CTV", result.Code);
            Assert.AreEqual("Clinical Target Volume (as defined in [ICRU Report 50])", result.Description);
        }

        [Test]
        public void GTV_Test()
        {
            var result = RtRoiInterpretedTypePresets.GTV();

            Assert.AreEqual("GTV", result.Code);
            Assert.AreEqual("Gross Tumor Volume (as defined in [ICRU Report 50])", result.Description);
        }

        [Test]
        public void TREATED_VOLUME_Test()
        {
            var result = RtRoiInterpretedTypePresets.TREATED_VOLUME();

            Assert.AreEqual("TREATED_VOLUME", result.Code);
            Assert.AreEqual("Treated Volume (as defined in [ICRU Report 50])", result.Description);
        }

        [Test]
        public void IRRAD_VOLUME_Test()
        {
            var result = RtRoiInterpretedTypePresets.IRRAD_VOLUME();

            Assert.AreEqual("IRRAD_VOLUME", result.Code);
            Assert.AreEqual("Irradiated Volume (as defined in [ICRU Report 50])", result.Description);
        }

        [Test]
        public void BOLUS_Test()
        {
            var result = RtRoiInterpretedTypePresets.BOLUS();

            Assert.AreEqual("BOLUS", result.Code);
            Assert.AreEqual("patient bolus to be used for external beam therapy", result.Description);
        }

        [Test]
        public void AVOIDANCE_Test()
        {
            var result = RtRoiInterpretedTypePresets.AVOIDANCE();

            Assert.AreEqual("AVOIDANCE", result.Code);
            Assert.AreEqual("region in which dose is to be minimized", result.Description);
        }

        [Test]
        public void ORGAN_Test()
        {
            var result = RtRoiInterpretedTypePresets.ORGAN();

            Assert.AreEqual("ORGAN", result.Code);
            Assert.AreEqual("patient organ", result.Description);
        }

        [Test]
        public void MARKER_Test()
        {
            var result = RtRoiInterpretedTypePresets.MARKER();

            Assert.AreEqual("MARKER", result.Code);
            Assert.AreEqual("patient marker or marker on a localizer", result.Description);
        }

        [Test]
        public void REGISTRATION_Test()
        {
            var result = RtRoiInterpretedTypePresets.REGISTRATION();

            Assert.AreEqual("REGISTRATION", result.Code);
            Assert.AreEqual("registration ROI", result.Description);
        }

        [Test]
        public void ISOCENTER_Test()
        {
            var result = RtRoiInterpretedTypePresets.ISOCENTER();

            Assert.AreEqual("ISOCENTER", result.Code);
            Assert.AreEqual("treatment isocenter to be used for external beam therapy", result.Description);
        }

        [Test]
        public void CONTRAST_AGENT_Test()
        {
            var result = RtRoiInterpretedTypePresets.CONTRAST_AGENT();

            Assert.AreEqual("CONTRAST_AGENT", result.Code);
            Assert.AreEqual("volume into which a contrast agent has been injected", result.Description);
        }

        [Test]
        public void CAVITY_Test()
        {
            var result = RtRoiInterpretedTypePresets.CAVITY();

            Assert.AreEqual("CAVITY", result.Code);
            Assert.AreEqual("patient anatomical cavity", result.Description);
        }

        [Test]
        public void BRACHY_CHANNEL_Test()
        {
            var result = RtRoiInterpretedTypePresets.BRACHY_CHANNEL();

            Assert.AreEqual("BRACHY_CHANNEL", result.Code);
            Assert.AreEqual("brachytherapy channel", result.Description);
        }

        [Test]
        public void BRACHY_ACCESSORY_Test()
        {
            var result = RtRoiInterpretedTypePresets.BRACHY_ACCESSORY();

            Assert.AreEqual("BRACHY_ACCESSORY", result.Code);
            Assert.AreEqual("brachytherapy accessory device", result.Description);
        }

        [Test]
        public void BRACHY_SRC_APP_Test()
        {
            var result = RtRoiInterpretedTypePresets.BRACHY_SRC_APP();

            Assert.AreEqual("BRACHY_SRC_APP", result.Code);
            Assert.AreEqual("brachytherapy source applicator", result.Description);
        }

        [Test]
        public void BRACHY_CHNL_SHLD_Test()
        {
            var result = RtRoiInterpretedTypePresets.BRACHY_CHNL_SHLD();

            Assert.AreEqual("BRACHY_CHNL_SHLD", result.Code);
            Assert.AreEqual("brachytherapy channel shield", result.Description);
        }

        [Test]
        public void SUPPORT_Test()
        {
            var result = RtRoiInterpretedTypePresets.SUPPORT();

            Assert.AreEqual("SUPPORT", result.Code);
            Assert.AreEqual("external patient support device", result.Description);
        }

        [Test]
        public void FIXATION_Test()
        {
            var result = RtRoiInterpretedTypePresets.FIXATION();

            Assert.AreEqual("FIXATION", result.Code);
            Assert.AreEqual("external patient fixation or immobilization device", result.Description);
        }

        [Test]
        public void DOSE_REGION_Test()
        {
            var result = RtRoiInterpretedTypePresets.DOSE_REGION();

            Assert.AreEqual("DOSE_REGION", result.Code);
            Assert.AreEqual("ROI to be used as a dose reference", result.Description);
        }

        [Test]
        public void CONTROL_Test()
        {
            var result = RtRoiInterpretedTypePresets.CONTROL();

            Assert.AreEqual("CONTROL", result.Code);
            Assert.AreEqual("ROI to be used in control of dose optimization and calculation", result.Description);
        }

        [Test]
        public void DOSE_MEASUREMENT_Test()
        {
            var result = RtRoiInterpretedTypePresets.DOSE_MEASUREMENT();

            Assert.AreEqual("DOSE_MEASUREMENT", result.Code);
            Assert.AreEqual("ROI representing a dose measurement device, such as a chamber or TLD", result.Description);
        }

        [Test]
        public void STOMACH_Test()
        {
            var result = RtRoiInterpretedTypePresets.STOMACH();

            Assert.AreEqual("STOMACH", result.Code);
            Assert.AreEqual("Stomach", result.Description);
        }

        [Test]
        public void PANCREAS_Test()
        {
            var result = RtRoiInterpretedTypePresets.PANCREAS();

            Assert.AreEqual("PANCREAS", result.Code);
            Assert.AreEqual("Pancreas", result.Description);
        }

        [Test]
        public void SMALL_BOWEL_Test()
        {
            var result = RtRoiInterpretedTypePresets.SMALL_BOWEL();

            Assert.AreEqual("SMALL_BOWEL", result.Code);
            Assert.AreEqual("Small bowel (small intestine)", result.Description);
        }

        [Test]
        public void SPINAL_CORD_Test()
        {
            var result = RtRoiInterpretedTypePresets.SPINAL_CORD();

            Assert.AreEqual("SPINAL_CORD", result.Code);
            Assert.AreEqual("Spinal cord", result.Description);
        }

        [Test]
        public void LIVER_Test()
        {
            var result = RtRoiInterpretedTypePresets.LIVER();

            Assert.AreEqual("LIVER", result.Code);
            Assert.AreEqual("Liver", result.Description);
        }

        [Test]
        public void LEFT_KIDNEY_Test()
        {
            var result = RtRoiInterpretedTypePresets.LEFT_KIDNEY();

            Assert.AreEqual("LEFT_KIDNEY", result.Code);
            Assert.AreEqual("Left kidney", result.Description);
        }

        [Test]
        public void RIGHT_KIDNEY_Test()
        {
            var result = RtRoiInterpretedTypePresets.RIGHT_KIDNEY();

            Assert.AreEqual("RIGHT_KIDNEY", result.Code);
            Assert.AreEqual("Right kidney", result.Description);
        }

        [Test]
        public void PRV_Test()
        {
            var result = RtRoiInterpretedTypePresets.PRV();

            Assert.AreEqual("PRV", result.Code);
            Assert.AreEqual("Planning risk volume. DOI:10.1007/s00066-016-1057-x", result.Description);
        }

        [Test]
        public void PTV_DOM_Test()
        {
            var result = RtRoiInterpretedTypePresets.PTV_DOM();

            Assert.AreEqual("PTV_DOM", result.Code);
            Assert.AreEqual("Dominant Planning risk volume. DOI:10.1007/s00066-016-1057-x", result.Description);
        }

        [Test]
        public void PTV_SIP_STOMACH_Test()
        {
            var result = RtRoiInterpretedTypePresets.PTV_SIP_STOMACH();

            Assert.AreEqual("PTV_SIP_STOMACH", result.Code);
            Assert.AreEqual("Simultaneous integrated protection of stomach. DOI:10.1007/s00066-016-1057-x", result.Description);
        }

        [Test]
        public void PTV_SIP_SMALL_BOWEL_Test()
        {
            var result = RtRoiInterpretedTypePresets.PTV_SIP_SMALL_BOWEL();

            Assert.AreEqual("PTV_SIP_SMALL_BOWEL", result.Code);
            Assert.AreEqual("Simultaneous integrated protection of small bowel. DOI:10.1007/s00066-016-1057-x", result.Description);
        }

        [Test]
        public void ITV_Test()
        {
            var result = RtRoiInterpretedTypePresets.ITV();

            Assert.AreEqual("ITV", result.Code);
            Assert.AreEqual("Internal target volume", result.Description);
        }
    }
}
