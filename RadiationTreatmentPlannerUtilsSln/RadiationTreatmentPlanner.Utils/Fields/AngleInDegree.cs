﻿using System;

namespace RadiationTreatmentPlanner.Utils.Fields
{
    public readonly struct AngleInDegree
    {
        public double Value { get; }

        /// <summary>
        /// Angle in degree. Equals operations work with tolerance of 0.001 to fix floating point errors.
        /// </summary>
        /// <param name="value">The value of angle in degree [0, 360]</param>
        public AngleInDegree(double value)
        {
            if (value < 0 || value > 360)
                throw new ArgumentOutOfRangeException($"{nameof(value)} must be >= 0 and <= 360," +
                                                      $" but was {value}.");
            Value = value;
        }

        public override bool Equals(object obj)
        {
            return obj != null && Equals((AngleInDegree)obj);
        }

        public bool Equals(AngleInDegree other)
        {
            return Math.Abs(Value - other.Value) < 0.001;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public override string ToString()
        {
            return $"{Value}°";
        }
    }
}